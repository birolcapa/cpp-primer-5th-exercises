## Writing to a Stream
`std::cout << "Enter 2 numbers:";`\
`std::cout << std::endl;`

The second operator prints endl, which is a special value called manipulator.

Writing endl has the effect of ending the current line and flushing the buffer associated with that device.

Flushing the buffer ensures that all the output the program has generated so far is actually written to the output stream, rather than sitting in memory waiting to be written.

## Input Stream
An istream becomes invalid when we hit end-of-file or encounter an invalid output, such as reading a value that is not an integer.
An istream that is in an invalid state will cause the condition to yield false.

## Introducing Classes
In C++ we define our own data structures by defining a class.
A class defines a type along with a collection of operations that are related to that type.

How these data are stored or computed is not our concern.
To use a class, we need NOT to care about how it is implemented.
Instead, what we need to know is what operations objects of that type can perform.

## Key Concept: Classes define behavior
The important thing to keep in mind when you read these programs is that the author of the `Sales_item` class defines all the actions that can be performed by objects of this class.

That is, the `Sales_item` class defines what happens when a `Sales_item` object is created and what happens when the assignment, addition, or the input and output operators are applied to `Sales_item`.

In general, the class author determines all the operations that can ben used on objects of the class type.