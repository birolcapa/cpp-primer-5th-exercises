/**
 *  \file 21.cpp
 *  \brief Exercise 1.21:
 *  Write a program that reads two Sales_item objects that have the same ISBN 
 *  and produces their sum.
 *  Info:
 *  How to run: 21.exe <"Data\add_item" > "Data\21_out"
 */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item item1, item2;
    std::cin >> item1 >> item2;
    std::cout << item1 + item2;
    return 0;
}