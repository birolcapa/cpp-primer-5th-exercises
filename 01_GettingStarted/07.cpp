/**
 *  \file 07.cpp
 *  \brief Exercise 1.7:
 *  Compile a program that has incorrectly nested comments
 */

#include <iostream>

/*
 * Comment error cause compile error /* */
*/
int main()
{
    return 0;
}