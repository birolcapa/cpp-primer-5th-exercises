/**
 *  \file 11.cpp
 *  \brief Exercise 1.11:
 *  Write a program that prompts the user for two integers. 
 *  Print each number in the range specified by those two integers.
 */

#include <iostream>

int main()
{
    int begin = 0 , end = 0;
    std::cout << "Enter 2 number as begin and end" << std::endl;
    std::cin >> begin >> end;
    while( begin <= end )
    {
        std::cout << begin << std:: endl;
        begin++;
    }
    return 0;
}