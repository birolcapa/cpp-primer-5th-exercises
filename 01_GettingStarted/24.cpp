/**
 *  \file 24.cpp
 *  \brief Exercise 1.24:
 *  Test the previous program by giving multiple transactions 
 *  representing multiple ISBNs. 
 *  The records for each ISBN should be grouped together.
 *  Info:
 *  How to run: 24.exe <"Data\book_sales" > "Data\24_out"
 */
#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item currItem, item;
    // read first number and ensure that we have data to process
    if(std::cin >> currItem) {
        int cnt = 1;
        while(std::cin >> item){ // read the remaining numbers
            if(item.isbn() == currItem.isbn()) {
                ++cnt;
            }
            else { // otherwise, print the count for the previous value
                std::cout << currItem.isbn() << " " << cnt << std::endl;
                currItem = item;
                cnt = 1;
            }
        }
        // remember to print the count for the last value in the file
        std::cout << currItem.isbn() << " " << cnt << std::endl;
    }
    return 0;
}