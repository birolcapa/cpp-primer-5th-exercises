/**
 *  \file 23.cpp
 *  \brief Exercise 1.23:
 *  Write a program that reads several transactions and counts 
 *  how many transactions occur for each ISBN.
 *  How to run: 23.exe <"Data\book_sales" > "Data\23_out"
 */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item currItem, item;
    // read first number and ensure that we have data to process
    if(std::cin >> currItem) {
        int cnt = 1;
        while(std::cin >> item){ // read the remaining numbers
            if(item.isbn() == currItem.isbn()) {
                ++cnt;
            }
            else { // otherwise, print the count for the previous value
                std::cout << currItem.isbn() << " " << cnt << std::endl;
                currItem = item;
                cnt = 1;
            }
        }
        // remember to print the count for the last value in the file
        std::cout << currItem.isbn() << " " << cnt << std::endl;
    }
    return 0;
}