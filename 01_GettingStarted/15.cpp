/**
 *  \file 15.cpp
 *  \brief Exercise 1.15:
 *  Write programs that contain the common errors discussed 
 *  in the box on page 16. 
 *  Familiarize yourself with the messages the compiler generates
 *  Info:
 *  An istream becomes invalid when we hit end-of-file or
 *  encounter an invalid input, such as reading a value that is not an integer. 
 *  An istream that is in an invalid state will cause the condition to yield false.
 */

#include <iostream>

int main()
{
    // error: used colon, not a semicolon, after endl
    std::cout << "Read each file" << std::endl:

    // error: missing quotes around string literal
    std::cout << Update master. << std::endl;
    
    // error: second output operator is missing
    std::cout << "Write new master" std::endl;
    return 0;
}