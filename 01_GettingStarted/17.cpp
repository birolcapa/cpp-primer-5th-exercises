/**
 *  \file 17.cpp
 *  \brief Exercise 1.17:
 *  What happens in the program presented in this section 
 *  if the input values are all equal?
 *  What if there are no duplicated values?
 */

#include <iostream>

int main()
{
    int currentVal = 0, val = 0;
    if(std::cin >> currentVal)
    {
        int cnt = 1;
        while(std::cin >> val)
        {
            if(val == currentVal) cnt++;
            else 
            {
                std::cout << currentVal << " occurs "
                    << cnt << " times" << std::endl;
                currentVal = val;
                cnt = 1;
            }
        }
        std::cout << currentVal << " occurs "
            << cnt << " times" << std::endl;
    }
    return 0;
}