/**
 *  \file 19.cpp
 *  \brief Exercise 1.19: Revise the program you wrote for the exercises in 
 *  1.4.1 (p. 13) that printed a range of numbers so that
 *  it handles input in which the first number is smaller than the second.
 *  Info:
 *  Write a program that prompts the user for two integers. 
 *  Print each number in the range specified by those two integers
 */

#include <iostream>

int main()
{
    int first = 0 , second = 0;
    int begin = 0 , end = 0;
    std::cout << "Enter 2 number as begin and end" << std::endl;
    std::cin >> first >> second;
    if(first < second)
    {
        begin = first;
        end = second;
    }
    else 
    {
        begin = second;
        end = first;
    }
    while( begin <= end )
    {
        std::cout << begin << std:: endl;
        begin++;
    }
    return 0;
}