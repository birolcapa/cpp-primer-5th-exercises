/**
 *  \file 08.cpp
 *  \brief Exercise 1.8:
 *  Indicate which, if any, of the following output statements are legal:
 */

#include <iostream>

int main()
{
    std::cout << "/*"; // This line is Ok
    std::cout << "*/"; // This line is Ok
    std::cout << /* "*/" */; This line has a problem
    std::cout << /* "*/" /* "/*" */; // This line is Ok
    return 0;
}