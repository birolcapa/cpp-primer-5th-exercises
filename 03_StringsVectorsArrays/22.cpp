/**
 *  \file 22.cpp
 *  \brief Exercise 3.22:
 *  Revise the loop that printed the first paragraph in text to instead 
 *  change the elements in text that correspond to the first paragraph to all uppercase.
 *  After you’ve updated text, print its contents.
 */

#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;

using std::cout;
using std::endl;
using std::cin;

int main()
{
    vector<string> text;
    string line;
    
    while(getline(cin, line)) {
        text.push_back(line);
    }
    
    for(auto it = text.begin(); it != text.end() && !it->empty(); ++it) {
        for(auto &c : *it){
            c = toupper(c);
        }
    }
    
    for(auto &elem : text) {
        if(elem.empty()) {
            cout << endl;
        }
        else {
            cout << elem << " ";
        }
    }
    
    return 0;
}