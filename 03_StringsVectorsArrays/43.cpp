/**
 *  \file 43.cpp
 *  \brief Exercise 3.43: 
 *  Write three different versions of a program to print the elements of ia.
 *  One version should use a range for to manage the iteration,
 *  the other two should use an ordinary for loop in one case using subscripts
 *  and in the other using pointers. In all three programs write all the types directly.
 *  That is, do not use a type alias, auto, or decltype to simplify the code.
 */

#include <iostream>
#include <iterator>
#include <vector>
using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::vector;

int main()
{
    constexpr size_t row = 3;
    constexpr size_t col = 4;
    
    int ia[row][col] = {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11},
    };
    
    // a range for manage the iteration
    for(const int(&p)[4] : ia) {
        for(int q : p) {
            cout << q << " ";
        }
        cout << endl;
    }
    cout << endl;
    
    // ordinary loop using subscripts
    for(size_t i = 0; i != row; ++i) {
        for(size_t j = 0; j!= col; ++j) {
            cout << ia[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
    
    // pointers
    for(int(*p)[4] = ia; p != ia + row; ++p) {
        for(int *q = *p; q != *p + col; ++q) {
            cout << *q << " ";
        }
        cout << endl;
    }
    cout << endl; 
    return 0;
}