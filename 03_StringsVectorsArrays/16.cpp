/**
 *  \file 16.cpp
 *  \brief Exercise 3.16:
 *  Write a program to print the size and contents of the vectors from
 *  exercise 3.13. Check whether your answers to that exercise were correct.
 *  If not, restudy 3.3.1 (p. 97) until you understand why you were wrong.
 */
 
#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;

using std::cout;
using std::endl;

int main()
{
    vector<int> v1; // size: 0
    cout << "size of v1 is " << v1.size() << endl;
    for(auto &i : v1) cout << i << " ";
    cout << endl;
    
    vector<int> v2(10); // size: 10, since type is int, every element is: 0
    cout << "size of v2 is " << v2.size() << endl;
    for(auto &i : v2) cout << i << " ";
    cout << endl;
    
    vector<int> v3(10, 42); // size: 10, every value is 42
    cout << "size of v3 is " << v3.size() << endl;
    for(auto &i : v3) cout << i << " ";
    cout << endl;
    
    vector<int> v4{10}; // size: 1, value is 10
    cout << "size of v4 is " << v4.size() << endl;
    for(auto &i : v4) cout << i << " ";
    cout << endl;
    
    vector<int> v5{10, 42}; //size: 2, values 10 and 42
    cout << "size of v5 is " << v5.size() << endl;
    for(auto &i : v5) cout << i << " ";
    cout << endl;
    
    vector<string> v6{10}; // size:10, every element is empty string
    cout << "size of v6 is " << v6.size() << endl;
    for(auto &i : v6) cout << i << " ";
    cout << endl;
    
    vector<string> v7{10, "hi"}; // size: 10, every value is "hi"
    cout << "size of v7 is " << v7.size() << endl;
    for(auto &i : v7) cout << i << " ";
    cout << endl;
    return 0;
}