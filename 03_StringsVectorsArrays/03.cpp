/**
 *  \file 3.cpp
 *  \brief Exercise 3.3: 
 *  Explain how whitespace characters are handled in the string input operator and in the getline function
 *  
 *  Info:
 *  The getline function takes an input stream and a string. 
 *  The function reads the given stream up to including
 *  the first newline and stores what it read - not including the newline - in its string argument. 
 *  
 *  getline function white space handling: 
 *  do not ignore the beginning of the line blank characters 
 *  until it encounters a line break, read to termination and discard newline 
 *  (line breaks removed from the input stream but is not stored in the string object)
 */
