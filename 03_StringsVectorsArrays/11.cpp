Exercise 3.11: Is the following range for legal? If so, what is the type of c?

const string s = "Keep out!";
for (auto &c : s) { /* ... */ }

When you dont change the c, it is legal
When you change the c, then it is illegal.
cout << c  is legal but
c = 'X' is illegal
c is a const char reference.
So it is a readonly variable