/**
 *  \file 2.cpp
 *  \brief Exercise 3.2:
 *  Write a program to read the standard input a line at a time.
 *  Modify your program to read a word at a time
 */

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

int main()
{
    /*string input;
    while(getline(cin, input)) {
        cout << "input is: " << input << endl;
    }*/
    
    string word;
    while(cin >> word) {
        cout << "word is: " << word << endl;
    }
    
    return 0;
}