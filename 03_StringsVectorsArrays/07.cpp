/**
 *  \file 7.cpp
 *  \brief Exercise 3.7:
 *  What would happen if you define the loop control variable 
 *  in the previous exercise as type char?
 *  Predict the results and then change your program to use a char to see if you were right.
 *  Info:
 *  There would be no difference.
 *  We use auto to let the compiler determine the type of c. which in this case will be char&.
 */

#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
    string str{"a simple string" };
    cout << str << endl;
    for(char &c : str) {
        c = 'X';
    }
    cout << str << endl;
    return 0;
}