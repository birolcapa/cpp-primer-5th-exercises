/**
 *  \file 42.cpp
 *  \brief Exercise 3.42:
 *  Write a program to copy a vector of ints into an array of ints.
 */

#include <iostream>
#include <iterator>
#include <vector>
using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::vector;

int main()
{
    vector<int> vec {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    for(const auto &elem : vec) {
        cout << elem << " ";
    }
    cout << endl;
    
    int arr[10];
    for(auto i = begin(arr); i != end(arr); ++i) {
        *i = 3 * vec[i-begin(arr)];
    }
    
    for(const auto &elem : arr) {
        cout << elem << " ";
    }
    cout << endl;
    return 0;
}