/**
 *  \file 45.cpp
 *  \brief Exercise 3.45:
 *  Rewrite the programs again, this time using auto.
 */

#include <iostream>
#include <iterator>
#include <vector>
using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::vector;

int main()
{
    constexpr size_t row = 3;
    constexpr size_t col = 4;
    
    int ia[row][col] = {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11},
    };
        
    // a range for manage the iteration
    for(const auto &p : ia) {
        for(const auto q : p) {
            cout << q << " ";
        }
        cout << endl;
    }
    cout << endl;
    
    // ordinary loop using subscripts
    for(size_t i = 0; i != row; ++i) {
        for(size_t j = 0; j!= col; ++j) {
            cout << ia[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
    
    // pointers
    for(auto p = ia; p != ia + row; ++p) {
        for(auto q = *p; q != *p + col; ++q) {
            cout << *q << " ";
        }
        cout << endl;
    }
    cout << endl; 
    return 0;
}