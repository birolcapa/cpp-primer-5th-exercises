/**
 *  \file 32.cpp
 *  \brief Exercise 3.32:
 *  Copy the array you defined in the previous exercise into another array.
 *  Rewrite your program to use vectors.
 */
#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

int main()
{
    int arr[10];
    for(size_t i = 0; i < 10; i++) {
        arr[i] = i;
    }
    int brr[10];
    for(size_t i = 0; i < 10; i++) {
        brr[i] = arr[i];
    }
    
    vector<int> crr;
    for(int i = 0; i < 10;  i++) {
        crr.push_back(i);
    }
    
    for(auto i: arr)
    {
        cout << i << " ";
    }
    cout<< endl;
    for(auto i: brr)
    {
        cout << i << " ";
    }
    cout<< endl;
    for(auto i: crr)
    {
        cout << i << " ";
    }
    cout<< endl;
    return 0;
}
