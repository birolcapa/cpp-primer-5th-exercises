/**
 *  \file 17.cpp
 *  \brief Exercise 3.17:
 *  Read a sequence of words from cin and store the values a vector. 
 *  After you’ve read all the words, process the vector and change each word to uppercase.
 *  Print the transformed elements, eight words to a line.
 */

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;

using std::vector;

int main()
{
    string word;
    vector<string> words;
    
    while(cin >> word) {
        words.push_back(word);
    }
    
    for(auto &word : words) {
        for(auto &c : word) {
            c = toupper(c);
        }
    }
    
    decltype (words.size()) i = 0;
    for(i = 0; i < words.size(); i++) {
        cout << words[i] << " ";
        if(i % 8 == 0) {
            cout << endl;
        }
    }
    
    return 0;
}