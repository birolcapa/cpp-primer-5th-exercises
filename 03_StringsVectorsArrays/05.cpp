/**
 *  \file 5.cpp
 *  \brief Exercise 3.5:
 *  Write a program to read strings from the standard input, 
 *  concatenating what is read into one large string. 
 *  Print the concatenated string. Next, change
 *  the program to separate adjacent input strings by a space.
 */
 
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

void concatenate() {
    string s1, large;
    while(cin >> s1) { // exits with end of file char: ctrl + z in windows
        large += s1;
    }
    cout << "large string is " << large << endl;
}

void seperate() {
    string str;
    string largeStr;
    while(cin >> str) {
        if(largeStr.empty()) {
            largeStr += str;
        }
        else{
            largeStr += " " + str;
        }
    }
    cout << "seperated large str is " << largeStr << endl;
}
int main()
{
    seperate();
    return 0;
}