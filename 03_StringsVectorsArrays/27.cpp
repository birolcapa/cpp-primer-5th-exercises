/**
 *  \file 27.cpp
 *  \brief Exercise 3.27: Assuming txt_size is a function that takes no arguments and returns
an int value, which of the following definitions are illegal? Explain why.
 */

#include <iostream>

int txt_size() {
    return 10;
}

int main()
{
    unsigned buf_size = 1024;
    //int ia[buf_size]; // illegal not a constant expression
    int ia[4 * 7 - 14]; // Ok
    //int ia[txt_size()]; // failure was caused by call of undefined function or one not declared 'constexpr'
    char st[11] = "fundamental"; // illegal error C2117: 'st': array bounds overflow we need trailing '\0'
    return 0;
}
