/**
 *  \file 14.cpp
 *  \brief Exercise 3.14:
 *  Write a program to read a sequence of ints from cin and store
 *  those values in a vector.
 */
#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;

using std::vector;

int main()
{
    int input;
    vector<int> inputs;
    while(cin >> input) {
        inputs.push_back(input);
    }
    decltype(inputs.size()) i = 0;
    for (i = 0; i < inputs.size(); ++i) {
        cout << inputs[i] << " ";
    }
    cout << endl;
    return 0;
}