/**
 *  \file 20.cpp
 *  \brief Exercise 3.20:
 *  Read a set of integers into a vector. Print the sum of each pair of adjacent elements.
 *  Change your program so that it prints the sum of the first and last
 *  elements, followed by the sum of the second and second-to-last, and so on.
 */

#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;

using std::vector;

int main()
{
    int input;
    vector<int> inputs;
    while(cin >> input) {
        inputs.push_back(input);
    }
    decltype(inputs.size()) i = 0;
    for (i = 0; i < inputs.size() - 1; ++i) {
        cout << inputs[i] + inputs[i+1] << " ";
    }
    cout << endl;
    
    decltype(inputs.size()) mid;
    if(inputs.size() %2 == 0) {
        mid = inputs.size() / 2;
    }
    else {
        mid = inputs.size() / 2 + 1;
    }
    
    for (i = 0; i != mid; ++i) {
        cout << inputs[i] + inputs[inputs.size() - 1 - i] << " ";
    }
    return 0;
}