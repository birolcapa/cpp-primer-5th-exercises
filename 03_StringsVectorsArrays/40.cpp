/**
 *  \file 40.cpp
 *  \brief Exercise 3.40:
 *  Write a program to define two character arrays initialized from string
 *  literals. Now define a third character array to hold the concatenation of the two arrays.
 *  Use strcpy and strcat to copy the two arrays into the third.
 */

#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;
int main()
{

    const char ca1[] = "A string example"; // null terminator added automatically
    const char ca2[] = "A different string"; // null terminator added automatically
    // strlen(p) Returns the length of p, not counting the null
    size_t new_size = strlen(ca1) + strlen(" ") + strlen(ca2) + 1; // +1 is: null terminator
    char* ca_sum = new char[new_size];
    strcpy(ca_sum, ca1); // copies ca1 into ca_sum
    strcat(ca_sum, " "); // adds a space at the end of ca_sum
    strcat(ca_sum, ca2); // concatenates ca2 onto ca_sum

    cout << ca_sum << endl;
    delete[] ca_sum;
    return 0;
}