/**
 *  \file 23.cpp
 *  \brief Exercise 3.23:
 *  Write a program to create a vector with ten int elements. Using an
 *  iterator, assign each element a value that is twice its current value. 
 *  Test your program by printing the vector.
 */

#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;

using std::vector;

int main()
{
    int input;
    vector<int> inputs;
    while(cin >> input) {
        inputs.push_back(input);
    }
    
    for(auto it = inputs.begin(); it != inputs.end(); ++it)
    {
        *it *= 2;
    }
    
    for (auto it = inputs.cbegin(); it != inputs.cend(); ++it) {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}