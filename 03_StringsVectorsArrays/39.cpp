/**
 *  \file 39.cpp
 *  \brief Exercise 3.39:
 *  Write a program to compare two strings. Now write a program to
 *  compare the values of two C-style character strings.
 */

#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;
int main()
{
    string s1 = "hello";
    string s2 = "world";
    if(s1==s2) {
        cout << "strings are equal" << endl;
    }
    else {
        cout << "strings are NOT equal" << endl;
    }
    char a3[] = "C++"; // null terminator added automatically
    const char ca1[] = "A string example";
    const char ca2[] = "A different string";
    if(!strcmp(ca1, ca2)) {
        cout << "char arrays are equal" << endl;
    }
    else {
        cout << "char arrays are NOT equal" << endl;
    }
    
    return 0;
}