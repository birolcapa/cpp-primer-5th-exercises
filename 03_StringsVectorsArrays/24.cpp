/**
 *  \file 24.cpp
 *  \brief Exercise 3.24:
 *  Redo the last exercise from 3.3.3 (p. 105) using iterators.
 *  Exercise 3.20:
 *  Read a set of integers into a vector. Print the sum of each pair of adjacent elements. 
 *  Change your program so that it prints the sum of the first and last elements,
 *  followed by the sum of the second and second-to-last, and so on.
 */

#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;

using std::vector;

void sum_adjacent(vector<int> &inputs) {
    for (auto it = inputs.begin(); it != inputs.end()-1; ++it) {
        cout << *it + *(it+1) << " ";
    }
    cout << endl;
}

void sum_symetric(vector<int> &inputs) {
    for(auto beg = inputs.begin(), end = inputs.end()-1;
            beg <= end; 
            beg++, end--) {
        cout << *beg + *end << " ";
    }
    cout << endl;
}
int main()
{
    int input;
    vector<int> inputs;
    while(cin >> input) {
        inputs.push_back(input);
    }

    sum_adjacent(inputs);
    sum_symetric(inputs);
    return 0;
}