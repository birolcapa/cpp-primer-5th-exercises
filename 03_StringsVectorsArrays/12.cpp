Exercise 3.12: Which, if any, of the following vector definitions are in error? For
those that are legal, explain what the definition does. For those that are not legal,
explain why they are illegal.
(a) vector<vector<int>> ivec; // legal with c++11
(b) vector<string> svec = ivec; // illegal, ivec is a vector of vector<int>s : different type
(c) vector<string> svec(10, "null"); // legal, svec has 10 null strings