/**
 *  \file 33.cpp
 *  \brief Exercise 3.33:
 *  What would happen if we did not initialize the scores array in the
 *  program on page 116?
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
    // count the number of grades by clusters of ten: 0--9, 10--19, . . . 90--99, 100
    unsigned scores[11]; // undefined values
    unsigned grade;
    while (cin >> grade) {
        if (grade <= 100)
        ++scores[grade/10]; // increment the counter for the current cluster
    }
    for(auto i:scores){
        cout << i << " ";
    }
    cout << endl;
    return 0;
}
/*
if scores was defined inside a function. then the value of each elemens is undefined:
here is the sample output:
11520475 17824672 11591476 17824680 11341839 0 17824708 11444519 11393849 11393849 11444540
if scores was defined globally, then the value of each element would be 0.
*/