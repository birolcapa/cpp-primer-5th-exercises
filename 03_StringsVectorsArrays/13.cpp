Exercise 3.13: How many elements are there in each of the following vectors? What
are the values of the elements?
(a) vector<int> v1; // size: 0
(b) vector<int> v2(10); // size: 10, since type is int, every element is: 0
(c) vector<int> v3(10, 42); // size: 10, every value is 42
(d) vector<int> v4{10}; // size: 1, value is 10
(e) vector<int> v5{10, 42}; //size: 2, values 10 and 42
(f) vector<string> v6{10}; // size:10, every element is empty string
(g) vector<string> v7{10, "hi"}; // size: 10, every value is "hi"