/**
 *  \file 37.cpp
 *  \brief Exercise 3.37:
 *  What does the following program do?
 *  Info: 
 *  Since there is no null character at the end, the program print all the characters in ca, 
 *  and continue to print whatever in memory until it finds a null character
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    const char ca[] = {'h', 'e', 'l', 'l', 'o' };
    const char *cp = ca;
    while (*cp) {
        cout << *cp << endl;
        ++cp;
    }
    return 0;
}