/**
 *  \file 1.cpp
 *  \brief Exercise 3.1:
 *  Rewrite the exercises from 1.4.1 (p. 13) and 
 *  2.6.2 (p. 76) with appropriate using declarations.
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

struct Sales_data {
    string bookNo;
    unsigned int units_sold = 0;
    double revenue = 0.0;
};

void ex_1_5_1() {
    Sales_data item;
    double price = 0;
    cin >> item.bookNo >> item.units_sold >> price;
    item.revenue = item.units_sold * price;
    cout << item.bookNo << " " << item.units_sold << " " << item.revenue << endl;
}

int ex_1_5_2() {
    Sales_data item1, item2;
    double price1, price2;
    cin >> item1.bookNo >> item1.units_sold >> price1;
    cin >> item2.bookNo >> item2.units_sold >> price2;
    item1.revenue = item1.units_sold * price1;
    item2.revenue = item2.units_sold * price2;
    if(item1.bookNo == item2.bookNo) {
        unsigned totalCnt = item1.units_sold + item2.units_sold;
        double totalRevanue = item1.revenue + item2.revenue;
        cout << item1.bookNo << " " << item1.units_sold << " " << item1.revenue << endl;
        if(totalCnt != 0) {
            cout << "Average value is " << totalRevanue / totalCnt << endl;
        }
        else {
            cout << "no sales" << endl;
        }
    }
    else {
        std::cerr << "Data must refer to same ISBN" << endl;
        return -1;
    }
    return 0;
}

int ex_1_6() {
    Sales_data total;
    double totalPrice = 0.0;
    if(cin >> total.bookNo >> total.units_sold >> totalPrice) {
        total.revenue = total.units_sold * totalPrice;
        Sales_data trans;
        double transPrice = 0.0;
        while(cin >> trans.bookNo >> trans.units_sold >> transPrice) {
            trans.revenue = trans.units_sold * transPrice;
            if(total.bookNo == trans.bookNo) {
                total.units_sold += trans.units_sold;
                total.revenue += trans.revenue;
            }
            else {
                cout << total.bookNo << " " << total.units_sold << " revenue " << total.revenue << endl;
                if(total.units_sold != 0) {
                    cout << "Average value is " << total.revenue / total.units_sold << endl;
                }
                else {
                    cout << "no sales" << endl;
                }
                
                total.bookNo = trans.bookNo;
                total.units_sold = trans.units_sold;
                total.revenue = trans.revenue;
            }
        }
        cout << total.bookNo << " " << total.units_sold << " revenue " << total.revenue << endl;
        if(total.units_sold != 0) {
            cout << "Average value is " << total.revenue / total.units_sold << endl;
        }
        else {
            cout << "no sales" << endl;
        }
        
        return 0;
    }
    else {
        std::cerr << "No Data" << endl;
        return -1;
    }
    
}

void ex_1_4_1() {
    int sum = 0;
    int val = 1;
    
    while(val <=10) {
        sum += val;
        ++val;
    }
    cout << "Sum of 1 to 10 inclusive is " << sum << endl;
}

int main()
{
    ex_1_4_1();
    //ex_1_5_1();
    //int returnVal = ex_1_5_2();
    int returnVal = ex_1_6();
    return returnVal;
}