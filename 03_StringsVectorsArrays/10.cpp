/**
 *  \file 10.cpp
 *  \brief Exercise 3.10: 
 *  Write a program that reads a string of characters including punctuation
 *  and writes what was read but with the punctuation removed
 */

#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
    string str;
    cout << "Enter a string of characters including punctuation:" << endl;
    while(getline(cin, str)) {
        for(auto c : str) {
            if(!ispunct(c)){
                cout << c;
            }
        }
        cout << endl;
    }
    return 0;
}