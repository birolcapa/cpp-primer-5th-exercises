/**
 *  \file 21.cpp
 *  \brief Exercise 3.21:
 *  Redo the first exercise from § 3.3.3 (p. 105) using iterators.
 */

#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;

using std::cout;
using std::endl;

template <class T>
void print(const vector<T> & v) {
    for(auto it = v.begin(); it != v.end(); ++it) {
        cout << *it << " ";
    }
    cout << endl;
}

int main()
{
    vector<int> v1; // size: 0
    print(v1);
    
    vector<int> v2(10); // size: 10, since type is int, every element is: 0
    print(v2);
    
    vector<int> v3(10, 42); // size: 10, every value is 42
    print(v3);
    
    vector<int> v4{10}; // size: 1, value is 10
    print(v4);
    
    vector<int> v5{10, 42}; //size: 2, values 10 and 42
    print(v5);
    
    vector<string> v6{10}; // size:10, every element is empty string
    print(v6);
    
    vector<string> v7{10, "hi"}; // size: 10, every value is "hi"
    print(v7);
    
    return 0;
}