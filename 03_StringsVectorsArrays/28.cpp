/**
 *  \file 28.cpp
 *  \brief Exercise 3.28:
 *  What are the values in the following arrays?
 */

#include <iostream>
#include <string>

int ia[10]; // ten elements of 0
std::string sa[10]; // ten elements of empty string

int main() {
    std::string sa2[10]; // ten elements of empty string
    int ia2[10]; // ten elements of undefined value
    std::cout << ia2[0] << std::endl;
}