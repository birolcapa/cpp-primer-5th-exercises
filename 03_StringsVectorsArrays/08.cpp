/**
 *  \file 8.cpp
 *  \brief Exercise 3.8:
 *  Rewrite the program in the first exercise, first using a while and again
 *  using a traditional for loop. Which of the three approaches do you prefer and why?
 */

#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
    string str{"a simple string" };
    cout << str << endl;
    
    // very easy 10/10
    for(auto &c : str) {
        c = 'X';
    }
    cout << str << endl;
    
    decltype(str.size()) i = 0;
    
    // dont forget to increase i 6/10
    while(i < str.size()) {
        str[i] = 'Y';
        ++i;
    }
    cout << str << endl;
    
    // I like reading this, hard to review 7/10
    for(i = 0; i < str.size(); ++i) {
        str[i] = 'Z';
    }
    cout << str << endl;
    
    return 0;
}