/**
 *  \file 19.cpp
 *  \brief Exercise 3.19:
 *  List three ways to define a vector and give it ten elements, each with
 *  the value 42. Indicate whether there is a preferred way to do so and why.
 */

#include <iostream>
#include <vector>
using std::vector;
using std::cout;
using std::endl;

int main()
{
    vector<int> v1(10,42);
    
    cout << "size of v1 is " << v1.size() << endl;
    for(auto &i : v1) cout << i << " ";
    cout << endl;
    
    vector<int> v2{42,42,42,42,42,42,42,42,42,42};
    
    cout << "size of v2 is " << v2.size() << endl;
    for(auto &i : v2) cout << i << " ";
    cout << endl;
    
    vector<int> v3;
    for(auto i = 0; i < 10; i++) {
        v3.push_back(42);
    }
    
    cout << "size of v3 is " << v3.size() << endl;
    for(auto &i : v3) cout << i << " ";
    cout << endl;
    return 0;
}