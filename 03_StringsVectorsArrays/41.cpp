/**
 *  \file 41.cpp
 *  \brief Exercise 3.41:
 *  Write a program to initialize a vector from an array of ints.
 */
#include <iostream>
#include <iterator>
#include <vector>
using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::vector;

int main()
{
    int arr[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    vector<int> vec(begin(arr), end(arr));
    for(const auto &elem : vec) {
        cout << elem << " ";
    }
    cout << endl;
    return 0;
}