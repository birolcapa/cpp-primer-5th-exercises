/**
 *  \file 36.cpp
 *  \brief Exercise 3.36:
 *  Write a program to compare two arrays for equality. 
 *  Write a similar program to compare two vectors
 */

#include <iostream>
#include <iterator>
#include <vector>
using std::cin;
using std::cout;
using std::endl;
using std::begin;
using std::end;
using std::vector;

int main()
{
    int arr1[3] {1, 2, 3};
    int arr2[3] {2, 4, 6};

    bool areEqual = false;
    
    auto pb1 = begin(arr1);
    auto pe1 = end(arr1);
    auto pb2 = begin(arr2);
    auto pe2 = end(arr2);
    auto dist1 = pe1 - pb1;
    auto dist2 = pe2 - pb2;
    
    if(dist1 != dist2) {
        areEqual = false;
    }
    else {
        for(auto i = pb1, j = pb2; i!=pe1 && j != pe2; ++i, ++j) {
            if(*i != *j) {
                areEqual = false;
                break;
            }
        }
    }
    
    if(areEqual) {
        cout << "Arrays are equal" << endl;
    }
    else {
        cout << "Arrays are NOT equal" << endl;
    }
    
    vector<int> vec1 = {0, 1, 2};
    vector<int> vec2 = {0, 1, 2};

    if (vec1 == vec2)
        cout << "Vectors are equal." << endl;
    else
        cout << "Vectors are NOT equal." << endl;
    return 0;
}