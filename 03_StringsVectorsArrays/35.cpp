/**
 *  \file 35.cpp
 *  \brief Exercise 3.35:
 *  Using pointers, write a program to set the elements in an array to zero.
 */

#include <iostream>
#include <iterator>
using std::cin;
using std::cout;
using std::endl;
using std::begin;
using std::end;

int main()
{
    int arr[20];
    int *b = begin(arr);
    int *e = end(arr);
    for(int *i = b; i!=e; ++i) {
        *i = 0;
    }
    for(auto &i:arr) {
        cout << i << " ";
    }
    cout << endl;
    return 0;
}