/**
 *  \file 25.cpp
 *  \brief Exercise 6.25
 *  Write a main function that takes two arguments. Concatenate the supplied arguments and print the resulting string.
 */

#include <iostream>
using std::cout; 
using std::endl;
using std::cerr;
#include <string>
using std::string;

int main(int argc, char** argv)
{
    /* When you use the arguments in argv,
    remember that the optional arguments begin in argv[1];
    argv[0] contains the program's name, not user input.*/
    
    if(argc > 3) {
        cerr << "Argument count should be 2!" << endl;
        return -1; 
    }

    string str;
    for(int i = 1; i != argc; ++i) {
        str += argv[i];
        str += " ";
    }
    cout << str << endl;
    return 0;
}