/**
 *  \file 28.cpp
 *  \brief Exercise 6.28
 *  In the second version of error_msg that has an ErrCode parameter,
 *  what is the type of elem in the for loop?
 *  Info:
 *  Unlike vector, the elements in an initializer_list are always const values; 
 *  Answer:
 *  std::string const&
 */

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;

#include <initializer_list>
using std::initializer_list;

class ErrCode
{
private:
    int m_code;
public:    
    ErrCode(int code)
    {
        m_code = code;
    }
    
    string msg() const {
        return std::to_string(m_code);
    }
};

void error_msg(ErrCode e, initializer_list<string> il)
{
    cout << e.msg() << ": ";
    for (const auto &elem : il)
        cout << elem << " " ;
    cout << endl;
}

int main()
{
    string expected = "x";
    string actual = "y";
    if (expected != actual)
        error_msg(ErrCode(42), {"functionX", expected, actual});
    else
        error_msg(ErrCode(0), {"functionX", "okay"});        
}