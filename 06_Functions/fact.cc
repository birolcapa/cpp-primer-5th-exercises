#include "Chapter6.h"
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

void ask_factorial() {
    unsigned int input;
    cout << "Enter a number and see its factorial:";
    while(cin >> input)
    {
        unsigned int factorial = calculate_factorial(input);
        cout << input << "'s factorial is: " << factorial << endl;
        cout << input << "'s factorial is: " << calculate_factorial(input) << endl;
    }
}

unsigned int calculate_factorial(unsigned int input) { // please not that this can be overflowed easily
    if(input == 0 || input == 1) {return 1;}
    int result = 1;
    for(unsigned int i = 1; i<=input; ++i) {
        result *= i;
    }
    return result;    
}