/**
 *  \file 21.cpp
 *  \brief Exercise 6.21:
 *  Write a function that takes an int and a pointer to an int and returns
 *  the larger of the int value or the value to which the pointer points. 
 *  What type should you use for the pointer?
 */

#include <iostream>
using std::cout;
using std::endl;

int func(int a, int *b) {
    return (a > *b) ? a : *b;
}

int main()
{
    int a = 5;
    int x = 7;
    int *b = &x;
    cout << func(a, b) << endl;
    return 0;
}