Exercise 6.35: In the call to factorial, why did we pass val - 1 rather than val--?

int factorial(int val)
{
    if (val > 1)
        return factorial(val-1) * val;
    return 1;
}

Remember First:
val--:
The decrements val and yields a copy of the previous value of val as its result. 
Lets use like that:

auto temp = factorial(val) * val;
val--;
return temp;

As a result, it would be wrong.