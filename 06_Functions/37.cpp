/**
 *  \file 37.cpp
 *  \brief Exercise 6.37
 *  Write three additional declarations for the function in the previous exercise.
 *  One should use a type alias, one should use a trailing return, and the third
 *  should use decltype. Which form do you prefer and why?
 *  Exercise 6.36
 *  Write the declaration for a function that returns a reference to an array
 *  of ten strings, without using either a trailing return, decltype, or a type alias.
 */

#include < iostream>

/*
The most straightforward way is to use a type alias (§ 2.5.1, p. 67):
typedef int arrT[10]; // arrT is a synonym for the type array of ten ints
using arrT = int[10]; // equivalent declaration of arrT; see § 2.5.1 (p. 68)
arrT* func(int i); // func returns a pointer to an array of ten ints
*/

/*
Using a Trailing Return Type
// fcn takes an int argument and returns a pointer to an array of ten ints
auto func(int i) -> int(*)[10];
*/

/*
Using decltype
int odd[] = {1,3,5,7,9};
int even[] = {0,2,4,6,8};
// returns a pointer to an array of five int elements
decltype(odd) *arrPtr(int i)
{
    return (i % 2) ? &odd : &even; // returns a pointer to the array
}
*/

/*
As a concrete example, the following declares func without using a type alias:
int (*func(int i))[10];
To understand this declaration, it can be helpful to think about it as follows:
- func(int) says that we can call func with an int argument.
- (*func(int)) says we can dereference the result of that call.
- (*func(int))[10] says that dereferencing the result of a call to func
yields an array of size ten.
- int (*func(int))[10] says the element type in that array is int.
*/

#include<iostream>
#include<string>
using std::string;

string(&func())[10];

using ArrT = string[10];
ArrT& func_TypeAlias();

// This is the most basic one I think
auto func_TrailingType() -> string(&)[10];

string str[10] = {};
decltype(str) &func_decltype();

int main() {
    return 0;
}