/**
 *  \file 04.cpp
 *  \brief Exercise 6.4:
 *  Write a function that interacts with the user, asking for a number and
 *  generating the factorial of that number. Call this function from main.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

unsigned int calculate_factorial(unsigned int input) { // please not that this can be overflowed easily
    if(input == 0 || input == 1) {return 1;}
    unsigned int result = 1;
    for(unsigned int i = 1; i<=input; ++i) {
        result *= i;
    }
    return result;    
}

void ask_factorial() {
    unsigned int input;
    cout << "Enter a number and see its factorial:";
    while(cin >> input)
    {
        int factorial = calculate_factorial(input);
        cout << input << "'s factorial is: " << factorial << endl;
        cout << input << "'s factorial is: " << calculate_factorial(input) << endl;
    }
}

int main()
{
    ask_factorial();
    return 0;
}