/**
 *  \file 09.cpp
 *  \brief Exercise 6.9:
 *  Write your own versions of the fact.cc and factMain.cc files. These
 *  files should include your Chapter6.h from the exercises in the previous section. 
 *  Use these files to understand how your compiler supports separate compilation.
 *  How to compile: cl /W4 /EHsc 09.cpp fact.cc
 */

#include <iostream>
#include "Chapter6.h"

int main()
{
    ask_factorial();
    return 0;
}
