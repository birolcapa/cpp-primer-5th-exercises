/**
 *  \file 54.cpp
 *  \brief Exercise 6.54
 *  Write a declaration for a function that takes two int parameters and
 *  returns an int, and declare a vector whose elements have this function pointer type.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

int test_function(int, int);

int (*ptf)(int, int); // ptftype1 points to the test_function
typedef int (*ptftype1)(int, int);
typedef decltype(test_function) *ptftype2;
using ptftype3 = int (*)(int, int);
using ptftype4 = decltype(test_function) *;

int main()
{
    vector<ptftype1> v1;
    v1.push_back(test_function);
    cout << "result of calling v1[0](1, 2) is " << v1[0](1, 2) << endl;

    vector<ptftype2> v2;
    v2.push_back(test_function);
    cout << "result of calling v2[0](2, 4) is " << v2[0](2, 4) << endl;

    vector<ptftype3> v3;
    v3.push_back(test_function);
    cout << "result of calling v3[0](6, 8) is " << v3[0](6, 8) << endl;

    vector<ptftype4> v4;
    v4.push_back(test_function);
    cout << "result of calling v4[0](16, 28) is " << v4[0](16, 28) << endl;

    return 0;
}

int test_function(int a, int b) {
  cout << "Called test_function(" << a << ", " << b << ")" << std::endl;
  return a+b;
}