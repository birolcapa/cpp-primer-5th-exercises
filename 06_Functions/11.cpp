/**
 *  \file 11.cpp
 *  \brief Exercise 6.11:
 *  Write and test your own version of reset that takes a reference.
 */

#include<iostream>
using std::cout;
using std::cin;
using std::endl;

void reset(int& i) {
    i = 0;
}

int main()
{
    int i = 89;
    cout << i << endl;
    reset(i);
    cout << "After reset" << endl;
    cout << i << endl;
    return 0;
}