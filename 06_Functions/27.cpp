/**
 *  \file 27.cpp
 *  \brief Exercise 6.27
 *  Write a function that takes an initializer_list<int>
 *  and produces the sum of the elements in the list.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <initializer_list>
using std::initializer_list;

int initlistexample(initializer_list<int> inputs) {
    int sum = 0;
    for(auto elem : inputs) {
        sum += elem;
    }
    return sum;
}

int main()
{
    int result = initlistexample({4, 5, 6, 7, 8});
    cout << result << endl;
}