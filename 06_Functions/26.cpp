/**
 *  \file 26.cpp
 *  \brief Exercise 6.26
 *  Write a program that accepts the options presented in this section.
 *  Print the values of the arguments passed to main.
 */

#include <iostream>
using std::cout; 
using std::endl;
using std::cerr;
#include <string>
using std::string;

int main(int argc, char** argv)
{
    /* When you use the arguments in argv,
    remember that the optional arguments begin in argv[1];
    argv[0] contains the program’s name, not user input.*/
    
    string str;
    for(int i = 1; i != argc; ++i) {
        str += argv[i];
        str += " ";
    }
    cout << str << endl;
    return 0;
}