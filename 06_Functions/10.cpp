/**
 *  \file 10.cpp
 *  \brief Exercise 6.10:
 *  Using pointers, write a function to swap the values of two ints. 
 *  Test the function by calling it and printing the swapped values.
 */

#include<iostream>
using std::cout;
using std::cin;
using std::endl;

// a is a const ptr to int
// b is a const ptr to int
void swapIntegersConst(int* const a, int* const b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void swapIntegers(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    int a, b;
    cout << "Enter 2 numbers: " << endl;
    cin >> a >> b;
    cout << "a is: " << a << " b is: " << b << endl;
    swapIntegers(&a, &b);
    cout << "After swap: "<< endl;
    cout << "a is: " << a << " b is: " << b << endl;
    swapIntegersConst(&a, &b);
    cout << "After swap: "<< endl;
    cout << "a is: " << a << " b is: " << b << endl;
}