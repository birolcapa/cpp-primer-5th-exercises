#ifndef CHAPTER_6_H
#define CHAPTER_6_H

void ask_factorial();
unsigned int absolute_val(int val);
unsigned int calculate_factorial(unsigned int input);

#endif