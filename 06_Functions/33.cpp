/**
 *  \file 33.cpp
 *  \brief Exercise 6.33
 *  Write a recursive function to print the contents of a vector
 */

#include < iostream>
using std::cout;
using std::endl;
#include <vector>
using std::vector;

void print(const vector<int>::iterator beginPoint, const vector<int>::iterator endPoint){
    if(beginPoint != endPoint) {
        cout << *beginPoint << " ";
        print( beginPoint+1, endPoint);
    }
    return;
}

int main() {
    vector<int> x {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    print(begin(x), end(x));
    return 0;
}