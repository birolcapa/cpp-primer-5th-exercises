/**
 *  \file 18.cpp
 *  \brief Exercise 6.18:
 *  Write declarations for each of the following functions. 
 *  When you write these declarations, 
 *  use the name of the function to indicate what the function does.
 *  (a) A function named compare that returns a bool and has two parameters 
 *  that are references to a class named matrix.
 *  (b) A function named change_val that returns a vector<int> iterator 
 *  and takes two parameters: One is an int and the other is an iterator for a vector<int>
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include <vector>
using std::vector;

class Matrix {
};

bool compare(const Matrix& m1, const Matrix& m2);
vector<int>::iterator change_val(int x, vector<int>::iterator y);

int main()
{
    return 0;
}