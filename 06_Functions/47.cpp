/**
 *  \file 47.cpp
 *  \brief Exercise 6.47
 *  Revise the program you wrote in the exercises in 6.3.2 (p. 228) that
 *  used recursion to print the contents of a vector to conditionally print information
 *  about its execution. For example, you might print the size of the vector on each call.
 *  Compile and run the program with debugging turned on and again with it turned off.
 *  Answer:
 *  turn off debug messages by    defining    NDEBUG cl /EHsc /W4 /D NDEBUG 47.cpp
 *  turn on debug messages by not defining    NDEBUG cl /EHsc /W4 47.cpp
 */

#include < iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;


void print(const vector<int>::iterator beginPoint, const vector<int>::iterator endPoint){
    #ifndef NDEBUG
    // _ _func_ _ is a local static defined by the compiler that holds the function’s name
    cout << "File: " << __FILE__
        << " : in function " << __func__ << " at line " << __LINE__ << endl
        << " Compiled on " << __DATE__ << " at " << __TIME__ << endl
        << ": vector size is " << endPoint-beginPoint << endl;
    #endif
    if(beginPoint != endPoint) {
        cout << *beginPoint << " ";
        print( beginPoint+1, endPoint);
    }
    return;
}

int main() {
    vector<int> x {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    print(begin(x), end(x));
    return 0;
}