/**
 *  \file 29.cpp
 *  \brief Exercise 6.29
 *  When you use an initializer_list in a range for would you ever
 *  use a reference as the loop control variable? If so, why? If not, why not?
 *  Answer:
 *  Yes, I would use as a referece. 
 *  But I would also remember, references are good when copy cost is expensive.
 *  If copy cost is cheap, reference may not be needed.
 *  Therefore, Ex 27 does not use a reference while Ex 28 and 29 use. 
 */
#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;

#include <initializer_list>
using std::initializer_list;

class ErrCode
{
private:
    int m_code;
public:    
    ErrCode(int code)
    {
        m_code = code;
    }
    
    string msg() const {
        return std::to_string(m_code);
    }
};

void error_msg(ErrCode e, initializer_list<string> il)
{
    cout << e.msg() << ": ";
    for (const auto &elem : il)
        cout << elem << " " ;
    cout << endl;
}

int main()
{
    string expected = "x";
    string actual = "y";
    if (expected != actual)
        error_msg(ErrCode(42), {"functionX", expected, actual});
    else
        error_msg(ErrCode(0), {"functionX", "okay"});        
}