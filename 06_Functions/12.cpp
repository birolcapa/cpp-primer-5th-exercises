/**
 *  \file 12.cpp
 *  \brief Exercise 6.12:
 *  Rewrite the program from exercise 6.10 in § 6.2.1 (p. 210) to use references
 *  instead of pointers to swap the value of two ints.
 *  Which version do you think would be easier to use and why?
 *  Reference version is easier since it has less syntax.
 */

#include<iostream>
using std::cout;
using std::cin;
using std::endl;

void swapIntegers(int &a, int &b) {
    int temp = a;
    a = b;
    b = temp;
}

int main()
{
    int a, b;
    cout << "Enter 2 numbers: " << endl;
    cin >> a >> b;
    cout << "a is: " << a << " b is: " << b << endl;
    swapIntegers(a, b);
    cout << "After swap: "<< endl;
    cout << "a is: " << a << " b is: " << b << endl;
}