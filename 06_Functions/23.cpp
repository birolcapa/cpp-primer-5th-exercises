/**
 *  \file 23.cpp
 *  \brief Exercise 6.23:
 *  Write your own versions of each of the print functions presented in
 *  this section. Call each of these functions to print i and j defined as follows:
 *  int i = 0, j[2] = {0, 1};
 *  Info:
 *  Using a Marker to Specify the Extent of an Array
 *  Using the Standard Library Conventions
 *  Explicitly Passing a Size Parameter
 */

#include <iostream>
using std::cout;
using std::endl;
using std::begin;
using std::end;

void print(const int i) {
    cout << "void print(const int i)" << endl;
    cout << i << endl;
}

void print(const int* beg, const int* end) {
    cout << "void print(const int* beg, const int* end)" << endl;
    while(beg != end) {
        cout << *beg++ << " ";
    }
    cout << endl;
}

void print(const int (&arr)[2]) {
    cout << "void print(const int (&arr)[2])" << endl;
    for(auto elem : arr) {
        cout << elem << " ";
    }
    cout << endl;
}

void print(const int arr[], size_t size) {
    cout << "void print(const int arr[], size_t size)" << endl;
    for(size_t i = 0; i < size ; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main()
{
    int i = 0, j[2] = {0, 1};
    print(i);
    print(begin(j), end(j));
    print(j);
    size_t size = end(j) - begin(j);
    print(j, size);
    return 0;
}