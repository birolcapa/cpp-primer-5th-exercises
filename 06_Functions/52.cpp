/**
 *  \file 52.cpp
 *  \brief Exercise 6.52
 *  Given the following declarations,
 *  void manip(int, int);
 *
 *  what is the rank (6.6.1, p. 245) of each conversion in the following calls?
 *  (a) manip('a', 'z'); (b) manip(55.4, dobj);
 */

void manip(int, int);
double dobj;

manip('a', 'z'); // 3. Match through a promotion (4.11.1, p. 160).
manip(55.4, dobj); // 4. Match through an arithmetic (4.11.1, p. 159) or pointer conversion (§ 4.11.2, p. 161).