/**
 *  \file 03.cpp
 *  \brief Exercise 6.3:
 *  Write and test your own version of fact.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

unsigned int calculate_factorial(unsigned int input) {
    if(input == 0 || input == 1) return 1;
    unsigned int result = 1;
    for(unsigned int i = 1; i<=input; ++i) {
        result *= i;
    }
    return result;
}

int main()
{
    unsigned int input;
    cout << "Enter a number and see its factorial:";
    while(cin >> input)
    {
        unsigned int factorial = calculate_factorial(input);
        cout << input << "'s factorial is: " << factorial << endl;
        cout << input << "'s factorial is: " << calculate_factorial(input) << endl;
    }
    return 0;
}