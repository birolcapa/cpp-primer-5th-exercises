/**
 *  \file 17.cpp
 *  \brief Exercise 6.17:
 *  Write a function to determine whether a string contains any capital
 *  letters. Write a function to change a string to all lowercase.
 *  Do the parameters you used in these functions have the same type?
 *  If so, why? If not, why not?
 */
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include <string>
using std::string;

bool has_capital_letter(const string& input) {
    for(auto& c : input) {
        if(isupper(c)) {return true;}
    }
    return false;
}

void make_lowercase(string& input) {
    for(auto& c : input) {
        if(isupper(c)) {
            c = static_cast<char>(tolower(c));
        }
    }
}

int main()
{
    string str{"Hello World!"};
    cout << str 
        << (has_capital_letter(str) ? " has " : " doesn't have") 
        << " capital letters" << endl;
    make_lowercase(str);
    cout << str 
        << (has_capital_letter(str) ? " has " : " doesn't have") 
        << " capital letters" << endl;
    return 0;
}