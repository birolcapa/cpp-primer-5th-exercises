/**
 *  \file 32.cpp
 *  \brief Exercise 6.32
 *  Indicate whether the following function is legal. If so, explain what it
 *  does; if not, correct any errors and then explain it.
 *  Answer: Legal
 *  When the object is not a local object, then we may return a reference to it.
 *  Get returns a reference to ith element of ia.
 *  The reference is an lvalue, so it can be used to assign i to it. 
 */

#include < iostream>

int &get(int *arry, int index) 
{ 
    return arry[index];
}

int main() {
    int ia[10];
    for (int i = 0; i != 10; ++i) {
        get(ia, i) = i;
    }
    for(auto elem : ia) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;
    return 0;
}