/**
 *  \file 46.cpp
 *  \brief Exercise 6.46
 *  Would it be possible to define isShorter as a constexpr?
 *  If so, do so. If not, explain why not.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

// Note:
// A constexpr function body may contain other statements so long as those
// statements generate no actions at runtime.
// size() can calculate regarding value at runtime
// As a result:
//error C3615: constexpr function 'isShorter' cannot result in a constant expression
// compare the length of two strings
constexpr bool isShorter(const string &s1, const string &s2)
{
    return s1.size() < s2.size();
}

int main()
{
    bool result = isShorter("test", "test2");
    cout << result << endl;
    return 0;
}