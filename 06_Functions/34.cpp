/**
 *  \file 34.cpp
 *  \brief Exercise 6.34
 *  What would happen if the stopping condition in factorial were
 *  if (val != 0)
 *  Answer:
 *  If val is positive, recursion will stop when val is zero.
 *  If val is negative, recursion will never stop.
 *  factorial(-5) -> factorial(-6) * -5
 *  factorial(-6) -> factorial(-7) * -6
 *  factorial(-7) -> factorial(-8) * -7
 *  ...
 *  The function will recurse "forever", meaning that the
 *  function will continue to call itself until the program stack is exhausted.
 */

#include < iostream>

// calculate val!, which is 1 * 2 * 3 . . . * val
int factorial(int val)
{
    if (val != 0)//if (val > 1)
        return factorial(val-1) * val;
    return 1;
}

int main() {
    std::cout << factorial(5) << std::endl;
    std::cout << factorial(-5) << std::endl;
    return 0;
}