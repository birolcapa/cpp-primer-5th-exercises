/**
 *  \file 08.cpp
 *  \brief Exercise 6.8:
 *  Write a header file named Chapter6.h that contains declarations for
 *  the functions you wrote for the exercises in § 6.1 (p. 205).
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include "Chapter6.h"


void ask_factorial() {
    unsigned int input;
    cout << "Enter a number and see its factorial:";
    while(cin >> input)
    {
        unsigned int factorial = calculate_factorial(input);
        cout << input << "'s factorial is: " << factorial << endl;
        cout << input << "'s factorial is: " << calculate_factorial(input) << endl;
    }
}

int main()
{
    ask_factorial();
    return 0;
}

unsigned int absolute_val(int val) {
    return ((val > 0) ? val : -val);
}

unsigned int calculate_factorial(unsigned int input) { // please not that this can be overflowed easily
    if(input == 0 || input == 1) {return 1;}
    int result = 1;
    for(unsigned int i = 1; i<=input; ++i) {
        result *= i;
    }
    return result;    
}