/**
 *  \file 48.cpp
 *  \brief Exercise 6.48
 *  Explain what this loop does and whether it is a good use of assert:
 *  The loop is used to read the input until input equals sought.
 *  After loop cin checked with assert.
 *  Assert will always happen when user sends EOF.
 *  But this may happen, no need to assert this. Bad use of assert.
 *  The assert macro is often used to check for conditions that "cannot happen."
 *  assert(expr);
 *  evaluates expr and if the expression is false (i.e., zero),
 *  then assert writes a message and terminates the program.
 *  If the expression is true (i.e., is nonzero), then assert does nothing.
 *  The behavior of assert depends on the status of a preprocessor variable named
 *  NDEBUG. If NDEBUG is defined, assert does nothing. By default, NDEBUG is not
 *  defined, so, by default, assert performs a run-time check.
 *  Since assert(cin); will evaluate EOF, it will terminate the
 */
#include < iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include <cassert>

int main() {
    string s;
    string sought = "test";
    while (cin >> s && s != sought) { } // empty body
    assert(cin);
    cout << sought << " found" << endl;
    return 0;
}

