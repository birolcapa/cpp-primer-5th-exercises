/**
 *  \file 36.cpp
 *  \brief Exercise 6.36
 *  Write the declaration for a function that returns a reference to an array
 *  of ten strings, without using either a trailing return, decltype, or a type alias.
 */

#include < iostream>

/*
As a concrete example, the following declares func without using a type alias:
int (*func(int i))[10];
To understand this declaration, it can be helpful to think about it as follows:
- func(int) says that we can call func with an int argument.
- (*func(int)) says we can dereference the result of that call.
- (*func(int))[10] says that dereferencing the result of a call to func
yields an array of size ten.
- int (*func(int))[10] says the element type in that array is int.
*/

#include<iostream>
#include<string>
using std::string;

string(&func())[10];

int main() {
    return 0;
}