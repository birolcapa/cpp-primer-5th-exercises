/**
 *  \file 05.cpp
 *  \brief Exercise 6.5:
 *  Write a function to return the absolute value of its argument.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

unsigned int absolute_val(int val) {
    return ((val > 0) ? val : -val);
}

int main()
{
    cout << "Enter a value to see its absolute: " << endl;
    int input;
    while(cin >> input) {
    cout << absolute_val(input) << endl;
    }
    return 0;
}