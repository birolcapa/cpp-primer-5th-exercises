/**
 *  \file 22.cpp
 *  \brief Exercise 6.22: Write a function to swap two int pointers.
 */

#include <iostream>
using std::cout;
using std::endl;

int func(int a, int *b) {
    return (a > *b) ? a : *b;
}

void swapIntegerPointers(int **a, int **b) {
    int *temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    int x = 5;
    int y = 7;
    int *px = &x;
    int *py = &y;
    cout << "px = " << px << " *px = " << *px << endl;
    cout << "py = " << py << " *py = " << *py << std::endl;
    
    // memory plan: |x = 5        |y = 7        |
    // adresses     |0x20         |0x24         |
    // memory plan: |px = 0x20    |py = 0x24    | -> *px = (*0x20) = x = 5 ...
    // address        |0x56         |0x60         |
    swapIntegerPointers(&px, &py);
    // so when I changed the addresses of two pointers
    // address        |0x56         |0x60         | would become
    // address        |0x60         |0x56         |
    // lets think again
    
    // memory plan: |x = 7        |y = 5        |
    // adresses     |0x24         |0x20         |
    // memory plan: |px = 0x24    |py = 0x20    | -> *px = (*0x24) = x = 7 ...
    // address        |0x60         |0x56         |
    cout << "After swap:"<< endl;
    cout << "px = " << px << " *px = " << *px << endl;
    cout << "py = " << py << " *py = " << *py << std::endl;
    return 0;
}