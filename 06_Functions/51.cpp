/**
 *  \file 51.cpp
 *  \brief Exercise 6.51
 *  Write all four versions of f.
 *  Each function should print a distinguishing message.
 *  Check your answers for the previous exercise.
 *  If your answers were incorrect,
 *  study this section until you understand why your answers were wrong.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

void f(){
    cout << "f()" << endl;
}

void f(int param){
    cout << __func__ << " " << param << endl;
}

void f(int param1, int param2){
    cout << __func__ << " " << param1 << " " << param2 << endl;
}
void f(double param1, double param2 = 3.14) {
    cout << __func__ << " " << param1 << " " << param2 << endl;
}

int main()
{
    // f(2.56, 42); // illegal 51.cpp(35): error C2666: 'f': 2 overloads have similar conversions
    /* The call is ambiguous, because either f(int, int) or f(double, double)
    is a better match than the other on one of the arguments to the call.*/

    f(42); // legal
    /*The best match is f(int);*/

    f(42, 0); // legal
    /*The best match is f(int, int);*/

    f(2.56, 3.14); // legal
    /*The best match is f(double, double = 3.14);*/

    return 0;
}