/**
 *  \file 42.cpp
 *  \brief Exercise 6.42
 *  Give the second parameter of make_plural (6.3.2, p. 224) a default
 *  argument of ’s’. Test your program by printing singular and plural versions of the
 *  words success and failure.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;
// return the plural version of word if ctr is greater than 1
string make_plural(size_t ctr, const string &word, const string &ending = "s")
{
    return (ctr > 1) ? word + ending : word;
}

int main()
{
    string success {"success"};
    cout << success << ": plural form is " <<make_plural(2, success, "es") << endl;
    string failure {"failure"};
    cout << failure << ": plural form is " <<make_plural(2, failure) << endl;
    return 0;
}