/**
 *  \file 55.cpp
 *  \brief Exercise 6.55
 *  Write four functions that add, subtract, multiply, and divide two int
 *  values. Store pointers to these functions in your vector from the previous exercise.
 */

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#include <vector>
using std::vector;

int add(int a, int b);
int subtract(int a, int b);
int multiply(int a, int b);
int divide(int a, int b);

typedef int (*ptf)(int, int);

int main()
{
    vector<ptf> v1;
    v1.push_back(add);
    v1.push_back(subtract);
    v1.push_back(multiply);
    v1.push_back(divide);
    int x = 50;
    int y = 24;
    for(const auto& elem : v1) {
        cout << elem(x, y) << endl;
    }

    x = 56;
    y = 0;
    for(const auto& elem : v1) {
        cout << elem(x, y) << endl;
    }
    return 0;
}

int add(int a, int b) {
    cout << "Called " <<  __func__ << "(" << a << ", " << b << "): ";
    return a + b;
}

int subtract(int a, int b) {
    cout << "Called " <<  __func__ << "(" << a << ", " << b << "): ";
    return a - b;
}

int multiply(int a, int b) {
    cout << "Called " <<  __func__ << "(" << a << ", " << b << "): ";
    return a * b;
}

int divide(int a, int b) {
    cout << "Called " <<  __func__ << "(" << a << ", " << b << "): ";
    try {
        if(b == 0) {
            throw std::runtime_error("Divide by zero!");
        }
        return a/b;
    }
    catch(std::runtime_error err) {
        cerr << err.what() << " ";
    }
    return 0;
}