/**
 *  \file 38.cpp
 *  \brief Exercise 6.38
 *  Revise the arrPtr function on to return a reference to the array.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

int odd[] = {1,3,5,7,9};
int even[] = {0,2,4,6,8};
// returns a pointer to an array of five int elements
decltype(odd) &arrPtr(int i)
{
    return (i % 2) ? odd : even; // returns a pointer to the array
}

int main()
{
    int (&arrOdd)[5] = arrPtr(1);
    for(auto elem: arrOdd) {
        cout << elem << " ";
    }
    cout << endl;
    int (&arrEven)[5] = arrPtr(2);
    for(auto elem: arrEven) {
        cout << elem << " ";
    }
    cout << endl;
    return 0;
}