/**
 *  \file 44.cpp
 *  \brief Exercise 6.44
 *  Rewrite the isShorter function from § 6.2.2 (p. 211) to be inline.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

// compare the length of two strings
inline bool isShorter(const string &s1, const string &s2)
{
    return s1.size() < s2.size();
}

int main()
{
    bool result = isShorter("test", "test2");
    cout << result << endl;
    return 0;
}