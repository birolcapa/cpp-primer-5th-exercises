/**
 *  \file 9.cpp
 *  \brief Exercise 2.9:
 *  Explain the following definitions. For those that are illegal, 
 *  explain what’s wrong and how to correct it.
 */

#include <iostream>

int main()
{
    //std::cin >> int input_value; // 9.cpp(5): error C2062: type 'int' unexpected
    int input_value;
    std::cin >> input_value;
    
    // int i = { 3.14 }; // 9.cpp(9): error C2397: conversion from 'double' to 'int' requires a narrowing conversion
    // double salary = wage = 9999.99; // 9.cpp(10): error C2065: 'wage': undeclared identifier
    int i = 3.14; // i is 3
    std::cout << i << std::endl;
    return 0;
}