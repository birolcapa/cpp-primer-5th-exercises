/**
 *  \file 30.cpp
 *  \brief 
 *  We use the term top-level const to
 *  indicate that the pointer itself is a const. 
 *  
 *  When a pointer can point to a const
 *  object, we refer to that const as a low-level const
 *  
 *  Exercise 2.30: For each of the following
 *  declarations indicate whether the object being
 *  declared has top-level or low-level const.
 *  const int v2 = 0; 
 *  int v1 = v2;
 *  int *p1 = &v1, &r1 = v1;
 *  const int *p2 = &v2, *const p3 = &i, &r2 = v2;
 *  
 *  v2 is top level const
 *  p2 is low level const
 *  p3 right most top level. left most low level
 *  r2 is low level
 */
 
 
