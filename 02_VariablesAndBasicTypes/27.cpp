/**
 *  \file 27.cpp
 *  \brief Exercise 2.27:
 *  Which of the following initializations are legal? Explain why
 */

#include <iostream>

int main()
{
    {// a
        //int i = -1, &r = 0; // illegal, r must refer to an object. 
        // a nonconst reference cannot be initialized to a literal
    }
    
    {// b
        int i2;
        int *const p2 = &i2; // legal
    }
    
    {// c
        const int i = -1, &r = 0; // legal
    }
    
    {// d
        int i2;
        const int *const p3 = &i2; // legal
    }
    
    {// e
        int i2;
        const int *p1 = &i2; // legal
    }
    
    {// f
        //const int &const r2; // illegal
        /*
        27.cpp(33): warning C4227: anachronism used: qualifiers on reference are ignored
        27.cpp(33): error C2530: 'r2': references must be initialized
        */
    }
    
    {// g
        int i;
        const int i2 = i, &r = i; //  warning C4700: uninitialized local variable 'i' used
    }
    return 0;
}