/**
 *  \file 16.cpp
 *  \brief Exercise 2.16: Which, if any, of the following assignments are invalid? 
 *  If they are valid, explain what they do
 */

#include <iostream>

int main()
{
    int i = 0, &r1 = i; // ok
    double d = 0, &r2 = d; // ok
    r2 = 3.14159; // d becomes 3.14159
    std::cout << d << std::endl;
    r2 = r1; // d becomes 0 because i = 0
    std::cout << d << std::endl;
    i = r2; // i beomes 0 again because d is 0
    std::cout << i << std::endl;
    r1 = d; // i becomes 0 again becase d is 0
    std::cout << i << std::endl;
    return 0;
}