Exercise 2.23: Given a pointer p, can you determine whether p points to a valid object?
If so, how? If not, why not?

A Valid Object: Not we cannot. Because more information is needed to determine whether the pointer is valid or not. 
It would be expensive to maintain meta data about what constitutes a valid pointer and what does not, and in Cpp you dont pay for what you dont want. 

https://stackoverflow.com/questions/17202570/c-is-it-possible-to-determine-whether-a-pointer-points-to-a-valid-object/17202622#17202622

https://github.com/pezy/CppPrimer/tree/master/ch02

However, a smart pointer can be used to tell if it points to a valid object.

