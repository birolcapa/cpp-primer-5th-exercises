Exercise 2.22: Assuming p is a pointer to int, explain the following code:
if (p) // It means if the pointer p is not null
if (*p) // It means the object pointed by pointer is not false 
        // (in other words the object is not null or zero)