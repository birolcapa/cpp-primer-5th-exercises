/**
 *  \file 11.cpp
 *  \brief Exercise 2.11:
 *  Explain whether each of the following is a declaration or a definition:
 */

#include <iostream>

extern int ix = 1024; // Definition
int iy; // Declaration: declares and defines iy
extern int iz; // Declaration: Declares but does not define iz
int main()
{
    // It is an error to provide an initializer on an extern inside a function.

    // extern int ix2 = 1024; //  error C2205: 'ix2': cannot initialize extern variables with block scope
    return 0;
}