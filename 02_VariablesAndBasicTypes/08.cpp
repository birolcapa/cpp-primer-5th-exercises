/**
 *  \file 8.cpp
 *  \brief Exercise 2.6:
 *  What, if any, are the differences between the following definitions:
 */
#include <iostream>

int main()
{
    std::cout << "Hi 2\x4d\n" << std::endl;
    
    std::cout << "2M\n" << std::endl;
    std::cout << "2\tM\n" << std::endl;
    return 0;
}