/**
 *  \file 6.cpp
 *  \brief Exercise 2.6:
 *  What, if any, are the differences between the following definitions:
 */

#include <iostream>

int main()
{
    int month = 9, day = 7;
    int month1 = 09, day2 = 07; // error C2041: illegal digit '9' for base '8'
    return 0;
}