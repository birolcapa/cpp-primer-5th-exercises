/**
 *  \file 24.cpp
 *  \brief Exercise 2.24:
 *  Why is the initialization of p legal but that of lp illegal?
 */

#include <iostream>

int main()
{
    int i = 42; 
    void *p = &i; // a void pointer can point to any type. 
    // long *lp = &i; // Error: error C2440: 'initializing': cannot convert from 'int *' to 'long *'
    // a long * pointer cannot point to int* pointer
    // first cast int* pointer then dereference it.
    std::cout << *(int*)(p) << std::endl;
    return 0;
}