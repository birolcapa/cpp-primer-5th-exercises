/**
 *  \file 36.cpp
 *  \brief Exercise 2.36:
 *  In the following code, determine the type of each variable and the value
 *  each variable has when the code finishes:
 *  int a = 3, b = 4;
 *  decltype(a) c = a;
 *  decltype((b)) d = a;
 *  ++c;
 *  ++d;
 */
 
 #include <iostream>

int main()
{
    int a = 3, b = 4;
    decltype(a) c = a; // c is int 3
    decltype((b)) d = a; // d is int& , bind to a, 
    ++c;
    std::cout << "c is " << c << std::endl;
    ++d;
    std::cout << "d is " << d << std::endl;
    return 0;
}