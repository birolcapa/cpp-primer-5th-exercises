/**
 *  \file 41.cpp
 *  \brief Exercise 2.41:
 *  Use your Sales_data class to rewrite the exercises in 1.5.1 
 *  (p. 22 - Exercise 1.20 , 21, 22), 1.5.2 (p. 24) (exercises 23, 24), and 
 *  1.6 (p. 25) (exercises 25). 
 *  For now, you should define your Sales_data class 
 *  in the same file as your main function.
 */

#include <iostream>
#include <string>

struct Sales_data {
    std::string bookNo;
    unsigned int units_sold = 0;
    double revenue = 0.0;
};

void ex_1_5_1() {
    Sales_data item;
    double price = 0;
    std::cin >> item.bookNo >> item.units_sold >> price;
    item.revenue = item.units_sold * price;
    std::cout << item.bookNo << " " << item.units_sold << " " << item.revenue << std::endl;
}

int ex_1_5_2() {
    Sales_data item1, item2;
    double price1, price2;
    std::cin >> item1.bookNo >> item1.units_sold >> price1;
    std::cin >> item2.bookNo >> item2.units_sold >> price2;
    item1.revenue = item1.units_sold * price1;
    item2.revenue = item2.units_sold * price2;
    if(item1.bookNo == item2.bookNo) {
        unsigned totalCnt = item1.units_sold + item2.units_sold;
        double totalRevanue = item1.revenue + item2.revenue;
        std::cout << item1.bookNo << " " << item1.units_sold << " " << item1.revenue << std::endl;
        if(totalCnt != 0) {
            std::cout << "Average value is " << totalRevanue / totalCnt << std::endl;
        }
        else {
            std::cout << "no sales" << std::endl;
        }
    }
    else {
        std::cerr << "Data must refer to same ISBN" << std::endl;
        return -1;
    }
    return 0;
}

int ex_1_6() {
    Sales_data total;
    double totalPrice = 0.0;
    if(std::cin >> total.bookNo >> total.units_sold >> totalPrice) {
        total.revenue = total.units_sold * totalPrice;
        Sales_data trans;
        double transPrice = 0.0;
        while(std::cin >> trans.bookNo >> trans.units_sold >> transPrice) {
            trans.revenue = trans.units_sold * transPrice;
            if(total.bookNo == trans.bookNo) {
                total.units_sold += trans.units_sold;
                total.revenue += trans.revenue;
            }
            else {
                std::cout << total.bookNo << " " << total.units_sold << " revenue " << total.revenue << std::endl;
                if(total.units_sold != 0) {
                    std::cout << "Average value is " << total.revenue / total.units_sold << std::endl;
                }
                else {
                    std::cout << "no sales" << std::endl;
                }
                
                total.bookNo = trans.bookNo;
                total.units_sold = trans.units_sold;
                total.revenue = trans.revenue;
            }
        }
        std::cout << total.bookNo << " " << total.units_sold << " revenue " << total.revenue << std::endl;
        if(total.units_sold != 0) {
            std::cout << "Average value is " << total.revenue / total.units_sold << std::endl;
        }
        else {
            std::cout << "no sales" << std::endl;
        }
        
        return 0;
    }
    else {
        std::cerr << "No Data" << std::endl;
        return -1;
    }
    
}
int main()
{
    //ex_1_5_1();
    //int returnVal = ex_1_5_2();
    int returnVal = ex_1_6();
    return returnVal;
}