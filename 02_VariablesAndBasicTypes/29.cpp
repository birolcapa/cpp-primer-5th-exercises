/**
 *  \file 28.cpp
 *  \brief Exercise 2.29: Uing the variables in the
 *  previous exercise, which of the following
 *  assignments are legal? Explain why.
 *  (a) i = ic; legal
 *  (b) p1 = p3; illegal, pc is a const pointer to const integer
 *  (c) p1 = &ic; illegal, ic is a const int 
 *  (d) p3 = &ic; p3 is a const pointer
 *  (e) p2 = p1; p2 is a const pointer
 *  (f) ic = *p3;ic is a const int
 *   
 */
 
#include <iostream>

int main()
{
    // int i, *const cp; // error C2734: 'cp': 'const' object must be initialized if not 'extern'

    // int *p1, *const p2; // error C2734: 'p2': 'const' object must be initialized if not 'extern'
    
    // const int ic, &r = ic; // error C2734: 'ic': 'const' object must be initialized if not 'extern'
    
    // const int *const p3; // error C2734: 'p3': 'const' object must be initialized if not 'extern'
    
    const int *p; // legal a pointer to const int
    return 0;
}