/**
 *  \file 20.cpp
 *  \brief Exercise 2.20:
 *  What does the following program do?
 */

#include <iostream>

int main()
{
    int i = 42;
    int *p1 = &i;
    *p1 = *p1 * *p1;
    std::cout << "*p1 is " << *p1 << std::endl; // 42 * 42 = 1764
    return 0;
}