/**
 *  \file 26.cpp
 *  \brief Exercise 2.26:
 *  Which of the following are legal? For those that are illegal, explain why.
 */

#include <iostream>

int main()
{
    // const int buf; // Error: error C2734: 'buf': 'const' object must be initialized if not 'extern'
    int cnt = 0; // Valid
    const int sz = cnt; // Valid
    ++cnt; // Valid
    // ++sz; // Error: error C3892: 'sz': you cannot assign to a variable that is const
    return 0;
}