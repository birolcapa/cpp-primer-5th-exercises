/**
 *  \file 10.cpp
 *  \brief Exercise 2.10:
 *  What are the initial values, if any, of each of the following variables?
 */

#include <iostream>

std::string global_str; // empty string object
int global_int; // 0
int main()
{
    int local_int; // undefined: warning C4700: uninitialized local variable 'local_int' used
    std::cout << local_int << std::endl;
    std::string local_str; // empty string object
    return 0;
}