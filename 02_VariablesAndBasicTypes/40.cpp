/**
 *  \file 40.cpp
 *  \brief Exercise 2.40:
 *  Write your own version of the Sales_data class
 */

#include <iostream>
struct Sales_data {
    std::string bookNo;
    unsigned unit_sold = 0;
    double revenue = 0.0;
};

int main()
{
    return 0;
}