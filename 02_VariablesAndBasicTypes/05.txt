Exercise 2.5: Determine the type of each of the following literals. Explain the differences among the literals in each of the four examples:
(a) ’a’, L’a’, "a", L"a"
(b) 10, 10u, 10L, 10uL, 012, 0xC
(c) 3.14, 3.14f, 3.14L
(d) 10, 10u, 10., 10e-2

’a’:     character literal, type is char 
L’a’:    wide character literal, w_char_t
"a",:    character string literal
L"a":    wide character string literal

10:        integral type, int
10u:    unsigned integral literal, unsigned integral
10L:    integral literal, long int
10uL:    unsigned integral literal, unsigned long int
012:    octal integral literal, int
0xC:    hexadecimal integral literal, int

3.14:    floating-point literal, double
3.14f:    floating-point literal, float
3.14L:    floating-point literal, long double

10:        integral literal, int
10u:    unsigned integral literal, unsigned int
10.:    floating-point literal, type is double
10e-2:    floating-point literal, type is double