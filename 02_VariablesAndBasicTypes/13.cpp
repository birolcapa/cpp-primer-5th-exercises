/**
 *  \file 13.cpp
 *  \brief Exercise 2.13:
 *  What is the value of j in the following program?
 */
#include <iostream>

int i = 42;
int main()
{
    int i = 100;
    int j = i; // 100
    std::cout << j << std::endl; 
    return 0;
}