/**
 *  \file 18.cpp
 *  \brief Exercise 2.18:
 *  Write code to change the value of a pointer.
 *  Write code to change the value to which the pointer points.
 */

#include <iostream>

int main()
{
    int i = 16;
    std::cout << "i is " << i << std::endl; // 16
    int *ip = &i;
    *ip = 32;
    std::cout << "i is " << i << std::endl; // 32
    i = 64;
    std::cout << "*ip is " << *ip << std::endl; // 64
    return 0;
}