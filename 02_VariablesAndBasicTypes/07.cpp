/**
 *  \file 7.cpp
 *  \brief Brief description
 *  Exercise 2.7: What values do these literals represent?
 *  What type does each have?
 *  (a) "Who goes with F\145rgus?\012"
 *  (b) 3.14e1L (c) 1024f (d) 3.14L
 *  
 *  (a) "Who goes with F\145rgus?\012": 
 *  Character string literal containing two octal escape sequences
 *  
 *  (b) 3.14e1L: floating-point literal, long double
 *  (c) 1024f: floating-point literal, float
 *  (d) 3.14L: floating-point literal, long double
 */

#include <iostream>

int main()
{
    std::cout << "Who goes with F\145rgus?\012" << std::endl;
    return 0;
}