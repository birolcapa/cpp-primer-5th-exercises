/**
 *  \file 21.cpp
 *  \brief Exercise 2.21:
 *  Explain each of the following definitions.
 *  Indicate whether any are illegal and, if so, why
 */

#include <iostream>

int main()
{
    int i = 0;
    // double* dp = &i; // Error: error C2440: 'initializing': cannot convert from 'int *' to 'double *'
    // int *ip = i; // Error: error C2440: 'initializing': cannot convert from 'int' to 'int *'
    int *p = &i; // Valid
    return 0;
}