/**
 *  \file 15.cpp
 *  \brief Exercise 2.15:
 *  Which of the following definitions, if any, are invalid? Why?
 */

#include <iostream>

int main()
{
    int ival = 1.01; // Valid but ival becomes 1
    // int &rval1 = 1.01; // Error: error C2440: 'initializing': cannot convert from 'double' to 'int &'
    int &rval2 = ival; // Valid, rval2 is 1.
    std::cout << rval2 << std::endl;
    // int &rval3; // Error: error C2530: 'rval3': references must be initialized
    return 0;
}