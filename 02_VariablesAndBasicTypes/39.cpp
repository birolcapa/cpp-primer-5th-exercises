/**
 *  \file 39.cpp
 *  \brief Exercise 2.39:
 *  Compile the following program to see what happens when you forget
 *  the semicolon after a class definition. 
 *  Remember the message for future reference.
 */

struct Foo { /* empty */ } // Note: no semicolon
// error C2628: 'Foo' followed by 'int' is illegal (did you forget a ';'?)
int main()
{
    return 0;
}