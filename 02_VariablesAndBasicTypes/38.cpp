/**
 *  \file 38.cpp
 *  \brief Exercise 2.38: Describe the differences in type deduction between 
 *  decltype and auto. 
 *  Give an example of an expression where auto and decltype will deduce
 *  the same type and an example where they will deduce differing types.
 *  Info:
 *  auto will ignore the top-level const qualifier and reference.
 *  decltype will include top-level const and reference.
 */

#include <iostream>

int main()
{
    int x = 8;
    auto a1 = x;     // int
    decltype(x) d1;  // int

    int y, &j = y;
    auto a2 = j;     // int
    decltype(j) d2 = j;  // int &

    const int i = y;
    auto a3 = i;          // int
    decltype(i) d3 = 42;  // const int
    return 0;
}
