/**
 *  \file 28.cpp
 *  \brief Exercise 2.28: Explain the following
 *  definitions. Identify any that are illegal.
 *  (a) int i, *const cp;
 *  (b) int *p1, *const p2;
 *  (c) const int ic, &r = ic; 
 *  (d) const int *const p3;
 *  (e) const int *p;
 */
 
#include <iostream>

int main()
{
    // int i, *const cp; // error C2734: 'cp': 'const' object must be initialized if not 'extern'
    
    // int *p1, *const p2; // error C2734: 'p2': 'const' object must be initialized if not 'extern'
    
    // const int ic, &r = ic; // error C2734: 'ic': 'const' object must be initialized if not 'extern'
    
    // const int *const p3; // error C2734: 'p3': 'const' object must be initialized if not 'extern'
    
    const int *p; // legal a pointer to const int
    return 0;
}