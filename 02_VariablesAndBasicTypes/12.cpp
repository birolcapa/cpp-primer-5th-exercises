/**
 *  \file 12.cpp
 *  \brief Exercise 2.12:
 *  Which, if any, of the following names are invalid?
 */

#include <iostream>

int main()
{
    // int double = 3.14; // Error: 'int' followed by 'double' is illegal
    // int catch-22; // Error: - in name
    double Double = 3.14; // Valid
    int _; // Valid
    int 1_or_2 = 1; // Error: start with number
    return 0;
}