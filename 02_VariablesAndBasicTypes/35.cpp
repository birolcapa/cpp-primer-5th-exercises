/**
 *  \file 35.cpp
 *  \brief Exercise 2.35:
 *  Determine the types deduced in each of the following definitions. 
 *  Once you’ve figured out the types, write a program to see whether you were correct.
 *  const int i = 42;
 *  auto j = i; const auto &k = i; auto *p = &i;
 *  const auto j2 = i, &k2 = i;
 */
 
#include <iostream>

int main()
{
    const int i = 42; // i is a const int
    std::cout << "i is " << typeid(i).name() << std::endl;

    auto j = i; // j is an int (top-level const in i is dropped
    std::cout << "j is " << typeid(j).name() << std::endl;
    const auto &k = i; // const int &
    std::cout << "k is " << typeid(k).name() << std::endl;
    auto *p = &i; // p is const int *
    const auto j2 = i, &k2 = i; // j2 is a const int, k2 is const int &
    return 0;
}