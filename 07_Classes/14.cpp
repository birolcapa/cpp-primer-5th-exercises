/**
 *  \file 14.cpp
 *  \brief Exercise 7.14
 *  Write a version of the default constructor that explicitly initializes the
 *  members to the values we have provided as in-class initializers.
 */

#include<iostream>
using std::cin;

#include "14.h"

int main()
{
    Sales_data total(cin);
    if(cin) {
        Sales_data trans(cin);
        while(cin) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(cout, total) << endl;
                total = trans;
            }
            read(cin, trans);
        }
        print(cout, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}