/**
 *  \file 49.cpp
 *  \brief Exercise 7.49
 *  For each of the three following declarations of combine, explain what
 *  happens if we call i.combine(s), where i is a Sales_data and s is a string:
    (a) Sales_data &combine(Sales_data);
        The constructor creates a (temporary) Sales_data object from string s.
        Then the object is passed to combine.
    (b) Sales_data &combine(Sales_data&);
        The constructor creates a (temporary) Sales_data object from string s.
        C++ only allows a temporary to be passed to a const reference, value, or rvalue reference.
        So it will cause an compile error:  Conversion loses qualifiers
    (c) Sales_data &combine(const Sales_data&) const;
        The constructor creates a (temporary) Sales_data object from string s.
        But since this is a const member function, by definition it does not allow change on data.
        eg: 'units_sold' cannot be modified because it is being accessed through a const object
 */

/**
 *  \file 41.cpp
 *  \brief Exercise 7.41
 *  Exercise 7.41: Rewrite your own version of the Sales_data class to use delegating
 *  constructors. Add a statement to the body of each of the constructors that prints a
 *  message whenever it is executed. Write declarations to construct a Sales_data object
 *  in every way possible. Study the output until you are certain you understand the order
 *  of execution among delegating constructors.
 */

/**
 *  \file 26.cpp
 *  \brief Exercise 7.26
 *  Define Sales_data::avg_price as an inline function.
 */

/**
 *  \file 21.cpp
 *  \brief Exercise 7.21
 *  Update your Sales_data class to hide its implementation.
 *  The programs you've written to use Sales_data operations should
 *  still continue to work.
 *  Recompile those programs with your new class definition to
 *  verify that they still work
 */

/**
 *  \file 14.cpp
 *  \brief Exercise 7.14
 *  Write a version of the default constructor that explicitly initializes the
 *  members to the values we have provided as in-class initializers.
 */

#include<iostream>
using std::cin;

#include "49.h"

int main()
{
    Sales_data total(string("9-999-99999-9"));
    total.combine(string("9-999-99999-9"));
    print(cout, total) << endl;
    return 0;
}