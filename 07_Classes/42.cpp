/**
 *  \file 42.cpp
 *  \brief Exercise 7.42
 *  For the class you wrote for exercise 7.40 in 7.5.1 (p. 291),
 *  decide whether any of the constructors might use delegation.
 *  If so, write the delegating constructor(s) for your class.
 *  If not, look at the list of abstractions and choose one that you
 *  think would use a delegating constructor.
 *  Write the class definition for that abstraction
 */

/**
 *  \file 40.cpp
 *  \brief Exercise 7.40
 *  Choose one of the following abstractions (or an abstraction of your
 *  own choosing).
 *  Determine what data are needed in the class. Provide an appropriate
 *  set of constructors. Explain your decisions.
 *  (a) Book (b) Date (c) Employee
 *  (d) Vehicle (e) Object (f) Tree
 */
#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

#include <string>
using std::string;

class Book {
    friend ostream& print(ostream& os, const Book& item);
public:
    Book(const string &name, const string &author, const string &publisher, double price):
        m_name(name), m_author(author), m_publisher(publisher), m_price(price) {}
    Book() : Book("Unknown", "Unknown", "Unknown", 0.0f) {}
private:
    string m_name;
    string m_author;
    string m_publisher;
    double m_price;
};

ostream& print(ostream& os, const Book& item) {
    os << "Name: " << item.m_name << " " << endl <<
        "\tAuthor: " << item.m_author << endl <<
    "\tPublisher: " << item.m_publisher << endl <<
    "\tPrice: " << item.m_price;
    return os;
}

int main()
{
    string name = "Cpp Learning";
    string author = "Stanley L.";
    string publisher = "Addison W";
    double price = 32.68;
    Book myBook(name, author, publisher, price);
    print(cout, myBook) << endl;
    Book unknownBook;
    print(cout, unknownBook) << endl;
    return 0;
}