/**
 *  \file 38.cpp
 *  \brief Exercise 7.38
 *  We might want to supply cin as a default argument to the constructor
 *  that takes an istream&.
 *  Write the constructor declaration that uses cin as a default argument.
 */

//Sales_data(istream &is) {read(is, *this);}
Sales_data(istream &is = std::cin) {read(is, *this);}