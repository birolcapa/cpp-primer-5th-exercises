/**
 *  \file 09.cpp
 *  \brief Exercise 7.9
 *  Add operations to read and print Person objects to the code you wrote
 *  for the exercises in 7.1.2 (p. 260).
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::istream;
using std::ostream;

#include <string>
using std::string;

class Person {
    string m_name;
    string m_address;
public:
    void setName(const string name) { m_name = name; }
    void setAddress(const string address) { m_address = address; }
    string getName() const { return m_name; }
    string getAddress() const { return m_address; }
};

istream& read(istream &is, Person& p) {
    string name, address;
    is >> name >> address;

    p.setName(name);
    p.setAddress(address);

    return is;
}

ostream& print(ostream& os, const Person& p) {
    os << "Name: " << p.getName() << " " <<
        " Address: " << p.getAddress();
        return os;
}

int main()
{
    Person x;
    read(cin, x);
    print(cout, x);
    return 0;
}