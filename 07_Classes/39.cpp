/**
 *  \file 39.cpp
 *  \brief Exercise 7.39
 *  Would it be legal for both the constructor that takes a string and the
 *  one that takes an istream& to have default arguments? If not, why not?
 *  Answer: No it is not legal.
 *  Calling Sales_data(std::string s) will be ambiguous.
 *
 *  39.cpp(32): error C2668: 'Sales_data::Sales_data': ambiguous call to overloaded function
 *  39.cpp(21): note: could be 'Sales_data::Sales_data(std::string,std::istream &)'
 *  39.cpp(20): note: or       'Sales_data::Sales_data(std::string)'
 */

#include <iostream>
using std::istream;

class Sales_data{
friend istream& read(istream &is, Sales_data& item);
public:
    Sales_data(std::string s);
    Sales_data(std::string s, istream &is = std::cin) {read(is, *this);}
private:
};

istream& read(istream &is, Sales_data& item) {
    return is;
}

int main()
{
    Sales_data testdata("test");
    return 0;
}