/**
 *  \file 35.cpp
 *  \brief Exercise 7.35
 *  Explain the following code, indicating which definition of Type or
 *  initVal is used for each use of those names. Say how you would fix any errors.
 */

// Fix
#include <string>
// Fix
using std::string;

typedef string Type;
Type initVal();

class Exercise {
public:
    typedef double Type;
    Type setVal(Type);
    Type initVal();
private:
    int val;
};

// Fix
// Define initVal member method of Exercise
Exercise::Type Exercise::initVal(){
    return 0.0;
}

// Fix
//Type Exercise::setVal(Type parm) {
Exercise::Type Exercise::setVal(Exercise::Type parm) {
    // Fix
    //val = parm + initVal();
    val = parm + Exercise::initVal();
    return val;
}

int main()
{
    return 0;
}