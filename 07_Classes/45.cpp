/**
 *  \file 45.cpp
 *  \brief Exercise 7.45
 *  What if we defined the vector in the previous exercise to hold objects of type C?
 *  Answer:
 *  Since C has a default constructor, the program will be compiled.
 */

/**
 *  \file 44.cpp
 *  \brief Exercise 7.44
 *  Exercise 7.44: Is the following declaration legal? If not, why not?
 *  vector<NoDefault> vec(10);
 *  Answer:
 *  No, it is not legal, because
 *  'NoDefault::NoDefault': no appropriate default constructor available!
 *  Compilation error
 */

/**
 *  \file 43.cpp
 *  \brief Exercise 7.43
 *  Assume we have a class named NoDefault that has a constructor that
 *  takes an int, but has no default constructor.
 *  Define a class C that has a member of
 *  type NoDefault. Define the default constructor for C.
 */
#include <vector>
using std::vector;

class NoDefault{
public:
    NoDefault(int x) {
        m_x = x;
    }
private:
    int m_x;
};

class C {
public:
    C(): m_Node(0) {}
private:
    NoDefault m_Node;
};

int main() {
    C c;
    vector<int> x(10);
    vector<C> y(10);
    return 0;
}