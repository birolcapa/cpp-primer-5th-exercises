/**
 *  \file 31.cpp
 *  \brief Exercise 7.31:
 *  Define a pair of classes X and Y,
 *  in which X has a pointer to Y,
 *  and Y has an object of type X.
 */

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;

class Y;

class X {
    Y* y;
public:
    /*
    31.cpp(33): error C2027: use of undefined type 'Y'
    31.cpp(15): note: see declaration of 'Y'
    We can use an incomplete type in only limited ways:
    We can define pointers or references to such types,
    and we can declare (but not define) functions that use an
    incomplete type as a parameter or return type.
    A class must be defined—not just declared—before
    we can write code that creates objects of that type.
    Otherwise, the compiler does not know how much storage
    such objects need.
    *//*
    X() {
        text = y->text;
    }*/
    string text;
};

class Y {
    X x;
public:
    Y() {
        text = "test Y";
    }
    string text;
};

int main()
{
    X test1;
    cout << test1.text << endl;
    Y test2;
    cout << test2.text << endl;
    return 0;
}