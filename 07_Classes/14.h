#ifndef _14_H_
#define _14_H_

#include<iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::istream;
using std::ostream;

#include <string>
using std::string;

struct Sales_data;
istream& read(istream &is, Sales_data& item);

struct Sales_data {
    Sales_data() : bookNo(""), units_sold(0), revenue(0.0) {}
    Sales_data(const string &s) : bookNo(s) {}
    Sales_data(const string &s, unsigned n, double p) :
        bookNo(s), units_sold(n), revenue(p * n) {}

    Sales_data(istream &is) {read(is, *this);}

    string isbn() const { return bookNo; }
    Sales_data& combine(const Sales_data&);

    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

// To return an lvalue, our combine function must return a reference (6.3.2, p. 226).
// Calls to functions that return references are lvalues; other return types yield rvalues.
Sales_data& Sales_data::combine(const Sales_data& rhs) {
    units_sold += rhs.units_sold;
    revenue += rhs.revenue;

    /*
    When we call a member function, this is initialized with
    the address of the object on which the function was invoked.
    Because this is intended to always refer to "this" object,
    this is a const pointer (2.4.2, p. 62).
    We cannot change the address that this holds.
    */

    /* Here the return statement dereferences this
    to obtain the object on which the function is executing.
    That is, for the call below, we return a reference to total.*/
    return *this;
}

Sales_data add(const Sales_data& lhs, const Sales_data& rhs) {
    Sales_data sum = lhs;
    sum.combine(rhs);
    return sum;
}

istream& read(istream &is, Sales_data& item) {
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}

ostream& print(ostream& os, const Sales_data& item) {
    os << "Book No " << item.isbn() << " " <<
        " Units sold: " << item.units_sold <<
        " revenue " << item.revenue;
        return os;
}

#endif