Exercise 7.55: Is the Data class from § 7.5.5 a literal class? If not, why not? If
so, explain why it is literal.

struct Data {
    int ival;
    string s;
}

In addition to the arithmetic types, references, and
pointers, certain classes are also literal types.

An aggregate class (§ 7.5.5, p. 298) whose data members are all of literal type is
a literal class.

Of the types we have used so far, the arithmetic, reference, and pointer types
are literal types.
Our Sales_item class and the library IO and string types are not literal types.

As a result no, this is not a literal class.