/**
 *  \file 11.cpp
 *  \brief Exercise 7.11
 *  Add constructors to your Sales_data class and write a program to
 *  use each of the constructors.
 */

#include<iostream>
using std::cin;

#include "11.h"

int main()
{
    Sales_data day1;
    print(cout, day1) << endl;
    Sales_data day2("0-000-0000-0");
    print(cout, day2) << endl;

    Sales_data day3("1-111-1111-1", 10, 3.14);
    print(cout, day3) << endl;

    Sales_data day4(cin);
    print(cout, day4) << endl;

    return 0;
}