#ifndef _49_H_
#define _49_H_

#include<iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::istream;
using std::ostream;

#include <string>
using std::string;

class Sales_data {

friend Sales_data add(const Sales_data& lhs, const Sales_data& rhs);
friend istream& read(istream &is, Sales_data& item);
friend ostream& print(ostream& os, const Sales_data& item);

public:
    Sales_data() : Sales_data("", 0, 0.0) {
        cout << "Sales_data() is called" << endl;
    }
    Sales_data(const string &s) : Sales_data(s, 0, 0.0) {
        cout << "Sales_data(const string &s) is called" << endl;
    }
    Sales_data(const string &s, unsigned n, double p) :
        bookNo(s), units_sold(n), revenue(p * n) {
            cout << "Sales_data(const string &s, unsigned n, double p) is called" << endl;
        }

    Sales_data(istream &is) : Sales_data() {
        cout << "Sales_data(istream &is) is called" << endl;
        read(is, *this);
    }

    string isbn() const { return bookNo; }
    Sales_data& combine(const Sales_data&) {
    // (a) Sales_data& combine(Sales_data rhs) {
    // (b) Sales_data &combine(Sales_data& rhs) {
        //  Conversion loses qualifiers
    // (c) Sales_data &combine(const Sales_data& rhs) const {
        // 'units_sold' cannot be modified because it is being accessed through a const object
        // 'revenue' cannot be modified because it is being accessed through a const object
        // 'return': cannot convert from 'const Sales_data' to 'Sales_data &'
        // Conversion loses qualifiers
        units_sold += rhs.units_sold;
        revenue += rhs.revenue;
        return *this;
    }
private:
    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
    double avg_price() const;
};

Sales_data add(const Sales_data& lhs, const Sales_data& rhs) {
    Sales_data sum = lhs;
    sum.combine(rhs);
    return sum;
}

istream& read(istream &is, Sales_data& item) {
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}

ostream& print(ostream& os, const Sales_data& item) {
    os << "Book No " << item.isbn() << " " <<
        " Units sold: " << item.units_sold <<
        " revenue " << item.revenue <<
        " average price " << item.avg_price();
        return os;
}

inline double Sales_data::avg_price() const {
    return units_sold ? revenue/units_sold : 0;
}

#endif