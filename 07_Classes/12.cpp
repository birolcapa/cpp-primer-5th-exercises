/**
 *  \file 12.cpp
 *  \brief Exercise 7.12
 *  Move the definition of the Sales_data constructor that takes an
 *  istream into the body of the Sales_data class.
 */

#include<iostream>
using std::cin;

#include "12.h"

int main()
{
    Sales_data day1;
    print(cout, day1) << endl;

    Sales_data day2("0-000-0000-0");
    print(cout, day2) << endl;

    Sales_data day3("1-111-1111-1", 10, 3.14);
    print(cout, day3) << endl;

    Sales_data day4(cin);
    print(cout, day4) << endl;

    return 0;
}