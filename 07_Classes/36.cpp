/**
 *  \file 36.cpp
 *  \brief Exercise 7.36
 *  The following initializer is in error.
 *  Identify and fix the problem.
 *  Answer:
 *  Members are initialized in the order in which
 *  they appear in the class definition:
 *  The first member is initialized first, then the next, and so on.
 *  The order in which initializers appear in the constructor initializer list
 *  does not change the order of initialization
 *  As a result rem initialized first, but its initialization depends on base.
 *  However, base is not initialized yet!.
 *  So rem will be initialized with the undefined value of base
 */

#include <iostream>

struct X {
    X (int i, int j): base(i), rem(base % j) { }
    int rem, base;
};

int main()
{
    int a = 30;
    int b = 15;
    X x(a, b);
    if((x.base == a) & (x.rem == (a%b))) {
        std::cout << "correct initialization" << std::endl;
    }
    else{
        std::cerr << "initialization is not correct" << std::endl;
        std::cerr << "Expected base: " << a << " Actual: " << x.base << std::endl;
        std::cerr << "Expected rem : " << (a%b) << " Actual: " << x.rem << std::endl;
    }
    return 0;
}