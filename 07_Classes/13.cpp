/**
 *  \file 13.cpp
 *  \brief Exercise 7.13
 *  Rewrite the program from page 255 to use the istream constructor
 */

#include<iostream>
using std::cin;

#include "13.h"

int main()
{
    Sales_data total(cin);
    if(cin) {
        Sales_data trans(cin);
        while(cin) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(cout, total) << endl;
                total = trans;
            }
            read(cin, trans);
        }
        print(cout, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}