/**
 *  \file 58.cpp
 *  \brief Exercise 7.58
 *  Which, if any, of the following static data member declarations and
 *  definitions are errors? Explain why.
 */

// example.h
class Example {
public:
    static double rate = 6.5; // -> rate shall be a constant expression.
    static const int vecSize = 20;
    static vector<double> vec(vecSize);
    // -> parentheses are not in-class initializers.  only use = or {}
    // ->  Ordinarily, class static members may not be initialized in the class body.
    //  However, we can provide in-class initializers for static members that have const
    //  integral type and must do so for static members
    // that are constexprs of literal type (7.5.6, p. 299).
};

// example.C
#include "example.h"
double Example::rate;
// -> Initialize static data member
// double Example::rate = 6.5;
vector<double> Example::vec;
// -> Initialize static data member
// vector<double> Example::vec(vecSize);