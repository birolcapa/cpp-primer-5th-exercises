/**
 *  \file 34.cpp
 *  \brief Exercise 7.34
 *  What would happen if we put the typedef of pos in the Screen class
 *  on page 285 as the last line in the class?
 *  Answer:
 *  All usages of pos would cause an error, because definition of pos is missing
 *  before the usage of it
 *  From Normal Block-Scope Name Lookup inside Member Definitions part:
 *  If a declaration for the name is not found in the class,
 *  look for a declaration that is in scope
 *  BEFORE the member function definition.
 *  34.cpp(28): error C2061: syntax error: identifier 'pos'
 *  34.cpp(32): error C3646: 'cursor': unknown override specifier
 *  34.cpp(32): error C2059: syntax error: '='
 *  34.cpp(32): error C2238: unexpected token(s) preceding ';'
 *  34.cpp(33): error C3646: 'height': unknown override specifier
 *  34.cpp(33): error C2059: syntax error: '='
 *  34.cpp(33): error C2238: unexpected token(s) preceding ';'
 *  34.cpp(29): error C2065: 'cursor': undeclared identifier
 *  34.cpp(29): error C2065: 'width': undeclared identifier
 */

#include <iostream>

// note: this code is for illustration purposes only and reflects bad practice
// it is generally a bad idea to use the same name for a parameter and a member
int height; // defines a name subsequently used inside Screen
class Screen {
public:
    //typedef std::string::size_type pos;
    void dummy_fcn(pos height) {
    cursor = width * height; // which height? the parameter
    }
private:
    pos cursor = 0;
    pos height = 0, width = 0;
    typedef std::string::size_type pos;
};

int main()
{
    return 0;
}
/*
Here

*/