/**
 *  \file 43.cpp
 *  \brief Exercise 7.43
 *  Assume we have a class named NoDefault that has a constructor that
 *  takes an int, but has no default constructor.
 *  Define a class C that has a member of
 *  type NoDefault. Define the default constructor for C.
 */

class NoDefault{
public:
    NoDefault(int x) {
        m_x = x;
    }
private:
    int m_x;
};

class C {
public:
    C(): m_Node(0) {}
private:
    NoDefault m_Node;
};

int main() {
    C c;
    return 0;
}