/**
 *  \file 24.cpp
 *  \brief Exercise 7.24
 *  Give your Screen class three constructors:
 *  a default constructor;
 *  a constructor that takes values for
 *  height and width and initializes the contents
 *  to hold the given number of blanks;
 *  and a constructor that takes values for
 *  height, width, and a character to use as the contents of the screen.
 */

/**
 *  \file 23.cpp
 *  \brief Exercise 7.23
 *  Write your own version of the Screen class.
 */
#include <iostream>
#include <string>

class Screen {
public:
    // alternative type definition
    //typedef std::string::size_type pos;
    using pos = std::string::size_type;
    // needed because Screen has another constructor
    Screen() = default;
    Screen(pos ht, pos wd, char c) :
        height(ht), width(wd), contents(ht * wd, c) {}
    char get() const { return contents[cursor]; }
    inline char get(pos ht, pos wd) const;
    Screen &move(pos r, pos c);
private:
    pos cursor = 0;
    pos height = 0, width = 0;
    std::string contents;
};

inline Screen &Screen::move(pos r, pos c) {
    pos row = r * width; // compute the row location
    cursor = row + c; // move cursor to the column within that row
    return *this; // return this object value as an lvalue
}

char Screen::get(pos r, pos c) const {
    pos row = r * width; // compute the row location
    return contents[row + c]; // return character at the given column
}

int main()
{
    Screen myScreen;
    char ch = myScreen.get(); // calls Screen::get()
    std::cout << ch << std::endl;
    ch = myScreen.get(0,0); // calls Screen::get(pos, pos)
    std::cout << ch << std::endl;
    return 0;
}