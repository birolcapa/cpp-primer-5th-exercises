/**
 *  \file 29.cpp
 *  \brief Exercise 7.29
 *  Revise your Screen class so that move, set, and display functions
 *  return Screen and check your prediction from the previous
 *  exercise.
 */

/**
 *  \file 27.h
 *  \brief Exercise 7.27
 *  Add the move, set, and display operations to your version of
 *  Screen.
 *  Test your class by executing the following code:
 *  Screen myScreen(5, 5, 'X');
 *  myScreen.move(4,0).set('#').display(cout);
 *  cout << "\n";
 *  myScreen.display(cout);
 *  cout << "\n";
 */

/**
 *  \file 24.cpp
 *  \brief Exercise 7.24
 *  Give your Screen class three constructors:
 *  a default constructor;
 *  a constructor that takes values for
 *  height and width and initializes the contents
 *  to hold the given number of blanks;
 *  and a constructor that takes values for
 *  height, width, and a character to use as the contents of the screen.
 */

/**
 *  \file 23.cpp
 *  \brief Exercise 7.23
 *  Write your own version of the Screen class.
 */
#include <iostream>
using std::cout;

#include <string>

class Screen {
public:
    // alternative type definition
    //typedef std::string::size_type pos;
    using pos = std::string::size_type;
    // needed because Screen has another constructor
    Screen() = default;
    Screen(pos ht, pos wd) :
        height(ht), width(wd), contents(ht * wd, ' ') {}
    Screen(pos ht, pos wd, char c) :
        height(ht), width(wd), contents(ht * wd, c) {}
    char get() const { return contents[cursor]; }
    inline char get(pos ht, pos wd) const;
    Screen move(pos r, pos c);
    Screen set(char c);
    Screen set(pos r, pos col, char ch);
    Screen display(std::ostream &os) { do_display(os); return *this;}
    const Screen display(std::ostream &os) const
        { cout<< "const -> "; do_display(os); return *this;}
private:
    pos cursor = 0;
    pos height = 0, width = 0;
    std::string contents;
    void do_display(std::ostream &os) const { os << contents; }
};

inline Screen Screen::move(pos r, pos c) {
    pos row = r * width; // compute the row location
    cursor = row + c; // move cursor to the column within that row
    return *this; // return this object value as an lvalue
}

/*
Functions that return a reference are lvalues (6.3.2, p. 226),
which means that they return the object itself, not a copy of the object.
If we concatenate a sequence of these actions into a single expression:
    // move the cursor to a given position, and set that character
    myScreen.move(4,0).set('#');
these operations will execute on the same object
In this expression, we first
- move the cursor inside myScreen and then
- set a character in myScreen’s contents member.
That is, this statement is equivalent to
    myScreen.move(4,0);
    myScreen.set('#');

Had we defined move and set to return Screen, rather than Screen&,
this statement would execute quite differently.
In this case it would be equivalent to:
// if move returns Screen not Screen&
    Screen temp = myScreen.move(4,0); // the return value would be copied
    temp.set('#'); // the contents inside myScreen would be unchanged
If move had a nonreference return type, then the return value of move would be a
copy of *this (6.3.2, p. 224).
The call to set would change the temporary copy, not myScreen.
*/

inline Screen Screen::set(char c) {
    contents[cursor] = c; // set the new value at the current cursor location
    return *this;
}

inline Screen Screen::set(pos r, pos col, char ch) {
    contents[r * width + col] = ch; // set specified location to given value
    return *this;
}

char Screen::get(pos r, pos c) const {
    pos row = r * width; // compute the row location
    return contents[row + c]; // return character at the given column
}

int main()
{
    Screen myScreen(5, 5, 'X');
    myScreen.move(4,0).set('#').display(std::cout);
    std::cout << "\n--------------------|\n";
    myScreen.display(std::cout);
    std::cout << "\n--------------------|\n";

    /*
    Expectation
    XXXXXXXXXXXXXXXXXXXX#XXXX
    --------------------|
    XXXXXXXXXXXXXXXXXXXX#XXXX
    --------------------|
    */

    /*
    Reality
    Had we defined move and set to return Screen,
    rather than Screen&,
    this statement would execute quite differently.
    In this case it would be equivalent to:
    // if move returns Screen not Screen&
    Screen temp = myScreen.move(4,0); // the return value would be copied
    temp.set('#'); // the contents inside myScreen would be unchanged

    If move had a nonreference return type,
    then the return value of move would be a copy of *this (6.3.2, p. 224).
    The call to set would change the temporary copy, not myScreen.
    XXXXXXXXXXXXXXXXXXXX#XXXX
    --------------------|
    XXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------|
    */
    return 0;
}