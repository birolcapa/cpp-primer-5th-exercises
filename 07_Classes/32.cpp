/**
 *  \file 32.h
 *  \brief Exercise 7.32
 *  Define your own versions of Screen and Window_mgr in which
 *  clear is a member of Window_mgr and a friend of Screen.
 */

/**
 *  \file 27.h
 *  \brief Exercise 7.27
 */

#include <iostream>
using std::cout;

#include <string>
using std::string;

#include <vector>
using std::vector;

class Screen;

class Window_mgr {
public:
    using screen_index = vector<Screen>::size_type;
    void clear(screen_index);
    Window_mgr();
private:
    vector<Screen> screens;
};

class Screen {
    friend void Window_mgr::clear(screen_index);
public:
    // alternative type definition
    //typedef std::string::size_type pos;
    using pos = string::size_type;
    // needed because Screen has another constructor
    Screen() = default;
    Screen(pos ht, pos wd) :
        height(ht), width(wd), contents(ht * wd, ' ') {}
    Screen(pos ht, pos wd, char c) :
        height(ht), width(wd), contents(ht * wd, c) {}
    char get() const { return contents[cursor]; }
    inline char get(pos ht, pos wd) const;
    Screen &move(pos r, pos c);
    Screen &set(char c);
    Screen &set(pos r, pos col, char ch);
    Screen &display(std::ostream &os) { do_display(os); return *this;}
    const Screen &display(std::ostream &os) const { cout<< "const -> "; do_display(os); return *this;}
private:
    pos cursor = 0;
    pos height = 0, width = 0;
    string contents;
    void do_display(std::ostream &os) const { os << contents; }
};

inline Screen &Screen::move(pos r, pos c) {
    pos row = r * width; // compute the row location
    cursor = row + c; // move cursor to the column within that row
    return *this; // return this object value as an lvalue
}

inline Screen &Screen::set(char c) {
    contents[cursor] = c; // set the new value at the current cursor location
    return *this;
}

inline Screen &Screen::set(pos r, pos col, char ch) {
    contents[r * width + col] = ch; // set specified location to given value
    return *this;
}

char Screen::get(pos r, pos c) const {
    pos row = r * width; // compute the row location
    return contents[row + c]; // return character at the given column
}

// constructor
Window_mgr::Window_mgr() : screens {Screen(24, 80, ' ')} {}

void Window_mgr::clear(screen_index i)
{
    // s is a reference to the Screen we want to clear
    Screen &s = screens[i];
    // reset the contents of that Screen to all blanks
    s.contents = string(s.height * s.width, ' ');
}

int main()
{
    Window_mgr window_mgr;
    window_mgr.clear(0);

    Screen myScreen(5, 5, 'X');
    myScreen.move(4,0).set('#').display(cout);
    cout << "\n";
    myScreen.display(cout);
    cout << "\n";

    Screen myScreen2(5, 3, 'Y');
    myScreen2.set('#').display(cout); // calls nonconst version
    cout << "\n";

    const Screen blank(5, 3, 'Y');
    blank.display(cout); // calls const version
    cout << "\n";
    return 0;
}