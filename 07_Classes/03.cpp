/**
 *  \file 03.cpp
 *  \brief Exercise 7.3
 *  Revise your transaction-processing program from 7.1.1 (p. 256) to use these members
 */

#include<iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#include <string>
using std::string;

struct Sales_data {
    string isbn() const { return bookNo; }
    Sales_data& combine(const Sales_data&);

    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

// To return an lvalue, our combine function must return a reference (6.3.2, p. 226).
// Calls to functions that return references are lvalues; other return types yield rvalues.
Sales_data& Sales_data::combine(const Sales_data& rhs) {
    units_sold += rhs.units_sold;
    revenue += rhs.revenue;

    /*
    When we call a member function, this is initialized with
    the address of the object on which the function was invoked.
    Because this is intended to always refer to "this" object,
    this is a const pointer (2.4.2, p. 62).
    We cannot change the address that this holds.
    */

    /* Here the return statement dereferences this
    to obtain the object on which the function is executing.
    That is, for the call below, we return a reference to total.*/
    return *this;
}

int main()
{
    Sales_data total;
    double price;
    if(cin >> total.bookNo >> total.units_sold >> price) {
        total.revenue = total.units_sold * price;
        Sales_data trans;
        while(cin >> trans.bookNo >> trans.units_sold >> price) {
            trans.revenue = trans.units_sold * price;
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                std::cout << "Book No " << total.bookNo <<
                    " Units sold: " << total.units_sold <<
                    " revenue " << total.revenue << std::endl;
                total = trans;
            }
        }
        std::cout << "Book No " << total.bookNo <<
                    " Units sold: " << total.units_sold <<
                    " revenue " << total.revenue << std::endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}