/**
 *  \file 04.cpp
 *  \brief Exercise 7.4
 *  Write a class named Person that represents the name and address of a person.
 *  Use a string to hold each of these elements. Subsequent exercises will
 *  incrementally add features to this class.
 */

#include <iostream>
#include <string>
using std::string;

class Person {
    string name;
    string address;
};

int main()
{
    return 0;
}