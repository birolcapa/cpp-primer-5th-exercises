/**
 *  \file 57.cpp
 *  \brief Exercise 7.57
 *  Write your own version of the Account class.
 */
#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class Account {
public:
    void calculate() { amount += amount * interestRate; }
    static double rate() { return interestRate; }
    static void rate(double newRate) { interestRate = newRate; }
private:
    std::string owner;
    double amount;
    static double interestRate;
    static double initRate() { return 1.8; }
};

double Account::interestRate = initRate();

int main()
{
    double r;
    r = Account::rate(); // access a static member using the scope operator
    cout << r << endl;
}