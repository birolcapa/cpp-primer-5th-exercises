/**
 *  \file 40.cpp
 *  \brief Exercise 7.40
 *  Choose one of the following abstractions (or an abstraction of your
 *  own choosing).
 *  Determine what data are needed in the class. Provide an appropriate
 *  set of constructors. Explain your decisions.
 *  (a) Book (b) Date (c) Employee
 *  (d) Vehicle (e) Object (f) Tree
 */
#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

#include <string>
using std::string;

class Book {
    friend ostream& print(ostream& os, const Book& item);
public:
    Book(const string &name, const string &author, const string &publisher, double price):
        m_name(name), m_author(author), m_publisher(publisher), m_price(price) {}
    Book() : Book("Unknown", "Unknown", "Unknown", 0.0f) {}
private:
    string m_name;
    string m_author;
    string m_publisher;
    double m_price;
};

ostream& print(ostream& os, const Book& item) {
    os << "Name: " << item.m_name << " " << endl <<
        "\tAuthor: " << item.m_author << endl <<
    "\tPublisher: " << item.m_publisher << endl <<
    "\tPrice: " << item.m_price;
    return os;
}

int main()
{
    string name = "Cpp Learning";
    string author = "Stanley L.";
    string publisher = "Addison W";
    double price = 32.68;
    Book myBook(name, author, publisher, price);
    print(cout, myBook) << endl;
    Book unknownBook;
    print(cout, unknownBook) << endl;
    return 0;
}