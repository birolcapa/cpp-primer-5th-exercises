/**
 *  \file 41.cpp
 *  \brief Exercise 7.41
 *  Exercise 7.41: Rewrite your own version of the Sales_data class to use delegating
 *  constructors. Add a statement to the body of each of the constructors that prints a
 *  message whenever it is executed. Write declarations to construct a Sales_data object
 *  in every way possible. Study the output until you are certain you understand the order
 *  of execution among delegating constructors.
 */

/**
 *  \file 26.cpp
 *  \brief Exercise 7.26
 *  Define Sales_data::avg_price as an inline function.
 */

/**
 *  \file 21.cpp
 *  \brief Exercise 7.21
 *  Update your Sales_data class to hide its implementation.
 *  The programs you've written to use Sales_data operations should
 *  still continue to work.
 *  Recompile those programs with your new class definition to
 *  verify that they still work
 */

/**
 *  \file 14.cpp
 *  \brief Exercise 7.14
 *  Write a version of the default constructor that explicitly initializes the
 *  members to the values we have provided as in-class initializers.
 */

#include<iostream>
using std::cin;

#include "41.h"

int main()
{
    Sales_data total(cin);
    if(cin) {
        Sales_data trans(cin);
        while(cin) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(cout, total) << endl;
                total = trans;
            }
            read(cin, trans);
        }
        print(cout, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}