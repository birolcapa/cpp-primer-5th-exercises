/**
 *  \file 05.cpp
 *  \brief Exercise 7.5
 *  Provide operations in your Person class to return the name and address.
 *  Should these functions be const? Explain your choice
 *  Answer:
 *  We just want to read name and address.
 *  We don't want to change name and address accidentally.
 *  Therefore these functions should be const.
 */

#include <iostream>
#include <string>
using std::string;

class Person {
    string name;
    string address;
public:
    string getName() const { return name; }
    string getAddress() const { return address; }
};

int main()
{
    return 0;
}