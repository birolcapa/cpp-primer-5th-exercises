/**
 *  \file 01.cpp
 *  \brief Exercise 7.1
 *  Write a version of the transaction-processing program from § 1.6 (p. 24)
 *  using the Sales_data class you defined for the exercises in § 2.6.1 (p. 72)
 */

#include<iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

struct Sales_data {
    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

int main()
{
    Sales_data total;
    double totalPrice = 0.0;
    if(std::cin >> total.bookNo >> total.units_sold >> totalPrice) {
        total.revenue = total.units_sold * totalPrice;
        Sales_data trans;
        double transPrice = 0.0;
        while(std::cin >> trans.bookNo >> trans.units_sold >> transPrice) {
            trans.revenue = trans.units_sold * transPrice;
            if(total.bookNo == trans.bookNo) {
                total.units_sold += trans.units_sold;
                total.revenue += trans.revenue;

            else {
                std::cout << "Book No " << total.bookNo <<
                    " Units sold: " << total.units_sold <<
                    " revenue " << total.revenue << std::endl;

                if(total.units_sold != 0) {
                    std::cout << "Average value is " << total.revenue / total.units_sold << std::endl;
                }
                else {
                    std::cout << "no sales" << std::endl;
                }

                total.bookNo = trans.bookNo;
                total.units_sold = trans.units_sold;
                total.revenue = trans.revenue;
            }
        }
        std::cout << "Book No " << total.bookNo <<
                    " Units sold: " << total.units_sold <<
                    " revenue " << total.revenue << std::endl;
        if(total.units_sold != 0) {
            std::cout << "Average value is " << total.revenue / total.units_sold << std::endl;
        }
        else {
            std::cout << "no sales" << std::endl;
        }
        return 0;
    }
    else {
        std::cerr << "No Data" << std::endl;
        return -1;
    }
}