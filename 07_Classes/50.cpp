/**
 *  \file 50.cpp
 *  \brief Exercise 7.50
 *  Determine whether any of your Person class constructors should be explicit.
 */

/**
 *  \file 22.cpp
 *  \brief Exercise 7.22
 *  Update your Person class to hide its implementation
 */

/**
 *  \file 15.cpp
 *  \brief Exercise 7.15: Add appropriate constructors to your Person class.
 */
/**
 *  \file 09.cpp
 *  \brief Exercise 7.9
 *  Add operations to read and print Person objects to the code you wrote
 *  for the exercises in 7.1.2 (p. 260).
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::istream;
using std::ostream;

#include <string>
using std::string;

class Person {
    friend istream& read(istream &is, Person& p);
    friend ostream& print(ostream& os, const Person& p);
private:
    string m_name;
    string m_address;
public:
    Person() = default;
    explicit Person(const string & name) : m_name(name) {}
    Person(const string & name, const string &address)
        : m_name(name), m_address(address) {}
    explicit Person(istream &is) {read(is, *this);}

    void setName(const string name) { m_name = name; }
    void setAddress(const string address) { m_address = address; }
    string getName() const { return m_name; }
    string getAddress() const { return m_address; }
};

istream& read(istream &is, Person& p) {
    string name, address;
    is >> name >> address;

    p.setName(name);
    p.setAddress(address);

    return is;
}

ostream& print(ostream& os, const Person& p) {
    os << "Name: " << p.getName() << " " <<
        " Address: " << p.getAddress();
        return os;
}

int main()
{
    Person p1;
    print(cout, p1) << endl;

    Person p2("James");
    print(cout, p2) << endl;

    Person p3("James", "Paris");
    print(cout, p3) << endl;

    Person p4(cin);
    print(cout, p4) << endl;
    
    // 50.cpp(83): error C2440: 'initializing': cannot convert from 'std::string' to 'Person'
    // 50.cpp(83): note: Constructor for class 'Person' is declared 'explicit'
    // Person p5 = string("Henry");
    // Because explicit Constructors Can Be Used Only for Direct Initialization
    // error: cannot use the copy form of initialization with an explicit constructor
    Person p5(string("Henry"));
    print(cout, p5) << endl;
    
    // 50.cpp(88): error C2440: 'initializing': cannot convert from 'std::istream' to 'Person'
    // 50.cpp(88): note: Constructor for class 'Person' is declared 'explicit'
    // Person p6 = cin;
    // Because explicit Constructors Can Be Used Only for Direct Initialization
    // error: cannot use the copy form of initialization with an explicit constructor
    Person p6(cin);
    print(cout, p6) << endl;

    return 0;
}