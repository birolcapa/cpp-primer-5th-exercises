/**
 *  \file 07.cpp
 *  \brief Exercise 7.7
 *  Rewrite the transaction-processing program you wrote for the exercises
 *  in 7.1.2 (p. 260) to use these new functions.
 */

#include<iostream>
using std::cin;

#include "07.h"

int main()
{
    Sales_data total;
    if(read(cin, total)) {
        Sales_data trans;
        while(read(cin, trans)) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(cout, total) << endl;
                total = trans;
            }
        }
        print(cout, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}