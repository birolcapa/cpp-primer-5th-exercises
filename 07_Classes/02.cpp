/**
 *  \file 02.cpp
 *  \brief Exercise 7.2
 *  Add the combine and isbn members to the Sales_data class
 *  you wrote for the exercises in 2.6.2 (p. 76)
 */

#include<iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

struct Sales_data {
    string isbn() const { return bookNo; }
    Sales_data& combine(const Sales_data&);

    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

// To return an lvalue, our combine function must return a reference (6.3.2, p. 226).
// Calls to functions that return references are lvalues; other return types yield rvalues.
Sales_data& Sales_data::combine(const Sales_data& rhs) {
    units_sold += rhs.units_sold;
    revenue += rhs.revenue;

    /*
    When we call a member function, this is initialized with
    the address of the object on which the function was invoked.
    Because this is intended to always refer to "this" object,
    this is a const pointer (2.4.2, p. 62).
    We cannot change the address that this holds.
    */

    /* Here the return statement dereferences this
    to obtain the object on which the function is executing.
    That is, for the call below, we return a reference to total.*/
    return *this;
}

int main()
{
    return 0;
}
