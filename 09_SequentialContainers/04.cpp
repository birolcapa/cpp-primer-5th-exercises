/**
 *  \file 04.cpp
 *  \brief Exercise 9.4
 *  Write a function that takes a pair of iterators to a vector<int> 
 *  and an int value. 
 *  Look for that value in the range and return a bool indicating whether it was found.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

// const_iterator is used, because loop will not change anything inside container
bool findTheVal(vector<int>::const_iterator begin, vector<int>::const_iterator end, int value)
{
    for(auto iter = begin; iter != end; ++iter)
    {
        if(*iter == value)
        {
            return true;
        }
    }
    return false;
}

int main()
{
    vector<int> input{1, 2, 3, 4, 5};
    
    cout << "Finding result is: " << findTheVal(input.cbegin(), input.cend(), 6) << endl;
    cout << "Finding result is: " << findTheVal(input.cbegin(), input.cend(), 2) << endl;
    return 0;
}