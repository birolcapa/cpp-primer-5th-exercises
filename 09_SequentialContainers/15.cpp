/**
 *  \file 15.cpp
 *  \brief Exercise 9.15: Write a program to determine whether two vector<int>s are equal.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

int main()
{
    vector<int> v1 = {1, 3, 5, 7, 9, 12};
    vector<int> v2 = {1, 3, 9};
    vector<int> v3 = {1, 3, 4, 7};
    vector<int> v4 = {1, 3, 5, 7, 9, 12};

    cout << std::boolalpha << (v1 == v2) << endl;
    cout << std::boolalpha << (v1 == v3) << endl;
    cout << std::boolalpha << (v1 == v4) << endl;

    return 0;
}