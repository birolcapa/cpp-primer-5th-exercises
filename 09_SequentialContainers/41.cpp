/**
 * @file 41.cpp
 * @brief Exercise 9.41: Write a program that initializes a string 
 * from a vector<char>.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <string>
using std::string;

int main()
{
    vector<char> cVec{'h', 'e', 'l', 'l', 'o'};
    string str(cVec.begin(), cVec.end());
    cout << str << endl;
    return 0;
}