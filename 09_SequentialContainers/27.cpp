/**
 *  \file 27.cpp
 *  \brief Exercise 9.27: Write a program to find and remove the odd-valued elements in a
 *  forward_list<int>.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <forward_list>
using std::forward_list;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    forward_list<int> fwd_list = {0, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(fwd_list, getName(fwd_list));
    
    auto prev = fwd_list.before_begin();
    auto current = fwd_list.begin();
    while(current != fwd_list.end())
    {
        if(*current % 2 == 1)
        {
            current = fwd_list.erase_after(prev);
        }
        else
        {
            prev = current;
            ++current;
        }
    }
    printElements(fwd_list, getName(fwd_list));
    return 0;
}