/**
 *  \file 14.cpp
 *  \brief Exercise 9.14: Write a program to assign the elements from a list of char* pointers
    to C-style character strings to a vector of strings.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << endl;
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    list<const char*> oldstyle{"A", "BC", "DFG"};
    printElements(oldstyle, getName(oldstyle));
    vector<string> v1;
    v1.assign(oldstyle.begin(), oldstyle.end());
    printElements(v1, getName(v1));
    return 0;
}