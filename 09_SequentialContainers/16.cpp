/**
 *  \file 16.cpp
 *  \brief Exercise 9.16: Repeat the previous program, but compare elements in a list<int>
    to a vector<int>.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    vector<int> v1 = {1, 3, 5, 7, 9, 12};
    list<int> l1 = {1, 3, 9};
    list<int> l2 = {1, 3, 5, 7, 9, 12};
    
    printElements(v1, getName(v1));
    printElements(l1, getName(l1));
    cout << getName(v1) << " == " << getName(l1) << " -> ";
    cout << std::boolalpha << (v1 == vector<int>(l1.begin(), l1.end())) << endl;

    printElements(v1, getName(v1));
    printElements(l2, getName(l2));
    cout << getName(v1) << " == " << getName(l2) << " -> ";
    cout << std::boolalpha << (v1 == vector<int>(l2.begin(), l2.end())) << endl;
    return 0;
}