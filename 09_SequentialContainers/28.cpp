/**
 *  \file 28.cpp
 *  \brief Exercise 9.28: Write a function that takes a forward_list<string> and two additional
 *  string arguments. The function should find the first string and insert the
 *  second immediately following the first. If the first string is not found, then insert the
 *  second string at the end of the list.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <forward_list>
using std::forward_list;

#define getName(var)  #var

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << endl;
    for (auto const& item : collection)
    {
        cout << item << endl;
    }
    cout << endl;
}

void findAndInsert(forward_list<string>& list,
    const string& item_to_be_searched,
    const string& item_to_be_inserted)
{
    auto prev = list.before_begin();
    auto current = list.begin();
    while(current != list.end())
    {
        if(*current == item_to_be_searched)
        {
            current = list.insert_after(current, item_to_be_inserted);
            return;
        }
        else
        {
            prev = current;
            ++current;
        }
    }
    list.insert_after(prev, item_to_be_inserted);
}

int main()
{
    forward_list<string> itsy = {"The itsy bitsy spider",
                                 "Went up the water spout",
                                 "Down came the rain and",
                                 "Washed the spider out",
                                 "Out came the sun",
                                 "And dried up all the rain",
                                 "Now the itsy bitsy spider",
                                 "Went up the spout again"};
    printElements(itsy, getName(itsy));
    findAndInsert(itsy, "And dried up all the rain", "Now the itsy bitsy spider");
    printElements(itsy, getName(itsy));
    findAndInsert(itsy, "Baby shark", "Ta da!");
    printElements(itsy, getName(itsy));
    return 0;
}