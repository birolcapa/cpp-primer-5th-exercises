/**
 *  \file 20.cpp
 *  \brief Exercise 9.20: Write a program to copy elements from a list<int> into two deques.
    The even-valued elements should go into one deque and the odd ones into the other.
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <list>
using std::list;

#include <deque>
using std::deque;


#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    list<int> numbers {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    deque<int> odds;
    deque<int> evens;

    for (auto const& item : numbers)
    {
        (item % 2 == 1 ? odds : evens).push_back(item);
    }

    printElements(numbers, getName(numbers));
    printElements(odds, getName(odds));
    printElements(evens, getName(evens));

    return 0;
}