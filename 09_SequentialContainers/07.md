Exercise 9.7: What type should be used as the index into a vector of ints?

`size_type`: Unsigned integral type big enough to hold the size of the largest possible container of this container type.

As a result:
```c++
vector<int>::size_type
```