/**
 *  \file 10.cpp
 *  \brief Exercise 9.10: What are the types of the following four objects?
    vector<int> v1;
    const vector<int> v2;
    auto it1 = v1.begin(), it2 = v2.begin();
    auto it3 = v1.cbegin(), it4 = v2.cbegin();
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <typeinfo>

int main()
{
    vector<int> v1;
    const vector<int> v2;
    
    // Following line gives error as following:
    // error C3538: in a declarator-list 'auto' must always deduce to the same type
    // auto it1 = v1.begin(), it2 = v2.begin();
    // Therefore, it shall be:
    auto it1 = v1.begin(); // vector<int>::iterator
    cout << typeid(it1).name() << endl;

    auto it2 = v2.begin(); // vector<int>::const_iterator
    cout << typeid(it2).name() << endl;
    
    auto it3 = v1.cbegin(), it4 = v2.cbegin(); // vector<int>::const_iterator and vector<int>::const_iterator
    cout << typeid(it3).name() << endl;
    cout << typeid(it4).name() << endl;
    
    return 0;
}