/**
 *  \file 31.cpp
 *  \brief Exercise 9.31: The program on page 354 to remove even-valued elements and duplicate
 *  odd ones will not work on a list or forward_list. Why? Revise the program
 *  so that it works on these types as well.
 */

#include "print.h"

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <forward_list>
using std::forward_list;

void testVectorImplementation()
{
    vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(vi);
    auto iter = vi.begin();
    while(iter != vi.end())
    {
        if(*iter % 2 == 1)
        {
            iter = vi.insert(iter, *iter); // duplicate the current element
            iter += 2; // advance past this element and the one inserted before it
        }
        else
        {
            iter = vi.erase(iter); // remove even elements
            // do not advance the iterator, 
            // iter denotes the element after the one we erased
        }
    }
    printElements(vi);
}

void testListImplementation()
{
    list<int> li = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(li);
    auto iter = li.begin();
    while(iter != li.end())
    {
        if(*iter % 2 == 1)
        {
            iter = li.insert(iter, *iter);
            // binary '+=': 'std::_List_iterator<std::_List_val<std::_List_simple_types<_Ty>>>' 
            // does not define this operator or a conversion to a type acceptable to the predefined operator
            //so iter += 2; is not possible
            ++iter; // Increments iter to refer to the next element in the container.
            ++iter; // Increments iter to refer to the next element in the container.
        }
        else
        {
            iter = li.erase(iter);
        }
    }
    printElements(li);
}

void testForwardListImplementation()
{
    forward_list<int> fwd = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(fwd);
    auto iter = fwd.begin();
    auto prev = fwd.before_begin();
    while(iter != fwd.end())
    {
        if(*iter % 2 == 1)
        {
            iter = fwd.insert_after(prev, *iter);
            ++iter;
            ++iter;
            ++prev;
            ++prev;
        }
        else
        {
            iter = fwd.erase_after(prev);
        }
    }
    printElements(fwd);
}

int main()
{
    testVectorImplementation();
    testListImplementation();
    testForwardListImplementation();
    return 0;
}