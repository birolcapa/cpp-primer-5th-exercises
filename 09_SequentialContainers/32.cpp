/**
 *  \file 32.cpp
 *  \brief Exercise 9.32: In the program on page 354 would it be
 *  legal to write the call to insert as follows? If not, why not?
 *  iter = vi.insert(iter, *iter++);
 *  
 *  The answer is NO:
 *  https://en.cppreference.com/w/cpp/language/eval_order
 *  Order of evaluation of any part of any expression, including
 *  order of evaluation of function arguments is unspecified (with
 *  some exceptions listed below). The compiler can evaluate
 *  operands and other subexpressions in any order, and may choose
 *  another order when the same expression is evaluated again.
 *  
 *  So we do not know 
 *  vi.insert(iter, *iter++)
 *  which one will happen first: 
 *  iter could be the original or original + 1
 *  
 */

#include "print.h"

#include <vector>
using std::vector;

void testVectorImplementation()
{
    vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(vi);
    auto iter = vi.begin();
    while(iter != vi.end())
    {
        if(*iter % 2 == 1)
        {
            //Causes an Infinite Loop at my current setup
            iter = vi.insert(iter, *iter++); // duplicate the current element
            //iter += 2; // advance past this element and the one inserted before it
            std::cout << "inf" << std::endl;
        }
        else
        {
            iter = vi.erase(iter); // remove even elements
            // do not advance the iterator, 
            // iter denotes the element after the one we erased
        }
    }
    printElements(vi);
}

int main()
{
    testVectorImplementation();
    return 0;
}