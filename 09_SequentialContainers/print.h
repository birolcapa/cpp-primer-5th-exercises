#pragma once

#include <iostream>

#define getName(variable) #variable
#define printElements(collection) printElementsHelper(collection, getName(collection))

template <typename T>
void printElementsHelper(const T& collection, const char *collection_name)
{
    std::cout << "Printing: " << collection_name << std::endl;
    for (auto const& item : collection)
    {
        std::cout << item << ' ';
    }
    std::cout << std::endl;
}