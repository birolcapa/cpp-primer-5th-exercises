/**
 *  \file 21.cpp
 *  \brief Exercise 9.21: Explain how the loop from page 345 that used the return from insert to 
    add elements to a list would work if we inserted into a vector instead.
    First page 345:

    ```cpp
    list<string> lst;
    auto iter = lst.begin();
    while (cin >> word)
        iter = lst.insert(iter, word); // same as calling push_front`
    ```
    Before the loop, initialize iter to lst.begin(). 

    The first call to insert takes the string we just read and puts it in front of the element denoted by iter.

    The value returned by insert is an iterator referring to this new element. 

    We assign that iterator to iter and repeat the while, reading another word. 

    As long as there are words to insert, each trip through the while inserts a new element ahead of iter and 
    reassigns to iter the location of the newly inserted element.

    That element is the (new) first element. Thus, each iteration inserts an element ahead of the first element in the list.

    Remember:
    vector Flexible-size array. Supports fast random access.
    Inserting or deleting elements other than at the back may be slow.

    list Doubly linked list. Supports only bidirectional sequential access.
    Fast insert/delete at any point in the list.

    So the cost would be higher if we used vector.

    1. Trial
    list:   Elapsed time in microseconds :   204715 microseconds
    vector: Elapsed time in microseconds : 56502574 microseconds
    
    2. Trial
    list:   Elapsed time in microseconds :   199028 microseconds
    vector: Elapsed time in microseconds : 54931323 microseconds
 */
 
#include <iostream>
using std::cout;
using std::endl;

#include <list>
using std::list;

#include <vector>
using std::vector;

#include <chrono>

template <typename T1, typename T2>
void MeasurePerformance(const T1& dataCollection, T2& collectionUnderTest)
{
    auto start = std::chrono::steady_clock::now();
    auto iter = collectionUnderTest.begin();
    for (auto const& item : dataCollection)
    {
        iter = collectionUnderTest.insert(iter, item); // same as calling push_front`
    }
    auto end = std::chrono::steady_clock::now();
    cout << "Elapsed time in microseconds : " 
        << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
        << " microseconds" << endl;
}

int main()
{
    list<int> veryBigList;
    for(int i = 0; i < 1000000; ++i) 
    {
        veryBigList.push_back(i);
    }

    list<int> testListPerformance;
    MeasurePerformance(veryBigList, testListPerformance);

    vector<int> testVectorPerformance;
    MeasurePerformance(veryBigList, testVectorPerformance);
    return 0;
}
