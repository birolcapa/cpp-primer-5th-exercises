/**
 * @file 42.cpp
 * @brief Exercise 9.42: Given that you want to read a character at a time into a string, and
 * you know that you need to read at least 100 characters, how might you improve the
 * performance of your program?
 * 
 * firstStringUnderTest.reserve(collectionSize); helps improving performance.
 * The reserve operation lets us tell the container how many elements it should be prepared to hold.
 * 
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <chrono>

int main()
{
    const unsigned int collectionSize = 1000000;
    vector<char> dataCollection;
    for(unsigned int i = 0; i < collectionSize; ++i) 
    {
        dataCollection.push_back('A');
    }

    auto start = std::chrono::steady_clock::now();
    string firstStringUnderTest;
    firstStringUnderTest.reserve(collectionSize);
    for (const auto& ch : dataCollection)
    {
        firstStringUnderTest.push_back(ch);
    }
    auto end = std::chrono::steady_clock::now();
    cout << "Elapsed time in microseconds : " 
        << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
        << " microseconds" << endl;
    // Elapsed time in microseconds : 2493 microseconds
    
    start = std::chrono::steady_clock::now();
    string secondStringUnderTest;
    for (const auto& ch : dataCollection)
    {
        secondStringUnderTest.push_back(ch);
    }
    end = std::chrono::steady_clock::now();
    cout << "Elapsed time in microseconds : " 
        << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
        << " microseconds" << endl;
    // Elapsed time in microseconds : 2820 microseconds
    return 0;
}