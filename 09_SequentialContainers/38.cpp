/**
 *  \file 38.cpp
 *  \brief Exercise 9.38: Write a program to explore how vectors grow in the library you use.
 *  Following program gives output as:
 *  vi: size: 0 capacity: 0
 *  vi: size: 24 capacity: 28
 */
 
#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

void showHowVectorsGrow()
{
    vector<int> vi;
    cout << "vi: size: " << vi.size() << " capacity: " << vi.capacity() << endl;
    for (vector<int>::size_type ix = 0; ix != 24; ++ix)
    {
        vi.push_back(ix);
    }
    cout << "vi: size: " << vi.size() << " capacity: " << vi.capacity() << endl;
}

int main()
{
    showHowVectorsGrow();
    return 0;
}
