/**
 *  \file 34.cpp
 *  \brief Exercise 9.34: Assuming vi is a container of ints that includes even and odd values,
 *  predict the behavior of the following loop. After you’ve analyzed this loop, write a
 *  program to test whether your expectations were correct.
 *  
 *  iter = vi.begin();
 *  while (iter != vi.end())
 *      if (*iter % 2)
 *          iter = vi.insert(iter, *iter);
 *  ++iter;
 *  The statement above causes an infinite loop if container has odd values. 
 *   *  Imagine vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
 *  when iter is at 1----------------|
 *  if (1 % 2 == 1) -> iter = vi.insert(iter, *iter); -> 0, 1, 1, 2, ...
 *  ++iter; -> iter is now showing newly added 1 --------------|
 *  if (1 % 2 == 1) -> iter = vi.insert(iter, *iter); -> 0, 1, 1, 1, 2, ...
 *  ++iter; -> iter is now showing newly added 1 ------------------|  
 *  So it is an infinite loop
 *  
 *  The statement above misses brackets. while loop shall become
 *  while (iter != vi.end())
 *  {
 *      if (*iter % 2)
 *          iter = vi.insert(iter, *iter);
 *      ++iter;
 *  }
 *  But again. imagine vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
 *  when iter is at 1-----------------------|
 *  if (1 % 2 == 1) -> iter = vi.insert(iter, *iter); -> 0, 1, 1, 2, ...
 *  ++iter; -> iter is now showing newly added 1 --------------|
 *  if (1 % 2 == 1) -> iter = vi.insert(iter, *iter); -> 0, 1, 1, 1, 2, ...
 *  ++iter; -> iter is now showing newly added 1 ------------------|  
 *  So it is an infinite loop
 *  
 *  To avoid this, we need to ++iter even in the if statement as following so that we end up with
 *  0 1 1 2 3 3 4 5 5 6 7 7 8 9 9
 */

#include "print.h"

#include <vector>
using std::vector;

void testVectorImplementation()
{
    vector<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(vi);
    auto iter = vi.begin();
    while(iter != vi.end())
    {
        if(*iter % 2 == 1)
        {
            iter = vi.insert(iter, *iter);
            ++iter;
        }
        ++iter;
    }
    printElements(vi);
}

int main()
{
    testVectorImplementation();
    return 0;
}