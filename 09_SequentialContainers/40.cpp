/**
 *  \file 40.cpp
 *  \brief Exercise 9.40: If the program in the previous exercise reads 256 words, what is its
 *  likely capacity after it is resized? What if it reads 512? 1,000? 1,048?
 *
 *  Sample output when using compile.bat
 *
 *  256:
 *  before resize -> svec: size: 256 capacity: 316
 *  after resize -> svec: size: 384 capacity: 474
 *
 *  512:
 *  before resize -> svec: size: 512 capacity: 711
 *  after resize -> svec: size: 768 capacity: 1066
 *
 *  1000:
 *  before resize -> svec: size: 1000 capacity: 1066
 *  after resize -> svec: size: 1500 capacity: 1599
 *
 *  1048:
 *  before resize -> svec: size: 1048 capacity: 1066
 *  after resize -> svec: size: 1572 capacity: 1599
 */

/**
 *  \file 39.cpp
 *  \brief Exercise 9.39: Explain what the following program fragment does:
 *      vector<string> svec;
 *      svec.reserve(1024);
 *      string word;
 *      while (cin >> word)
 *          svec.push_back(word);
 *      svec.resize(svec.size()+svec.size()/2);
 */

#include "print.h"
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <sstream>
using std::stringstream;

void testSizeAndCapacity(std::istream& ss)
{
    // create vector of strings
    vector<string> svec;
    
    // sets capacity to at least 1024; might be more
    svec.reserve(1024);
    
    // create an empty string
    string word;
    
    // get console input and put it into vector of strings
    while (ss >> word)
    {
        svec.push_back(word);
    }
    cout << "before resize -> svec: size: " << svec.size() << " capacity: " << svec.capacity() << endl;
    // Resize svec so that it has "svec.size()+svec.size()/2" elements. 
    // If n < svec.size(), the excess elements are discarded.
    // If new elements must be added, they are value initialized.
    svec.resize(svec.size()+svec.size()/2);
    cout << "after resize -> svec: size: " << svec.size() << " capacity: " << svec.capacity() << endl;
}

stringstream createTestInput(size_t size, const string& dummy)
{
    vector<string> words(size, dummy);
    stringstream ss;
    for(const auto& word : words)
    {
        ss << word << endl;
    }
    return ss;
}

int main()
{
    cout << "Test with 256 inputs "<< endl;
    stringstream testInput = createTestInput(256, "dummy");
    testSizeAndCapacity(testInput);

    cout << "Test with 512 inputs "<< endl;
    testInput = createTestInput(512, "dummy");
    testSizeAndCapacity(testInput);

    cout << "Test with 1000 inputs "<< endl;
    testInput = createTestInput(1000, "dummy");
    testSizeAndCapacity(testInput);

    cout << "Test with 1048 inputs "<< endl;
    testInput = createTestInput(1048, "dummy");
    testSizeAndCapacity(testInput);
    return 0;
}