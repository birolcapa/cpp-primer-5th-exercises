/**
 * \file 25.cpp
 * \brief Exercise 9.25: In the program on page 349 that erased a range of elements, what happens
 * if elem1 and elem2 are equal? What if elem2 or both elem1 and elem2 are the
 * off-the-end iterator?
*/

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <list>
using std::list;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    list<int> lst = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto it = lst.begin();
    while(it != lst.end())
    {
        if (*it % 2 == 0)
        {
            it = lst.erase(it);
        }
        else
        {
            ++it;
        }
    }
    printElements(lst, getName(lst));

    list<int> lst2 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(lst2, getName(lst2));
    auto zeroIter = lst2.begin();
    std::advance(zeroIter,2);
    auto threeIter = zeroIter;
    cout << *zeroIter << endl;
    cout << *threeIter << endl;
    // delete the range of elements between two iterators
    // returns an iterator to the element just after the last removed element
    //auto elem1 = lst2.erase(elem1, elem2); // after the call elem1 == elem2

    return 0;
}
