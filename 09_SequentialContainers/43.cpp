/**
 * @file 43.cpp
 * @brief Exercise 9.43: Write a function that takes three strings, s, oldVal, and newVal.
 * Using iterators, and the insert and erase functions replace all instances of oldVal
 * that appear in s by newVal. Test your function by using it to replace common abbre-
 * viations, such as “tho” by ”though” and ”thru” by “through”.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

void replaceExercise(string& sentence, const string& oldVal, const string& newVal)
{
    const auto oldValSize = oldVal.size();
    const auto newValSize = newVal.size();
    auto it = sentence.begin();
    while(it != sentence.end())
    {
        string subOldVal{it, it + oldValSize};
        if (subOldVal == oldVal)
        {
            sentence.erase(it, it + oldValSize);
            it = sentence.insert(it, newVal.begin(), newVal.end());
            //it will become invalid, after insertion. 
            // so it will still point to the original value with this statement
            it += newValSize;
        }
        else
        {
            ++it;
        }
    }
}

int main()
{
    string sentence0{};
    replaceExercise(sentence0, {}, {});
    cout << sentence0 << endl;

    string sentence1 {"the drinks were expensive, tho the cocktail selection was good."};
    replaceExercise(sentence1, "tho", "though");
    cout << sentence1 << endl;

    string sentence2 {"I didn't think season two was as good as season one. Still good, tho!"};
    replaceExercise(sentence2, "tho", "though");
    cout << sentence2 << endl;

    string sentence3 {"They send their children thru school."};
    replaceExercise(sentence3, "thru", "through");
    cout << sentence3 << endl;

    string sentence4 {"When the rocks come thru the holes! And from there they went on westward thru the mountains."};
    replaceExercise(sentence4, "thru", "through");
    cout << sentence4 << endl;
    return 0;
}