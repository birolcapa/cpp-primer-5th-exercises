/**
 * \file 24.cpp
 * \brief Exercise 9.24: Write a program that fetches the first element in a vector using at,
 * the subscript operator, front, and begin. Test your program on an empty vector.
*/

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

int main()
{
    vector<int> c;

    //auto val = c.at(0); // _Xout_of_range("invalid vector<T> subscript");
    //auto val = c[0]; // Unhandled exception thrown: read access violation.
    //auto val = c.front(); // Unhandled exception thrown: read access violation.
    //auto val = *c.begin(); // Access violation reading location 0x00000000

    return 0;
}
