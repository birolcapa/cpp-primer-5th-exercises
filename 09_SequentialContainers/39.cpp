/**
 *  \file 39.cpp
 *  \brief Exercise 9.39: Explain what the following program fragment does:
 *      vector<string> svec;
 *      svec.reserve(1024);
 *      string word;
 *      while (cin >> word)
 *          svec.push_back(word);
 *      svec.resize(svec.size()+svec.size()/2);
 */

#include "print.h"

#include <vector>
using std::vector;

#include <string>
using std::string;

void explainWhatThisProgramDoes()
{
    // create vector of strings
    vector<string> svec;
    
    // sets capacity to at least 1024; might be more
    svec.reserve(1024);
    
    // create an empty string
    string word;
    
    // get console input and put it into vector of strings
    while (std::cin >> word)
    {
        svec.push_back(word);
    }
    std::cout << "svec: size: " << svec.size() << " capacity: " << svec.capacity() << std::endl;
    printElements(svec);
    // Resize svec so that it has "svec.size()+svec.size()/2" elements. 
    // If n < svec.size(), the excess elements are discarded.
    // If new elements must be added, they are value initialized.
    svec.resize(svec.size()+svec.size()/2);
    std::cout << "svec: size: " << svec.size() << " capacity: " << svec.capacity() << std::endl;
    printElements(svec);
}

int main()
{
    std::cout << "Start" << std::endl;
    explainWhatThisProgramDoes();
    return 0;
}