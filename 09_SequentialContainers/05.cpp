/**
 *  \file 05.cpp
 *  \brief Exercise 9.5
 *  Rewrite the previous program to return an iterator to the requested element.
 *  Note that the program must handle the case where the element is not found.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

// const_iterator is used, because loop will not change anything inside container
vector<int>::const_iterator
findTheVal(vector<int>::const_iterator begin, vector<int>::const_iterator end, int value)
{
    for(auto iter = begin; iter != end; ++iter)
    {
        if(*iter == value)
        {
            return iter;
        }
    }
    return end;
}

void checkTheVal(vector<int> input, int value)
{
    auto iter = findTheVal(input.cbegin(), input.cend(), value);
    if(iter == input.end())
    {
        cout << "Finding result is: 0 " << endl;
    }
    else
    {
        cout << "Finding result is: 1 " << 
            "Position is: " << iter - input.cbegin() << endl;
    }
}

int main()
{
    vector<int> input{1, 2, 3, 4, 5};
    checkTheVal(input, 6);
    checkTheVal(input, 3);
    return 0;
}