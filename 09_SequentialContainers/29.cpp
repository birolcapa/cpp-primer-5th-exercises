/**
 *  \file 29.cpp
 *  \brief Exercise 9.29: Given that vec holds 25 elements, what does vec.resize(100) do?
 *  What if we next wrote vec.resize(10)?
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <vector>
using std::vector;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, const string& variableName)
{
    cout << "Printing: " << variableName << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

int main()
{
    vector<int> vec(25, 42); // 25 ints; each has value 42
    printElements(vec, getName(vec));

    vec.resize(100); // adds 75 elements of value 0 to the back of the vec
    printElements(vec, getName(vec));

    vec.resize(10); // erases 90 elements from the back of vec
    printElements(vec, getName(vec));
    
    return 0;
}