/**
 *  \file 33.cpp
 *  \brief Exercise 9.33: In the final example in this section what
 *  would happen if we did not assign the result of insert to begin?
 *  Write a program that omits this assignment to see if your
 *  expectation was correct.
 *  
 *  Answer:
 *  After insertion, iterator begin will be invalidated. 
 *  Therefore the program crashes.
 *  From the book:
 *  It is a serious run-time error to use an iterator, pointer, or reference that has been invalidated.
 */

#include "print.h"

#include <vector>
using std::vector;

int main()
{
    vector<int> v = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printElements(v);
    auto begin = v.begin();
    while (begin != v.end())
    {
        ++begin;
        std::cout << &begin << std::endl;
        // instead of following
        // begin = v.insert(begin, 42);
        // do this according to exercise:
        v.insert(begin, 42);
        // after insertion, iterator begin will be invalidated. 
        // therefore the program crashes.
        // From the book:
        // It is a serious run-time error to use an iterator, pointer, or reference that has been invalidated.
        std::cout << *begin << std::endl;
        ++begin;
    }
    printElements(v);
    return 0;
}