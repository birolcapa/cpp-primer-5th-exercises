/**
 *  \file 11.cpp
 *  \brief Exercise 9.11: Show an example of each of the six ways to create and initialize a
    vector. Explain what values each vector contains.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

void printElements(vector<int>& collection)
{
    std::cout << "Printing: ";
    for (auto const& item : collection)
    {
        std::cout << item << ' ';
    }
    std::cout << std::endl;
}

int main()
{
    vector<int> v1; // Default constructor: Empty vector
    printElements(v1);

    vector<int> v2(v1); // v2 is a copy of v1: Empty vector
    printElements(v2);

    vector<int> v3 = v1; // v3 is a copy of v1: Empty vector
    printElements(v3);
    
    vector<int> v4{1, 2, 3, 4}; // v4 is a copy of the elements in the initializer list
    printElements(v4);
    
    vector<int> v5 = {1, 2, 3, 4, 5}; // v5 is a copy of the elements in the initializer list
    printElements(v5);

    vector<int> v6(v5.begin(), v5.end()); // v6 is a copy of the elements in the range 
    // denoted by iterators v5.begin() and v5.end().
    printElements(v6);

    vector<int> v7(5); // v7 has 5 elements: 0, 0, 0, 0, 0
    printElements(v7);

    vector<int> v8(8, 42); //v8 has 8 elements: 42, 42, 42, 42, 42, 42, 42, 42
    printElements(v8);
    
    return 0;
}
