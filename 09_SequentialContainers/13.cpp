/**
 *  \file 13.cpp
 *  \brief Exercise 9.13: How would you initialize a vector<double> from a list<int>?
    From a vector<int>? Write code to check your answers.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <list>
using std::list;

template <typename T>
void printElements(const vector<T>& collection)
{
    std::cout << "Printing: ";
    for (auto const& item : collection)
    {
        std::cout << item << ' ';
    }
    std::cout << std::endl;
}

int main()
{
    list<int> list_of_ints{1, 2, 3};
    vector<double> v1{list_of_ints.begin(), list_of_ints.end()};
    printElements(v1);

    vector<int> vector_of_ints = {10, 20, 30};
    vector<double> v2{vector_of_ints.begin(), vector_of_ints.end()};
    printElements(v2);

    return 0;
}