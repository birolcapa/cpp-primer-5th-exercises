/**
 *  \file 19.cpp
 *  \brief Exercise 9.19: Rewrite the program from the previous exercise to use a list. List the
    changes you needed to make.
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <list>
using std::list;

int main()
{
    list<string> inputs;
    string input;

    while(cin >> input)
    {
        inputs.push_back(input);
    }

    for (auto const& item : inputs)
    {
        cout << item << ' ';
    }
    cout << endl;

    return 0;
}