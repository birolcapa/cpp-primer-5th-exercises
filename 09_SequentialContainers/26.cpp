/**
 *  \file 26.cpp
 *  \brief Exercise 9.26: Using the following definition of ia, copy ia into a vector and into a
 *  list. Use the single-iterator form of erase to remove the elements with odd values
 *  from your list and the even values from your vector.
 *  int ia[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 55, 89 };
 */

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <list>
using std::list;

#define getName(var)  #var 

template <typename T>
void printElements(const T& collection, string x)
{
    cout << "Printing: " << x << " ";
    for (auto const& item : collection)
    {
        cout << item << ' ';
    }
    cout << endl;
}

template <typename T>
void removeValues(T& collection, int x)
{
    auto it = collection.begin();
    while(it != collection.end())
    {
        if (*it % 2 == x)
        {
            it = collection.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

template <typename T>
void removeOddValues(T& collection)
{
    removeValues(collection, 1);
}

template <typename T>
void removeEvenValues(T& collection)
{
    removeValues(collection, 0);
}

int main()
{
    int ia[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 55, 89 };
    
    vector<int> vector_of_ints{std::begin(ia), std::end(ia)};
    printElements(vector_of_ints, getName(vector_of_ints));
    removeEvenValues(vector_of_ints);
    cout << "Even values are removed: " << endl;
    printElements(vector_of_ints, getName(vector_of_ints));

    cout << "************************" << endl;

    list<int> list_of_ints{std::begin(ia), std::end(ia)};
    printElements(list_of_ints, getName(list_of_ints));
    removeOddValues(list_of_ints);
    cout << "Odd values are removed: " << endl;
    printElements(list_of_ints, getName(list_of_ints));
    
    return 0;
}