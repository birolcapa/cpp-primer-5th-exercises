/**
 *  \file 18.cpp
 *  \brief Exercise 9.18: Write a program to read a sequence of strings from the standard input
    into a deque. Use iterators to write a loop to print the elements in the deque.
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <deque>
using std::deque;

int main()
{
    deque<string> inputs;
    string input;

    while(cin >> input)
    {
        inputs.push_back(input);
    }

    for (auto const& item : inputs)
    {
        cout << item << ' ';
    }
    cout << endl;

    return 0;
}