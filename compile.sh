#!/bin/bash
src="$1"
outputFile="${src%.*}.out"
echo $src
g++ -Wall -Wextra -pedantic -Wshadow -Werror \
    -std=c++11 -O3 -o "$outputFile" "$src"
./"$outputFile" ${@:2}