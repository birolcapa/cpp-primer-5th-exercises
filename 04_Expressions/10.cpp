/**
 *  \file 10.cpp
 *  \brief Exercise 4.10:
 *  Write the condition for a while loop that would read ints from the
 *  standard input and stop when the value read is equal to 42.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    int i = 0;
    while(cin >> i && i != 42) {
        cout << i << endl;
    }
    cout << "You entered 42: exiting the loop" << endl;
    return 0;
}