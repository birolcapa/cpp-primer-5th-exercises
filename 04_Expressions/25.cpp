Exercise 4.25: What is the value of ~'q' << 6 on a machine with 32-bit ints and 8 bit
chars, that uses Latin-1 character set in which ’q’ has the bit pattern 01110001?


~'q' << 6 -> ~('q')<< 6
Since it is char 

Info:
As usual, if an operand is a “small integer,” its value is first promoted (§ 4.11.1,
p. 160) to a larger integral type. The operand(s) can be either signed or unsigned

it will be promoted to int:
'q'            -> 00000000 00000000 00000000 01110001
~('q')      -> 11111111 11111111 11111111 10001110
Since it promoted to int. ~('q') means a negative int value.
Info: https://stackoverflow.com/questions/28141871/bitwise-operators-and-signed-types
"doing a left shift that changes the value of the sign bit is undefined"
~('q') << 6 -> 11111111 11111111 11100011 10000000