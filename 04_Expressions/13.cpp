/**
 *  \file 13.cpp
 *  \brief Exercise 4.13:
 *  What are the values of i and d after each assignment?
 *  int i; double d;
 *  (a) d = i = 3.5; (b) i = d = 3.5;
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    int i; double d;
    d = i = 3.5;
    cout << "d is " << d << " i is " << i << endl; // 3 3
    i = d = 3.5;
    cout << "d is " << d << " i is " << i << endl; // 3.5 3
    return 0;
}