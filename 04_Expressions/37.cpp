/**
 *  \file 37.cpp
 *  \brief Exercise 4.37:
 *  Rewrite each of the following old-style casts to use a named cast:
 *  int i; double d; const string *ps; char *pc; void *pv;
 *  (a) pv = (void*)ps; 
 *  (b) i = int(*pc);
 *  (c) pv = &d; 
 *  (d) pc = (char*) pv;
 */

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;

void old_style_cast()
{
    int i; double d; const string *ps; char *pc; void *pv;
    pv = (void*)ps; 
    i = int(*pc);
    pv = &d; 
    pc = (char*) pv;
}

void new_style_cast()
{
    int i; double d; const string *ps; char *pc; void *pv;
    pv = const_cast<string*>(ps); 
    i = static_cast<int>(*pc);
    pv = static_cast<void*>(&d); 
    pc = static_cast<char*>(pv);
}

int main()
{
    old_style_cast();
    new_style_cast();
    return 0;
}