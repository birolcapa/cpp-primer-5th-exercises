/**
 *  \file 34.cpp
 *  \brief Exercise 4.34:
 *  Given the variable definitions in this section, explain what conversions
 *  take place in the following expressions:
 *  (a) if (fval) (b) dval = fval + ival; (c) dval + ival * cval;
 *  Remember that you may need to consider the associativity of the operators.
 */

#include <iostream>

int main()
{
    bool flag; char cval;
    short sval; unsigned short usval;
    int ival; unsigned int uival;
    long lval; unsigned long ulval;
    float fval; double dval;
    3.14159L + 'a'; // 'a' promoted to int, then that int converted to long double
    dval + ival; // ival converted to double
    dval + fval; // fval converted to double
    ival = dval; // dval converted (by truncation) to int
    flag = dval; // if dval is 0, then flag is false, otherwise true
    cval + fval; // cval promoted to int, then that int converted to float
    sval + cval; // sval and cval promoted to int
    cval + lval; // cval converted to long
    ival + ulval; // ival converted to unsigned long
    usval + ival; // promotion depends on the size of unsigned short and int
    uival + lval; // conversion depends on the size of unsigned int and long
    
    if (fval) {}  // fval converted to bool 
    dval = fval + ival; // ival converted to float, then the result (of fval add ival) converted to double
    dval + ival * cval; // cval promoted to int, then the resulting int (of ival * cval) is converted to double
    return 0;
}