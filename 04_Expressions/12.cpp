/**
 *  \file 12.cpp
 *  \brief Exercise 4.12:
 *  Assuming i, j, and k are all ints, explain what i != j < k means.
 */

According to operator precedence, 
i != j < k is same as 
i != (j < k).
the result of (j < k) is bool.
int != bool -> not a good way to compare
when a bool is converted to another arithmetic type, false converts to 0 and true converts to 1

Since i is an int, the bool will be converted to int, which means i != 1 or i != 0 is evaluated.