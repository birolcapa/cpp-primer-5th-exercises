/**
 *  \file 36.cpp
 *  \brief Exercise 4.36:
 *  Assuming i is an int and d is a double write the expression i *= d so
 *  that it does integral, rather than floating-point, multiplication.
 *  
 *  int i;
 *  double d;
 *  i *= d // normally i promoted to double and the result will be truncated to int
 *  // we need an explicit conversion
 *  i*= static_cast<int>(d);
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    // init the values
    int i = 5;
    double d = 3.65;
    cout << (i *= d) << endl;
    // reset the values
    i = 5;
    d = 3.65;
    cout << (i *= static_cast<int>(d)) << endl;
    return 0;
}