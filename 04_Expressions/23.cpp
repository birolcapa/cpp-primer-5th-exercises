/**
 *  \file 23.cpp
 *  \brief Exercise 4.23:
 *  The following expression fails to compile due to operator precedence.
 *  Using Table 4.12 (p. 166), explain why it fails. How would you fix it?
 *  Info:
 *  conditional operator precedence is lower than arithmetic operator
 *  string s = "word";
 *  string pl = s + s[s.size() - 1] == ’s’ ? "" : "s" ;
 *  // I think the intent is when plural, add s to word:
 *  string pl = s + (s[s.size() - 1] == ’s’ ? "" : "s");
 */

#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::string;

int main()
{
    string s = "word";
    // string pl = s + s[s.size() - 1] == 's' ? "" : "s" ;
    string pl = s + (s[s.size() - 1] == 's' ? "" : "s");
    cout << pl << endl;
    return 0;
}