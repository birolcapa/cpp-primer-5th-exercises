/**
 *  \file 31.cpp
 *  \brief Exercise 4.31:
 *  The program in this section used the prefix increment and decrement operators. 
 *  Explain why we used prefix and not postfix. 
 *  What changes would have to be made to use the postfix versions? 
 *  Rewrite the program using postfix operators.
 */

vector<int>::size_type cnt = ivec.size();
// assign values from size . . . 1 to the elements in ivec
for(vector<int>::size_type ix = 0; ix != ivec.size(); ++ix, --cnt)
    ivec[ix] = cnt;

/*
ADVICE: USE POSTFIX OPERATORS ONLY WHEN NECESSARY
Readers from a C background might be surprised that we use the prefix increment in the programs we’ve written. 
The reason is simple: 
The prefix version avoids unnecessary work. It increments the value and returns the incremented version. 
The postfix operator must store the original value so that it can return the unincremented value as its result. 
If we don’t need the unincremented value, there’s no need for the extra work done by the postfix operator.
For ints and pointers, the compiler can optimize away this extra work. 
For more complicated iterator types, this extra work potentially might be more costly. 
By habitually using the prefix versions, we do not have to worry about whether the performance difference matters. 
Moreover—and perhaps more importantly—we can express the intent of our programs more directly.
*/

// There are no changes if we write the loop with postfix versions:
for(vector<int>::size_type ix = 0; ix != ivec.size(); ix++, cnt--)
    ivec[ix] = cnt;