/**
 *  \file 28.cpp
 *  \brief Exercise 4.28: 
 *  Write a program to print the size of each of the built-in types.
 *  Info: Remember the table:
 *  Type Meaning Minimum Size
 *  bool boolean NA
 *  char character 8 bits
 *  wchar_t wide character 16 bits
 *  char16_t Unicode character 16 bits
 *  char32_t Unicode character 32 bits
 *  short short integer 16 bits
 *  int integer 16 bits
 *  long long integer 32 bits
 *  long long long integer 64 bits
 *  float single-precision floating-point 6 significant digits
 *  double double-precision floating-point 10 significant digits
 *  long double extended-precision floating-point 10 significant digits
 */

#include <iostream>
using std::cout;
using std::endl;
#include <string>
using std::string;

void print_size(const string& type, size_t size)
{
    cout <<  type << " is \t" << size << endl;
}

int main()
{
    // print_size("void", sizeof(print_size("test", sizeof(bool))));
    // 28.cpp(32): error C2070: 'void': illegal sizeof operand
    print_size("bool", sizeof(bool));
    print_size("char", sizeof(char));
    print_size("wchar_t", sizeof(wchar_t));
    print_size("char16_t", sizeof(char16_t));
    print_size("char32_t", sizeof(char32_t));
    print_size("short", sizeof(short));
    print_size("int\t", sizeof(int));
    print_size("long", sizeof(long));
    print_size("long long", sizeof(long long));
    print_size("float", sizeof(float));
    print_size("double", sizeof(double));
    print_size("long double", sizeof(long double));
    return 0;
}