/**
 *  \file 6.cpp
 *  \brief Exercise 4.6:
 *  Write an expression to determine whether an int value is even or odd.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    int i = 0;
    while(cin >> i) {
        cout << ((i%2 == 0) ? "even" : "odd") << endl;
    }
    return 0;
}