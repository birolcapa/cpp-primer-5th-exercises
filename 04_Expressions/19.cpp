/**
 *  \file 19.cpp
 *  \brief Exercise 4.19:
 *  Given that ptr points to an int, that vec is a vector<int>, and that
 *  ival is an int, explain the behavior of each of these expressions.
 *  Which, if any, are likely to be incorrect? Why? How might each be corrected?
 *  (a) ptr != 0 && *ptr++
 *  (b) ival++ && ival
 *  (c) vec[ival++] <= vec[ival]
 */

int *ptr;
vector<int> vec;
int ival;

/*
The expression *pbeg++ is usually confusing to programmers new to both C++
and C. However, because this usage pattern is so common, C++ programmers must
understand such expressions.
The precedence of postfix increment is higher than that of the dereference operator, so *pbeg++ is equivalent to *(pbeg++). 
The subexpression pbeg++ increments pbeg and yields a copy of the previous value of pbeg as its result.
Accordingly, the operand of * is the unincremented value of pbeg. 
Thus, the statementprints the element to which pbeg originally pointed and increments pbeg.
*/

// checks ptr is not a nullptr and if it is not null, 
// if it is not null
// The subexpression ptr++ increments ptr and yields a copy of the previous value of ptr as its result. Accordingly, the operand of * is the unincremented value of ptr. Thus, the statement
// prints the element to which ptr originally pointed and increments pptr.
ptr != 0 && *ptr++

// ival++: ival value is incremented: it the result is nonzero, 
// ival is also evaluated as true, result is true.
ival++ && ival

// Remember That Operands Can Be Evaluated in Any Order
vec[ival++] <= vec[ival]
// it may be corrected as may be
++ival;
vec[ival] <= vec[ival + 1]
