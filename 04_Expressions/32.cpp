/**
 *  \file 32.cpp
 *  \brief Exercise 4.32:
 *  Explain the following loop.
 *  
 *  constexpr int size = 5;
 *  int ia[size] = {1,2,3,4,5};
 *  
 *  for (int *ptr = ia, ix = 0; ix != size && ptr != ia+size; ++ix, ++ptr) 
 *  { some code }
 *  
 *  int* ptr = ia -> pointer to int points ia[0] element,
 *  till it is not equal to one past last element, and ix is not equal to size
 *  loop continues. 
 *  
 *  int ix = 0, 
 *  till it is not equal to size and ptr is not equal to one past last element
 *  loop continues.
 *  
 *  ix and ptr have the same functionality
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    constexpr int size = 5;
    int ia[size] = {1,2,3,4,5};
    
    for (int *ptr = ia, ix = 0; ix != size && ptr != ia+size; ++ix, ++ptr) 
    {
        cout << ia[ix] << " ";
    }
    cout << endl;
    
    // remove ix
    for (int *ptr = ia; ptr != ia+size; ++ptr) 
    {
        cout << *ptr << " ";
    }
    cout << endl;
    
    // remove ptr
    for (int ix = 0; ix != size; ++ix) 
    {
        cout << ia[ix] << " ";
    }
    cout << endl;

    return 0;
}