/**
 *  \file 21.cpp
 *  \brief Exercise 4.21:
 *  Write a program to use a conditional operator to find the elements in a
 *  vector<int> that have odd value and double the value of each such element
 */

#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

int main()
{
    vector<int> vec;
    for(int i = 0; i < 10;  i++) {
        vec.push_back(i);
    }
    
    for(auto &elem : vec) {
        cout << elem << " ";
    }
    cout << endl;
    
    for(auto &elem : vec) {
        elem = (elem % 2 == 0) ? elem : (2*elem);
    }
    
    for(auto &elem : vec) {
        cout << elem << " ";
    }
    cout << endl;
    
    return 0;
}