/**
 *  \file 11.cpp
 *  \brief Exercise 4.11:
 *  Write an expression that tests four values, a, b, c, and d, and 
 *  ensures that a is greater than b, which is greater than c, which is greater than d
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    int a = 0, b = 0, c = 0, d = 0;
    
    while(cin >> a >> b >> c >> d) {
        if(a > b && b > c && c > d)
        {
            cout << "well done my lord" << endl;
            break;
        }
        cout << "Condition must be a > b && b > c && c > d" << endl;    
    }
    
    return 0;
}