/**
 *  \file 7.cpp
 *  \brief Exercise 4.7:
 *  What does overflow mean? Show three expressions that will overflow
 *  Info:
 *  Overflow happens when a value is computed that is outside the range of values 
 *  that the type can represent.
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    short sval = 32767; // sval++ -> -32768
    cout << ++sval << endl;
    unsigned uval = 0; // --uval -> 4294967295
    cout << --uval << endl;
    unsigned short usval = 65535; // ++usval -> 0
    cout << ++usval << endl;
    return 0;
}
