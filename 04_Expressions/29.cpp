/**
 *  \file 29.cpp
 *  \brief Exercise 4.29:
 *  Predict the output of the following code and explain your reasoning.
 *  Now run the program. Is the output what you expected? If not, figure out why.
 *  
 *  int x[10]; 
 *  int *p = x;
 *  cout << sizeof(x)/sizeof(*x) << endl;
 *  cout << sizeof(p)/sizeof(*p) << endl;
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    int x[10]; 
    int *p = x;
    cout << sizeof(x)/sizeof(*x) << endl; // 10 x sizeof(int) / sizeof(int) = 10
    cout << sizeof(p)/sizeof(*p) << endl; // sizeof a pointer to int / sizeof(int)
    // info: https://stackoverflow.com/questions/6751749/what-is-the-size-of-a-pointer
    // info: https://stackoverflow.com/questions/2428765/why-the-size-of-a-pointer-is-4bytes-in-c
    return 0;
}