Exercise 4.17: Explain the difference between prefix and postfix increment

prefix:
Prefix form increments (or decrements) its operand and yields
the changed object as its result
The prefix operators return the object itself as an lvalue.

postfix:
The postfix operators increment (or decrement) the operand but yield a copy of the original, unchanged value as its result:
The postfix operators return a copy of the object’s original value as an rvalue.

int i = 0, j;
j = ++i; // j = 1, i = 1: prefix yields the incremented value
j = i++; // j = 1, i = 2: postfix yields the unincremented value