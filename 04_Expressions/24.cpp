/**
 *  \file 24.cpp
 *  \brief Exercise 4.24:
 *  Our program that distinguished between high pass, pass, and fail depended on the fact that 
 *  the conditional operator is right associative. Describe how that operator would be evaluated if the
 *  operator were left associative.
 *  
 *  The conditional operator is right associative, 
 *  meaning (as usual) that the operands group right to left. 
 *  
 *  if the operator were left associative:
 *  finalgrade = (grade > 90) ? "high pass" : (grade < 60) ? "fail" : "pass"; 
 *  would be like that:
 *  finalgrade = ((grade > 90) ? "high pass" : (grade < 60)) ? "fail" : "pass";
 *  
 *  So if grade > 90, it will return "high pass", so it will become:
 *  ("high pass") ? "fail" : "pass"
 *  (true) ? "fail" : "pas"
 *  the will be "fail"
 *  
 *  if grade < 90, it will return (grade < 60), so it will become:
 *      ((grade < 60)) ? "fail" : "pass";
 *      if grade < 60, the result will be "fail", else "pass"
 */