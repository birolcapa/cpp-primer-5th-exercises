/**
 *  \file 35.cpp
 *  \brief Exercise 4.35:
 *  Given the following definitions,
 *  char cval; int ival; unsigned int ui;
 *  float fval; double dval;
 *  identify the implicit type conversions, if any, taking place:
 *  (a) cval = ’a’ + 3; (b) fval = ui - ival * 1.0;
 *  (c) dval = ui * fval; (d) cval = ival + fval + dval;
 */

#include <iostream>
using std::cout;
using std::endl;

int main()
{
    bool flag; char cval;
    short sval; unsigned short usval;
    int ival; unsigned int uival;
    long lval; unsigned long ulval;
    float fval; double dval;
    unsigned int ui;

    3.14159L + 'a'; // 'a' promoted to int, then that int converted to long double
    dval + ival; // ival converted to double
    dval + fval; // fval converted to double
    ival = dval; // dval converted (by truncation) to int
    flag = dval; // if dval is 0, then flag is false, otherwise true
    cval + fval; // cval promoted to int, then that int converted to float
    sval + cval; // sval and cval promoted to int
    cval + lval; // cval converted to long
    ival + ulval; // ival converted to unsigned long
    usval + ival; // promotion depends on the size of unsigned short and int
    uival + lval; // conversion depends on the size of unsigned int and long
    
    if (fval) {}  // fval converted to bool 
    dval = fval + ival; // ival converted to float, then the result (of fval add ival) converted to double
    cout << "fval + ival: " << typeid(fval + ival).name() << endl;
    dval + ival * cval; // cval promoted to int, then the resulting int (of ival * cval) is converted to double
    cout << "ival * cval: " << typeid(ival * cval).name() << endl;
    cout << "dval + ival * cval: " << typeid(dval + ival * cval).name() << endl;
    
    cval = 'a' + 3; // 'a' promoted to int, then the result (of 'a' + 3) converted to char
    fval = ui - ival * 1.0; // ival converted to double, then the result (of ival * 1.0) is double. ui converted to double.
    //    the result is double (ui - ival * 1.0)
    // then the result of the expression converted(by truncation) to float
    cout << "ui - ival * 1.0: " << typeid(ui - ival * 1.0).name() << endl;
    dval = ui * fval; // ui is converted to float, then float converted to double.
    cout << "ui * fval: " << typeid(ui * fval).name() << endl;
    cval = ival + fval + dval; // ival converted to float, then the result of ival + fval converted to double.
    // Then it is added to dval. The result is converted to char (by truncation)
    cout << "ival + fval + dval: " << typeid(ival + fval + dval).name() << endl;
    
    return 0;
}