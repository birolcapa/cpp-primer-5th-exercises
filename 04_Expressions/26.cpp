Exercise 4.26: In our grading example in this section, what would happen if we used
unsigned int as the type for quiz1?

unsigned long quiz1 = 0; // we’ll use this value as a collection of bits

Info:
Table 2.1: C++: Arithmetic Types
Type    Meaning            Minimum Size
int     integer         16 bits
long     long integer     32 bits

Teacher has 30 students, who needs at least 30 bits. 
Unsigned long promises to have at least 32 bits on any machine.
And unsigned int promises to have at least 16 bits on any machine.
As a result unsigned int would not be enough to hold all results.