/**
 *  \file 22.cpp
 *  \brief Exercise 4.22:
 *  Extend the program that assigned high pass, pass, and fail grades to
 *  also assign low pass for grades between 60 and 75 inclusive. 
 *  Write two versions: One version that uses only conditional operators; 
 *  the other should use one or more if statements.
 *  Which version do you think is easier to understand and why?
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    int grade = 0;
    while(cin >> grade) {
        cout << ((grade > 90) ? "high pass" : 
            (grade < 60) ? "fail" : 
            (grade < 75) ? "low pass" : "pass");
        cout << endl;
        
        // multiple if statements are easy to read.
        if(grade > 90) cout << "high pass" << endl;
        else if(grade < 60) cout << "fail" << endl;
        else if(grade < 75) cout << "low pass" << endl;
        else cout << "pass" << endl;
    }
    return 0;
}