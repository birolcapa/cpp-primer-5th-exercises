/**
 *  \file 10.cpp
 *  \brief Exercise 8.10
 *  Write a program to store each line from a file in a vector<string>.
 *  Now use an istringstream to read each element from the vector a word at a time.
 */

#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <fstream>
using std::ifstream;

#include <sstream>
using std::istringstream;

vector<string> ReadFile(string filePath)
{
    ifstream fileStream(filePath);
    if(!fileStream)
    {
        cerr << "File cannot be opened" << endl;
        std::exit(-1);
    }
    string line;
    vector<string> fileContent;
    while(std::getline(fileStream, line)) {
        fileContent.push_back(line);
    }
    return fileContent;
}

int main()
{
    string filePath;
    cout << "Please enter the file path!" << endl;
    cin >> filePath;
    vector<string> fileLines = ReadFile(filePath);
    for(auto& s:fileLines) {
        istringstream theStringStream(s);
        string word;
        while(theStringStream >> word) {
            cout << word << " - ";
        }
        cout << endl;
    }
    return 0;
}