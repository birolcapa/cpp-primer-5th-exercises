/**
 *  \file 05.cpp
 *  \brief Exercise 8.4:
 *  Write a function to open a file for input and read its contents into a
 *  vector of strings, storing each line as a separate element in the vector.
 */

/**
 *  \file 05.cpp
 *  \brief Exercise 8.5:
 *  Rewrite the previous program to store each word in a separate element.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::cerr;
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <fstream>
using std::ifstream;

vector<string> ReadWordsInFile(string filePath)
{
    vector<string> words;
    ifstream fileStream(filePath);
    if(fileStream) {
        string word;
        while(fileStream >> word) {
            words.emplace_back(word);
        }
    }
    else {
        cerr << "File cannot be opened" << endl;
    }
    return words;
}

int main()
{
    string filePath;
    cout << "Please enter file path" << endl;
    cin >> filePath;
    vector<string> words = ReadWordsInFile(filePath);
    for(decltype(words.size()) i = 0; i < words.size(); ++i) {
        cout << "Word " << (i + 1) << " is " << words.at(i) << endl;
    }
    return 0;
}