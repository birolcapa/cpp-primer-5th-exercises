/**
 *  \file 06.cpp
 *  \brief Exercise 8.6
 *  Rewrite the bookstore program from 7.1.1 (p. 256) to read its transactions
 *  from a file. Pass the name of the file as an argument to main (6.2.5, p. 218).
 *  
 *  The latest source about bookstore program was done for 7.26 exercise
 */

#include "../07_Classes/26.h"

#include <fstream>
using std::ifstream;

int main(int argc, char **argv)
{
    /* When you use the arguments in argv,
    remember that the optional arguments begin in argv[1];
    argv[0] contains the program's name, not user input.*/
    if(argc != 2) {
        cerr << "Argument count should be 2!" << endl;
        return -1; 
    }
    ifstream fileStream(argv[1]);
    if(!fileStream) {
        cerr << "File cannot be opened" << endl;
    }
    
    Sales_data total;
    if(read(fileStream, total)) {
        Sales_data trans;
        while(read(fileStream, trans)) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(cout, total) << endl;
                total = trans;
            }
        }
        print(cout, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}