/**
 *  \file 11.cpp
 *  \brief Exercise 8.11
 *  The program in this section defined its istringstream object inside
 *  the outer while loop. What changes would you need to make if record were defined
 *  outside that loop? Rewrite the program, moving the definition of record outside the
 *  while, and see whether you thought of all the changes that are needed.
 */
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <sstream>
using std::istringstream;

struct PersonInfo {
    string name;
    vector<string> phones;
};

int main()
{
    // will hold a line and word from input; respectively
    string line, word;
    // will hold all the records from the input
    vector<PersonInfo> people;
    
    // bind record to the line we just read
    istringstream record;
        
    // read the input a line at a time until cin hits end-of-file(or another error)
    while(getline(cin, line)) {
        // Create an object to hold this record's data
        PersonInfo info;
        
        // bind record to the line we just read
        //istringstream record(line);
        
        // first clear previous record
        record.clear();
        // bind record to the line we just read
        record.str(line);
        
        // read the name
        record >> info.name;
        // read phone numbers
        while(record >> word) {
            // store them
            info.phones.push_back(word);
        }
        people.push_back(info);
    }
    
    // display records
    for(auto& person : people) {
        cout << person.name << "'s pnone numbers are ";
        for(auto& phone : person.phones) {
            cout << phone << "; ";
        }
        cout << endl;
    }
    return 0;
}