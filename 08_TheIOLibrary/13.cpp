/**
 *  \file 13.cpp
 *  \brief Exercise 8.13
 *  Rewrite the phone number program from this section to read from a
 *  named file rather than from cin.
 */

/**
 *  \file 11.cpp
 *  \brief Exercise 8.11
 *  The program in this section defined its istringstream object inside
 *  the outer while loop. What changes would you need to make if record were defined
 *  outside that loop? Rewrite the program, moving the definition of record outside the
 *  while, and see whether you thought of all the changes that are needed.
 */
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <sstream>
using std::istringstream;
using std::ostringstream;

#include <fstream>
using std::ifstream;
using std::ofstream;

struct PersonInfo {
    string name;
    vector<string> phones;
};

vector<string> ReadFile(string filePath)
{
    ifstream fileStream(filePath);
    if(!fileStream)
    {
        cerr << "File cannot be opened" << endl;
        std::exit(-1);
    }
    string line;
    vector<string> fileContent;
    while(std::getline(fileStream, line)) {
        fileContent.push_back(line);
    }
    return fileContent;
}

const short phoneNumberLen = 10;

bool valid(const string &str) {
    if(str.size() != phoneNumberLen) {
        return false;
    }
    for(const auto &c : str) {
        if(c < '0' || c > '9') { 
            return false;
        }
    }
    return true;
}

string format(const string &str) {
    // 0(535)-288-44-55
    // --012--345-67-89
    return "0(" + str.substr(0, 3) + ")-" + str.substr(3, 3) + "-" + str.substr(6, 2) + "-" + str.substr(8, 2);
}

int main()
{
    string filePath;
    cout << "Please enter the file path!" << endl;
    cin >> filePath;
    vector<string> fileLines = ReadFile(filePath);

    // will hold all the records from the input
    vector<PersonInfo> people;
        
    // read the input a line at a time until cin hits end-of-file(or another error)
    for(auto& line : fileLines){
        // Create an object to hold this record's data
        PersonInfo info;
        // bind record to the line we just read
        istringstream record(line);
        // will hold a line and word from input; respectively
        string word;
        // read the name
        record >> info.name;
        
        // read phone numbers
        // When the string has been completely read,
        // "end-of-file" is signaled and the next input operation on
        // record will fail.
        while(record >> word) {
            // store them
            info.phones.push_back(word);
        }
        people.push_back(info);
    }
    // display records
    for(const auto& person : people) {
        ostringstream formatted, badNums;
        for(const auto &nums: person.phones) {
            if(!valid(nums)) {
                badNums << " " << nums; 
            }
            else {
                formatted << " " << format(nums) << "; ";
            }
        }
        if(badNums.str().empty()) {
            cout << person.name << ": " << formatted.str() << endl;
        }
        else {
            cerr << "input error: " << person.name
                << " invalid number(s) " << badNums.str() << endl;
        }
    }
    return 0;
}