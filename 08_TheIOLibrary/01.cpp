/**
 *  \file 01.cpp
 *  \brief Exercise 8.1
 *  Write a function that takes and returns an istream&.
 *  The function should read the stream until it hits end-of-file.
 *  The function should print what it reads to the standard output.
 *  Reset the stream so that it is valid before returning the stream.
 */
#include <iostream>
#include <string>

std::istream &read(std::istream& is)
{
    std::string input;
    while(is >> input)
    {
        std::cout << input << std::endl;
    }
    is.clear();
    return is;
}

int main()
{
    read(std::cin);
    return 0;
}