/**
 *  \file 08.cpp
 *  \brief Exercise 8.8
 *  Revise the program from the previous exercise to append its output to
 *  its given file. 
 *  Run the program on the same output file at least twice to ensure that the
 *  data are preserved.
 *  
 *  Answer: Table 8.4: File Modes
 *  app - Seek to the end before every write
 */

/**
 *  \file 07.cpp
 *  \brief Exercise 8.7
 *  Revise the bookstore program from the previous section to write its output
 *  to a file. Pass the name of that file as a second argument to main.
 */

#include "../07_Classes/26.h"

#include <fstream>
using std::ifstream;
using std::ofstream;

int main(int argc, char **argv)
{
    /*When you use the arguments in argv,
    remember that the optional arguments begin in argv[1];
    argv[0] contains the program's name, not user input.*/
    if(argc != 3) {
        cerr << "Argument count shall be 2!" << endl;
        return -1;
    }
    cout << "argv[0] is " << argv[0] << endl;
    cout << "argv[1] is " << argv[1] << endl;
    cout << "argv[2] is " << argv[2] << endl;
    
    // create an input file stream
    ifstream inputStream(argv[1]);
    if(!inputStream) {
        cerr << "Failed to open" << argv[1] << endl;
    }
    
    // create an output file stream
    // Table 8.4: File Modes: app - Seek to the end before every write
    ofstream outputStream(argv[2], ofstream::app);
    if(!outputStream) {
        cerr << "Failed to open " << argv[2] << endl;
    }
    
    Sales_data total;
    if(read(inputStream, total)) {
        Sales_data trans;
        while(read(inputStream, trans)) {
            if(total.isbn() == trans.isbn()) {
                total.combine(trans);
            }
            else {
                print(outputStream, total) << endl;
                total = trans;
            }
        }
        print(outputStream, total) << endl;
    }
    else {
        cerr << "No data" << endl;
        return -1;
    }
    return 0;
}