/**
 *  \file 09.cpp
 *  \brief Exercise 8.9
 *  Use the function you wrote for the first exercise in 8.1.2 (p. 314) to print
 *  the contents of an istringstream object.
 */

/**
 *  \file 01.cpp
 *  \brief Exercise 8.1
 *  Write a function that takes and returns an istream&.
 *  The function should read the stream until it hits end-of-file.
 *  The function should print what it reads to the standard output.
 *  Reset the stream so that it is valid before returning the stream.
 */
#include <iostream>
#include <string>
#include <sstream>

std::istream &read(std::istream& is)
{
    std::string input;
    while(is >> input)
    {
        std::cout << input << std::endl;
    }
    /*
    Reset all condition values in the stream s to valid state.
    Returns void.
    */
    is.clear();
    return is;
}

int main()
{
    std::istringstream theStringStream("This is an istringstream object!");
    read(theStringStream);
    return 0;
}