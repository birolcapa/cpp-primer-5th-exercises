/**
 *  \file 04.cpp
 *  \brief Exercise 8.4:
 *  Write a function to open a file for input and read its contents into a
 *  vector of strings, storing each line as a separate element in the vector.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::cerr;
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <fstream>
using std::ifstream;

vector<string> ReadFile(string filePath)
{
    ifstream fileStream(filePath);
    vector<string> fileContent;
    if(fileStream){
        string s;
        while(std::getline(fileStream, s)) {
            fileContent.push_back(s);
        }
    }
    else {
        cerr << "File cannot be opened!" << endl;
    }
    return fileContent;
}

int main()
{
    string filePath;
    cout << "Please enter file path" << endl;
    cin >> filePath;
    vector<string> fileContent = ReadFile(filePath);
    for(decltype(fileContent.size()) i = 0; i < fileContent.size(); ++i) {
        cout << "Line " << (i + 1) << " " << fileContent.at(i) << endl;
    }
    return 0;
}