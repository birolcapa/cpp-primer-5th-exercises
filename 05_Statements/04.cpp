/**
 *  \file 04.cpp
 *  \brief Exercise 5.4:
 *  Explain each of the following examples, and correct any problems you
 *  detect.
 */

(a) 
while (string::iterator iter != s.end()) { /* . . . */ }
// iter points nothing. A proper way would be:
string::iterator iter = s.begin();
while (string::iterator iter != s.end()) { /* . . . */ }

(b) 
while (bool status = find(word)) { /* . . . */ }
if (!status) { /* . . . */ }
// status is defined inside while block. It cannot be used from outside. 
// There would be a compile error: status not defined.
// A proper way would be:
bool status;
while (status = find(word)) { /* . . . */ }
if (!status) { /* . . . */ }