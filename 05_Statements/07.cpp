Exercise 5.7: Correct the errors in each of the following code fragments:

(a) 
Problem:
if (ival1 != ival2)
    ival1 = ival2 // missing semicolon
else ival1 = ival2 = 0;
Solution:
if (ival1 != ival2)
    ival1 = ival2;
else ival1 = ival2 = 0;
    
(b) 
Problem:
if (ival < minval) // indent is not enough, we need a block for more than one statements
    minval = ival;
    occurs = 1;
Solution:
if (ival < minval) {
    minval = ival;
    occurs = 1;
}

(c) 
Problem:
if (int ival = get_value()) // ival is defined inside if scope, it is undefined outside
    cout << "ival = " << ival << endl;
if (!ival)
    cout << "ival = 0\n";
Solution:
int ival;
if (ival = get_value()) // ival is defined inside if scope, it is undefined outside
    cout << "ival = " << ival << endl;
if (!ival)
    cout << "ival = 0\n";

(d) 
Problem:
if (ival = 0) // it is just an assignment not equality check
    ival = get_value();
Solution:
if (ival == 0)
    ival = get_value();