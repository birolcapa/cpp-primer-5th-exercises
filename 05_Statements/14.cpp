/**
 *  \file 14.cpp
 *  \brief Exercise 5.14:
 *  Write a program to read strings from standard input looking for duplicated words. 
 *  The program should find places in the input where one word is followed immediately by itself. 
 *  Keep track of the largest number of times a single repetition occurs and 
 *  which word is repeated. Print the maximum number of duplicates, or else 
 *  print a message saying that no word was repeated. 
 *  For example, if the input is 
 *  how now now now brown cow cow
 *  the output should indicate that the word now occurred three times.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <string>
using std::string;

int main()
{
    string word;
    string previous_word;
    string max_repeat_word;
    int reperat_times = 0;
    int max_repeat_times = 0;
    // read until end-of-file or other input failure
    while (cin >> word) {
        if(word == previous_word) {
            reperat_times++;
        }
        else {
            reperat_times = 1;
            previous_word = word;
        }
        
        if(max_repeat_times < reperat_times) {
            max_repeat_times = reperat_times;
            max_repeat_word = previous_word;
        }
    }
    if(max_repeat_times <= 1) {
        cout << "No words were repeated!";
    }
    else {
        cout << "The most repeated word is " << max_repeat_word 
            << " occured " << max_repeat_times << "time" << endl;
    }
    return 0;
}