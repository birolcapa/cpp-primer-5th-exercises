/**
 *  \file 19.cpp
 *  \brief Exercise 5.19:
 *  Write a program that uses a do while loop to repetitively request two
 *  strings from the user and report which string is less than the other.
 */
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include <string>
using std::string;

int main()
{
    string rsp;
    do {
        cout << "Please enter two strings:";
        string s1, s2;
        cin >> s1 >> s2;
        if(s1 < s2) { cout << s1 << " is less than " << s2 << endl;}
        else if(s2 < s1) { cout << s2 << " is less than " << s1 << endl;}
        else { cout << s1 << " is equal to " << s2 << endl;}
        cout << "Try more? Enter yes or no: ";
        cin >> rsp;
    } while(!rsp.empty() && rsp[0] == 'y');
    return 0;
}