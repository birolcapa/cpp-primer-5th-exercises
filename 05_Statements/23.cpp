/**
 *  \file 23.cpp
 *  \brief Exercise 5.23:
 *  Write a program that reads two integers from the standard input and
 *  prints the result of dividing the first number by the second
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
    int a, b;
    cin >> a >> b;
    cout << a/b << endl;
    return 0;
}