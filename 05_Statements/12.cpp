/**
 *  \file 12.cpp
 *  \brief Exercise 5.12:
 *  Modify our vowel-counting program so that it counts the number of
 *  occurrences of the following two-character sequences: ff, fl, and fi.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    unsigned aCount = 0, eCount = 0, iCount = 0, oCount = 0, uCount = 0;
    unsigned spaceCount = 0, tabCount = 0, newlineCount = 0, otherCount = 0;
    unsigned ffCount = 0, flCount = 0, fiCount = 0;
    char ch;
    char previousChar=' ';
    while(cin >> std::noskipws >> ch) {
        switch(ch) {
            case 'a': case 'A':
                ++aCount;
                break;
            case 'e': case 'E':
                ++eCount;
                break;
            case 'f':
                if(previousChar == 'f') {
                    ffCount++;
                }
                break;
            case 'i': case 'I':
                if(previousChar == 'f') {
                    fiCount++;
                }
                ++iCount;
                break;
            case 'l':
                if(previousChar == 'f') {
                    flCount++;
                }
            case 'o': case 'O':
                ++oCount;
                break;
            case 'u': case 'U':
                ++uCount;
                break;
            case ' ':
                ++spaceCount;
                break;
            case '\t':
                ++tabCount;
                break;
            case '\n':
                ++newlineCount;
                break;
            default:
                ++otherCount;
        }
        previousChar = ch;
    }
    
    cout << "Number of vowel: a, A"<< " " << aCount << endl;
    cout << "Number of vowel: e, E"<< " " << eCount << endl;
    cout << "Number of vowel: i, I"<< " " << iCount << endl;
    cout << "Number of vowel: o, O"<< " " << oCount << endl;
    cout << "Number of vowel: u, U"<< " " << uCount << endl;
    cout << "Number of vowel: space"<< " " << spaceCount << endl;
    cout << "Number of vowel: \\t"<< " " << tabCount << endl;
    cout << "Number of vowel: \\n"<< " " << newlineCount << endl;
    cout << "Number of vowel: others"<< " " << otherCount << endl;
    cout << "Number of vowel: ff"<< " " << ffCount << endl;
    cout << "Number of vowel: fi"<< " " << fiCount << endl;
    cout << "Number of vowel: fl"<< " " << flCount << endl;
    
    return 0;
}