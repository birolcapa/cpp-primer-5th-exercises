/**
 *  \file 11.cpp
 *  \brief Exercise 5.11:
 *  Modify our vowel-counting program so that it also counts the number
 *  of blank spaces, tabs, and newlines read.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    unsigned aCount = 0, eCount = 0, iCount = 0, oCount = 0, uCount = 0;
    unsigned spaceCount = 0, tabCount = 0, newlineCount = 0, otherCount = 0;
    char ch;
    while(cin >> std::noskipws >> ch) {
        switch(ch) {
            case 'a': case 'A':
                ++aCount;
                break;
            case 'e': case 'E':
                ++eCount;
                break;
            case 'i': case 'I':
                ++iCount;
                break;
            case 'o': case 'O':
                ++oCount;
                break;
            case 'u': case 'U':
                ++uCount;
                break;
            case ' ':
                ++spaceCount;
                break;
            case '\t':
                ++tabCount;
                break;
            case '\n':
                ++newlineCount;
                break;
            default:
                ++otherCount;
        }
    }
    
    cout << "Number of vowel: a, A"<< " " << aCount << endl;
    cout << "Number of vowel: e, E"<< " " << eCount << endl;
    cout << "Number of vowel: i, I"<< " " << iCount << endl;
    cout << "Number of vowel: o, O"<< " " << oCount << endl;
    cout << "Number of vowel: u, U"<< " " << uCount << endl;
    cout << "Number of vowel: space"<< " " << spaceCount << endl;
    cout << "Number of vowel: \\t"<< " " << tabCount << endl;
    cout << "Number of vowel: \\n"<< " " << newlineCount << endl;
    cout << "Number of vowel: others"<< " " << otherCount << endl;
    
    return 0;
}