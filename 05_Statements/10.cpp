/**
 *  \file 10.cpp
 *  \brief Exercise 5.10:
 *  There is one problem with our vowel-counting program as we’ve implemented it: 
 *  It doesn’t count capital letters as vowels. 
 *  Write a program that counts both lower- and uppercase letters 
 *  as the appropriate vowel—that is, your program 
 *  should count both ’a’ and ’A’ as part of aCnt, and so forth.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

void print_vowel(char ch, int count) {
    cout << "Number of vowel: "<< ch << " "<< count << endl;
}

int main()
{
    unsigned aCount = 0, eCount = 0, iCount = 0, oCount = 0, uCount = 0;
    char ch;
    while(cin >> ch) {
        switch(ch) {
            case 'a': case 'A':
                ++aCount;
                break;
            case 'e': case 'E':
                ++eCount;
                break;
            case 'i': case 'I':
                ++iCount;
                break;
            case 'o': case 'O':
                ++oCount;
                break;
            case 'u': case 'U':
                ++uCount;
                break;
        }
    }
    
    print_vowel('a', aCount);
    print_vowel('e', eCount);
    print_vowel('i', iCount);
    print_vowel('o', oCount);
    print_vowel('u', uCount);
    
    return 0;
}