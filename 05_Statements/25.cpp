/**
 *  \file 25.cpp
 *  \brief Exercise 5.25:
 *  Revise your program from the previous exercise to use a try block to
 *  catch the exception. The catch clause should print a message to the user and ask
 *  them to supply a new number and repeat the code inside the try.
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <stdexcept>
using std::runtime_error;

int main()
{
    int a, b;
    cout << "Enter two number:";
    while(cin >> a >> b) {
        try{
            if(b == 0) {
                throw runtime_error("divisor cannot be zero!");
            }
            cout << static_cast<double>(a) / b << endl;
        }
        catch(runtime_error error) {
            cout << error.what() << endl;
            cout << "Do you want to try again? Yes or No?" << endl;
            char input;
            cin >> input;
            if(!cin || input == 'n') {
                break;
            }
        }
    
    }
    if(b == 0) {
        throw std::runtime_error("divisor cannot be zero!");
    }
    cout << a/b << endl;
    return 0;
}