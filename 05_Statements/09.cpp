/**
 *  \file 09.cpp
 *  \brief Exercise 5.9:
 *  Write a program using a series of if statements to count the number of
 *  vowels in text read from cin.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

void print_vowel(char ch, int count) {
    cout << "Number of vowel: "<< ch << " "<< count << endl;
}

int main()
{
    unsigned aCount = 0, eCount = 0, iCount = 0, oCount = 0, uCount = 0;
    char ch;
    while(cin >> ch) {
        if(ch == 'a') {
            ++aCount;
        }
        else if(ch == 'e') {
            ++eCount;
        }
        else if(ch == 'i') {
            ++iCount;
        }
        else if(ch == 'o') {
            ++oCount;
        }
        else if(ch == 'u') {
            ++uCount;
        }
    }
    
    print_vowel('a', aCount);
    print_vowel('e', eCount);
    print_vowel('i', iCount);
    print_vowel('o', oCount);
    print_vowel('u', uCount);
    
    return 0;
}