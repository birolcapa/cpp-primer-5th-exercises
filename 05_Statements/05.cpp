/**
 *  \file 5.cpp
 *  \brief Exercise 5.5:
 *  Using an if–else statement, write your own version of the program to
 *  generate the letter grade from a numeric grade
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <vector>
using std::vector;

#include <string>
using std::string;

int main()
{
    vector<string> scores {"F", "D", "C", "B", "A", "A++"};    
    int grade = 0;
    cout << "Enter grade" << endl;
    while(cin >> grade) {
        string letter_grade;
        if(grade < 60) {
            letter_grade = scores[0];
        }
        else {
            letter_grade = scores[(grade-50)/10];
            if(grade != 100) {
                if(grade % 10 > 7) {
                    letter_grade += "+";
                }
                else if(grade % 10 < 3) {
                    letter_grade += "-";
                }
            }
        }
        cout << letter_grade << endl;
    }
    return 0;
}