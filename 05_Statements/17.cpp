/**
 *  \file 17.cpp
 *  \brief Exercise 5.17:
 *  Given two vectors of ints, write a program to determine whether
 *  one vector is a prefix of the other. 
 *  For vectors of unequal length, compare the number of elements of the smaller vector.
 *  For example, given the vectors containing 0, 1, 1, and 2 and 0, 1, 1, 2, 3, 5, 8, 
 *  respectively your program should return true.
 */

#include <iostream>
using std::cout;
using std::endl;
#include <vector>
using std::vector;

int main()
{
    int is_prefix = false;
    vector<int> vec1 { 0, 1, 1, 2};
    vector<int> vec2 { 0, 1, 1, 2, 3, 5, 8};
    auto size = (vec1.size() > vec1.size()) ? vec1.size() : vec2.size();
    for(decltype(vec1.size()) i = 0; i!= size; ++i) {
        if(vec1[i] != vec2[i]) {
            is_prefix = false;
            break;
        }
    }
    is_prefix = true;
    cout << "Is prefix? " << ((is_prefix) ? "Yes" : "No") << endl;
    return 0;
}