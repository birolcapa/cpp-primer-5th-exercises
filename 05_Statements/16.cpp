/**
 *  \file 16.cpp
 *  \brief Exercise 5.16:
 *  The while loop is particularly good at executing while some condition
 *  holds; for example, when we need to read values until end-of-file. The for loop is
 *  generally thought of as a step loop: An index steps through a range of values in a
 *  collection. Write an idiomatic use of each loop and then rewrite each using the other
 *  loop construct. If you could use only one loop, which would you choose? Why?
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

int main()
{
    // I would choose this idiom for that scenario
    int i;
    while(cin >> i) {
        cout << "i is: " << i << endl;
    }
    
    // I dont like it
    for(int j; cin >> j; /* no expression */) {
        cout << "j is: " << j << endl;
    }
    
    // I would choose this idiom for that scenario
    for(int i = 0; i < 10; ++i) {
        cout << "i is: " << i << endl;
    }
    
    // I dont like it
    int j = 0;
    while(j < 10) {
        cout << "j is: " << j << endl;
        j++;
    }
    
    return 0;
}