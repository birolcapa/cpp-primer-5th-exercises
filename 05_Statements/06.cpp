/**
 *  \file 06.cpp
 *  \brief Exercise 5.6:
 *  Rewrite your grading program to use the conditional operator (§ 4.7,p. 151)
 *  in place of the if–else statement
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <vector>
using std::vector;

#include <string>
using std::string;

int main()
{
    vector<string> scores {"F", "D", "C", "B", "A", "A++"};    
    int grade = 0;
    cout << "Enter grade" << endl;
    while(cin >> grade) {
        bool is_valid_entry = (grade >=0 && grade <=100) ? true : false;
        if(is_valid_entry) {
            string letter_grade = (grade < 60) ? scores[0] : scores[(grade-50)/10];
            letter_grade += (grade == 100 || grade < 60) ? ""
                : (grade % 10 > 7) ? "+"
                : (grade % 10 < 3) ? "-" 
                : "";
                cout << letter_grade << endl;
        }
        else {
            cout << "Please enter grades between 0 and 100" << endl;
            continue;
        }
    }
    return 0;
}