/**
 *  \file 21.cpp
 *  \brief Exercise 5.21:
 *  Revise the program from the exercise in 5.5.1 (p. 191) so that it looks
 *  only for duplicated words that start with an uppercase letter.
 *  
 *  Exercise 5.20:
 *  Write a program to read a sequence of strings
 *  from the standard input until either the same word occurs twice in succession 
 *  or all the words have been read.
 *  Use a while loop to read the text one word at a time. 
 *  Use the break statement to terminate the loop if a word occurs twice in succession.
 *  Print the word if it occurs twice in succession, 
 *  or else print a message saying that no word was repeated.
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cin;
#include <string>
using std::string;

int main()
{
    string str, prev_str;
    string repeated_word;
    while(cin >> str) {
        if(!isupper(str[0])){ // if it does not start with an uppercase letter
            continue;
        }
        
        if(str == prev_str) {
            repeated_word = str;
            break;
        }
        else {
            prev_str = str;
        }
    }
    if(!repeated_word.empty()) {
        cout << repeated_word << " is repeated" << endl;
    }
    else {
        cout << "No words was repeated" << endl;
    }
    return 0;
}